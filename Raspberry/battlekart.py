import subprocess
import os
import sys
import signal
import time
import serial
import paho.mqtt.client as mqtt

from subprocess import check_output, CalledProcessError

broker_address = "192.168.0.10"
defaultPort = 9000
library = "raspivid"

global connect_to_mqtt
global camera_running
global process
global exit_prg
global serialPort
global KartNo
global watchdog
global memWatchdog

global MQTT_id
global Topic_Startup
global Topic_Camera
global Topic_SendAttack
global Topic_RecvAttack
global Topic_Shutdown
global Topic_RecvJoystick

def initSerialPort();
	global serialPort
	serialPort = serial.Serial(
		port = '/dev/ttyAMA0',
		baudrate = 115200,
		parity = serial.PARITY_NONE,
		stopbits = serial.STOPBITS_ONE,
		bytesize = serial.EIGHTBITS,
		timeout = 1)
	

def getKartNo():
	global serialPort
	serialPort.write("N".encode())
	line = serialPort.readline()
	
	Nb = '\n'
	if (len(line)>0):
		Nb = chr(line[0])
	
	if( Nb == '1' or Nb == '2'):
		print('Connected to Kart No:%s'%(Nb))
		return int(Nb)
	else:
		return 0
		
def handler(signum, frame):
    global exit_prg
    msg="Ctrl-C was pressed"
    print(msg)
    exit_prg=True

def get_pid(name): 
    child = subprocess.Popen(['pgrep','-f',name],stdout=subprocess.PIPE)
    response = child.communicate()[0]
    return [int(pid) for pid in response.split()]

def on_disconnect(client, userdata, rc):
	global connect_to_mqtt
    print("disconnect")
	connect_to_mqtt = False

def on_message(client, userdata, message):
    global camera_running
    global process
	global Topic_Camera
	global Topic_SendAttack
	global Topic_Shutdown
	global Topic_RecvJoystick
	global serialPort
	global watchdog
    
	try:
		topic = message.topic
		message = str(message.payload.decode("utf-8"))
    except:
		print("invalid format to deceode")

    if(topic == Topic_Camera):
        if(message == 'C'):
            print("Software started") 
            subprocess.Popen(cameraCommand, stdout=subprocess.PIPE, bufsize=-1, shell=True)
            camera_running = True
            process = get_pid('raspivid')
            for id in process:
                print(id)
        elif(message == 'D'):
            print("Software ended")
            if(camera_running == True):
                for id in process:
                    os.system('sudo kill -9 %s'%(id))
                camera_running = False

	elif(topic == Topic_SendAttack):
		try:
			serialPort.write(message.encode())
		except:
			print("error transmiting to serial port")
			
	elif(topic == Topic_RecvJoystick):
		try:
			watchdog = watchdog + 1
			serialPort.write(message.encode())
		except:
			print("error transmiting to serial port")
			
	elif(topic == Topic_Shutdown):
		os.system('sudo shutdown now')
		
if __name__ == '__main__':
    exit_prg=False
    signal.signal(signal.SIGINT, handler)
	
	watchdog = 0
	memWatchdog = 0
	timeout = 0
	
	#init serial port and get kart No
	initSerialPort()
	KartNo = getKartNo()
	
	#configure command and MQTT topics
	if(KartNo != 0):
		cameraCommand = '(echo "--video boudary--"; %s -t 0 -w 1280 -h 720 -fps 25 -b 2000000 -o -;) | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=%s port=%s'%(library,broker_address,defaultPort+KartNo)
		MQTT_id = "Pi_%s"%(KartNo)
		Topic_Startup = "GameKartRpi%s/startup"%(KartNo)
		Topic_Camera = "GameKartPc/startcam"
		Topic_SendAttack = "GameKartPc%s/sendattack"%(KartNo)
		Topic_RecvAttack = "GameKartRpi%s/recvattack"%(KartNo)
		Topic_Shutdown = "GameKartPc/shutdown"
		Topic_RecvJoystick = "GameKartPc%s/joystick"%(KartNo)
	else:
		print("Error get Kart Number")
		exit(1)
		
    connect_to_mqtt=False
    client = mqtt.Client(MQTT_id)
    
	while(not exit_prg):
		while not connect_to_mqtt and not exit_prg:
			try:
				client.connect(broker_address,1883,10) #connect to broker
				client.subscribe(Topic_Camera)
				client.subscribe(Topic_SendAttack)
				client.subscribe(Topic_RecvJoystick)
				client.subscribe(Topic_Shutdown)
				client.on_message=on_message
				client.on_disconnect=on_disconnect
				connect_to_mqtt=True
				
				lastWillMessage="D"
				client.will_set(Topic_Startup,lastWillMessage,1,True)
				
				camera_running = False
				client.loop_start() #start the loop
				
				#Tell computer that program is started and connected
				client.publish(Topic_Startup,"C",1,True)
                print("connected to mqtt")
			except:
				print("error connecting MQTT")
                time.sleep(0.5)

				
		if connect_to_mqtt and camera_running:
			if watchdog == memWatchdog:
				timeout = timeout + 1
			else:
				memWatchdog = watchdog
				timeout = 0
				
			if timeout >= 4:
				client.loop_stop()
				connect_to_mqtt = False
				
		if(serialPort.inWaiting() > 0):
			sendMessage = serialPort.read(serialPort.inWaiting()).decode('ascii')
			client.publish(Topic_RecvAttack,sendMessage,1,False)
			
        time.sleep(0.5)

	#clean everything
    client.loop_stop()
	serialPort.close()
	exit(1)