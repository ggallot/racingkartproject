#define PIN_BTN_LAP   2

#define PIN_PORTE1    4
#define PIN_PORTE2    5

#define NOT_INIT      1
#define INIT          2
#define WAIT_END_LAP  3
#define END_LAP       4

char sendData[50];
char bufferRead[50];

int step;

void setup() {
  // put your setup code here, to run once:
 Serial.begin(115200);

 pinMode(PIN_BTN_LAP, INPUT_PULLUP);
 pinMode(PIN_PORTE1, INPUT_PULLUP);    
 pinMode(PIN_PORTE2, INPUT_PULLUP); 

 step = NOT_INIT;
}


void loop() {

  //lecture du port série
  int nbRead = Serial.available();
  if (nbRead > 0) {
    Serial.readBytes(bufferRead,nbRead);
  }

  //regarde si on a reçu le message de reset
  if(bufferRead[0] == 'R')
  {
    bufferRead[0] = '\0';
    step = NOT_INIT;
  }

  //regarde si on active le next lap par le bouton externe
  if(digitalRead(PIN_BTN_LAP)== false)
  {
    step = END_LAP;
  }

  //gestion de la machine d'état pour l'incrément des tours
  switch(step)
  {
    case NOT_INIT:
      if(digitalRead(PIN_PORTE1) && digitalRead(PIN_PORTE2))
      {
        step = INIT;
      }
      break;

    case INIT:
      if(digitalRead(PIN_PORTE2)==false)
      {
        step = WAIT_END_LAP;
      }
      break;

    case WAIT_END_LAP:
      if(digitalRead(PIN_PORTE1)==false)
      {
        step = END_LAP;
      }
      break;

    case END_LAP:
      int nb = sprintf(sendData,"L:1;100");
      sendData[nb] = 0;
      Serial.println(sendData);
      delay(1000);

      step = NOT_INIT;
      break;
  }
  
}
