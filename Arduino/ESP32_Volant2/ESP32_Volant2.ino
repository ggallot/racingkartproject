//Compile with ESP32 Dev Module
#include "RadioConfig.h"

#include "Adafruit_ILI9341.h"
#define ILI9341_SPI_MODE_DMA

#include "HX711.h"

#include "inc/wifi_config.h"
#include "Fond.h"
//#include "Fond_black.h"
//#include "Fond_bonus.h"
#include "WifiOff.h"
#include "WifiMid.h"
#include "WifiOn.h"
#include "Chiffre1.h"
#include "Chiffre2.h"
#include "Chiffre3.h"
#include "Missile.h"
#include "Eclair.h"
//#include "Shield.h"
#include "Oil.h"
#include "Volant.h"
#include "Ordinateur.h"
#include "Joystick.h"
#include "EspMQTTClient.h"

char* m_mqtt_broker = "192.168.0.12";       //Adresse IP du PC ayant le broker mosquitto
Adafruit_ILI9341 screen(TFT_CS, TFT_DC, TFT_RST);

HX711 scale1;
const int Calibration_Weight = 1000;

char texte[20] ;
EspMQTTClient* mqttClient;
Joystick joy;
int cntSend = 0;
char actualCenter = '_';
char requestedCenter = '_';

char actualFond = 'N';
char requestedFond = 'N';

char currentBonus = '_';
int noRadio = 0;
bool use_SecondaryWifi = false;
bool isMqttConnected = false;

String topicCon;
String topicJoy;
String topicRead;
String topicConf;

bool isMqtt_Init = false;

/**************************************************************************************
 *    Tâche de gestion de la communication MQTT avec le PC 
 * **********************************************************************************/
void taskMqtt(void * pvParameters) {
  char dataToSend[32] ;
  bool init = false;
  
  int cnt = 0;
  topicJoy = "radio";  topicJoy.concat(noRadio);  topicJoy.concat("/joystick");
  topicCon = "radio";  topicCon.concat(noRadio);  topicCon.concat("/startup");
  
  //Initialisation du client MQTT
  if(!use_SecondaryWifi) {
    mqttClient = new EspMQTTClient(m_ssid1, m_pwd1, m_mqtt_broker, "", "","Radio");
  }
  else
  {
     mqttClient = new EspMQTTClient(m_ssid2, m_pwd2, m_mqtt_broker, "", "","Radio");
  }
  String message = "D"; 
  mqttClient->enableLastWillMessage(topicCon.c_str(), message.c_str() ,true);

  mqttClient->setMqttReconnectionAttemptDelay(1000);
  mqttClient->setWifiReconnectionAttemptDelay(5000);

  isMqtt_Init = true;
  
  //boucle de traitement du client MQTT
  while(1) {
    
    joy.run();
      
    if(isMqttConnected) {
      char launchbonus = '_';
      if(joy.m_btnState) {
        launchbonus = 'L';
      }
      int nb = sprintf(dataToSend,"J:%i;S:%i;B:%c",joy.m_valDirection,joy.m_valSpeed,launchbonus);
      //mqttClient->publish(topicJoy, dataToSend);
    }
    cnt = 0;

    delay(100);
  }
}

void onConnectionEstablished()
{
  mqttClient->publish(topicCon, "C",true);
   
  topicRead = "radio";  topicRead.concat(noRadio);  topicRead.concat("/screen");
  topicConf = "radio";  topicConf.concat(noRadio);  topicConf.concat("/config");
  
  mqttClient->subscribe(topicRead.c_str(), [] (const String &payload)  {
    int nb = payload.length();

    if(nb == 1) requestedCenter = payload[0];
    if(nb == 2) {
      requestedFond = payload[1];

      switch(payload[0])
      {
        case 's':
          joy.stopKart();
          break;
        case 'b':
          joy.boostKart();
          break;
        case 'v':
          joy.InvertDirection();
          break;
        case 'r':
          joy.resetKartSpeed();
          break;
          
      }
    }
  });

  mqttClient->subscribe(topicConf.c_str(), [] (const String &payload)  {
    int val = 0;
    if(sscanf(payload.c_str(), "%i", &val) == 1) {
      joy.m_maxSpeed = (float) val;
    }
  });

}


void updateCenter(char requestedCenter)
{
  switch(requestedCenter)
  {
    case '_':
      screen.fillRect(100,74,120,120,ILI9341_BLACK);
      break;
    case '3':
      screen.drawRGBBitmap(100,74, image_data_Chiffre3, 120, 120);
      break;

    case '2':
      screen.drawRGBBitmap(100,74, image_data_Chiffre2, 120, 120);
      break;

    case '1':
      screen.drawRGBBitmap(100,74, image_data_Chiffre1, 120, 120);
      break;

    case 'G':
      screen.fillRect(100,74,120,120,ILI9341_BLACK);
      break;

    case 'M':
      screen.drawRGBBitmap(100,74, image_data_Missile, 120, 120);
      currentBonus = 'C';
      break;

    case 'E':
      screen.drawRGBBitmap(100,74, image_data_Eclair, 120, 120);
      currentBonus = 'E';
      break;

    case 'S':
      //screen.drawRGBBitmap(100,74, image_data_Shield, 120, 120);
      currentBonus = 'S';
      break;

    case 'O':
      screen.drawRGBBitmap(100,74, image_data_Oil, 120, 120);
      currentBonus = 'P';
      break;

    case 'V':
      screen.drawRGBBitmap(100,74, image_data_volant, 120, 120);
      currentBonus = 'V';
      break;
    
  }
   actualCenter = requestedCenter;
}

void updateFond(char requestedFond)
{
  switch(requestedFond)
  {
    case 'N':
      screen.drawRGBBitmap(0,25, image_data_fond,320, 216);
      break;
        
    case 'E':
      screen.drawRGBBitmap(0,25, image_data_fond,320, 216);
      screen.fillRect(0,25,320,10,ILI9341_YELLOW);
      screen.fillRect(0,230,320,10,ILI9341_YELLOW);
      screen.fillRect(0,35,10,220,ILI9341_YELLOW);
      screen.fillRect(310,35,10,220,ILI9341_YELLOW);
      break;
  }
  updateCenter(actualCenter);

  actualFond = requestedFond;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  
  pinMode(PIN_MOTOR, OUTPUT);
  digitalWrite(PIN_MOTOR,LOW);
  
  screen.begin();
  screen.setRotation(3);
  screen.fillScreen(ILI9341_BLACK);
  screen.setTextSize(2);
  
  screen.drawRGBBitmap(0,25, image_data_fond,320, 216);
  screen.fillRect(0,0,320,24,ILI9341_BLACK);

  noRadio = 1;
  
  use_SecondaryWifi = true;
  
  if(use_SecondaryWifi) {
    Serial.println("Use secondary Wifi");
  }

  scale1.begin(PIN_S1, PIN_S2);
  

  while(!scale1.is_ready());
  scale1.set_scale(1000);
  scale1.set_gain(128);
  long reading = scale1.read_average(10);
  Serial.print("HX711 reading: ");
  Serial.println(reading);
  scale1.set_offset(reading);

  joy.begin(noRadio);
  
  xTaskCreatePinnedToCore(
                  taskMqtt,     //Function to implement the task.  线程对应函数名称(不能有返回值)
                  "taskMqtt",   //线程名称
                  4096,      // The size of the task stack specified as the number of * bytes.任务堆栈的大小(字节)
                  NULL,      // Pointer that will be used as the parameter for the task * being created.  创建作为任务输入参数的指针
                  1,         // Priority of the task.  任务的优先级
                  NULL,      // Task handler.  任务句柄
                  1);        // Core where the task should run.  将任务挂载到指定内核b
                  
}

void loop() {

 /* if(isMqtt_Init)
  {
    mqttClient->loop();
    if(mqttClient->isConnected())
    {
      isMqttConnected = true;
      screen.drawRGBBitmap(290,0, image_data_WifiOn, 24, 24);
    }
    else if(mqttClient->isWifiConnected()) {
      isMqttConnected = false;
      screen.drawRGBBitmap(290,0, image_data_WifiMid, 24, 24);
    }
    else {
      isMqttConnected = false;
      screen.drawRGBBitmap(290,0, image_data_WifiOff, 24, 24);
    }
  
  }
  cntSend++;

  if( (cntSend%3) == 0)
  {
    screen.fillRect(100,0,40,24,ILI9341_BLACK);
    screen.setCursor(100, 3);
    screen.print((int)joy.m_maxSpeed);
  }

  screen.fillRect(240,0,40,24,ILI9341_BLACK);
  screen.setCursor(240, 3);
  screen.print(noRadio);

  if( (cntSend%3) == 0) screen.print('/');
  else if( (cntSend%3) == 1) screen.print('-');
  else screen.print('\\');

  if(requestedCenter != actualCenter) updateCenter(requestedCenter);
  if(requestedFond != actualFond)  updateFond(requestedFond);
*/
  if (scale1.is_ready()) {
    long reading = scale1.get_units(1);
    Serial.print("HX711 reading: ");
    Serial.println(reading);
  } else {
    Serial.println("HX711 not found.");
  }

  delay(200);
}
