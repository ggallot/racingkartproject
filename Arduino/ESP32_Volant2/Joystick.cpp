#include "Joystick.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Joystick
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void Joystick::begin(int nb)
{
  pinMode(PIN_BTN_ATTACK,INPUT);
  pinMode(PIN_JOY_SPEED_AVANCE,INPUT);
  pinMode(PIN_JOY_DIR_RECULE,INPUT);

  pinMode(PIN_POT_DROIT,INPUT);
  pinMode(PIN_POT_GAUCHE,INPUT);

  m_inclinometre.Setup();

  m_centerSpeedAvance = (int) analogRead(PIN_JOY_SPEED_AVANCE);
  m_centerSpeedRecule = (int) analogRead(PIN_JOY_DIR_RECULE);
}

//Demande d'inversion de sens du volant
void Joystick::InvertDirection()
{
  m_invertDirection = true;
}


//Demande d'arrêt du kart
void Joystick::stopKart()
{
  //Protection contre un double appel eventuel de la fonction
  if(!m_isStop)
  {
    m_memMaxSpeeed = m_maxSpeed;
    m_maxSpeed = 0;
    m_isStop = true;
    m_initTimerStop = millis();
  }
}

//Boost la vitesse max du kart
void Joystick::boostKart()
{
  m_memMaxSpeeed = m_maxSpeed;
  m_overSpeed = 10;
}

//Remet la vitesse du kart a sa vitesse nominale
void Joystick::resetKartSpeed()
{
  m_isStop = false;
  m_maxSpeed = m_memMaxSpeeed;
  m_overSpeed = 0;
  m_invertDirection = false;
}
  
void Joystick::run()
{
	readJoystick();

  if(m_invertDirection)
  {
    m_valDirection *= -1;
  }
  
	//Création du message à envoyer
  char launchbonus = '_';
  if(m_btnState) {
    launchbonus = 'L';
  }
	int nb = sprintf(m_sendData,"J:%i;S:%i;B:%c",m_valDirection,m_valSpeed,launchbonus);
	m_sendData[nb] = '\0';
 
  Serial.println(m_sendData);

  
  
  //Serial.println(analogRead(PIN_POT_DROIT));
  //Serial.println(analogRead(PIN_POT_GAUCHE));
  
  int nbRead = Serial.available();
  if (nbRead > 0) {
    Serial.readBytes(m_bufferRead,nbRead);
  }

  //remise en marche de la vitesse au cas où le message ne passe pas
  if(m_isStop)
  {
    unsigned long now = millis();
    if(now-m_initTimerStop>1500)
    {
      resetKartSpeed();
    }
  }
  else
  {
    int val_Potar = analogRead(PIN_POT_DROIT);
    m_maxSpeed = map(val_Potar,0,4095,80,50);
  }
}

void Joystick::readJoystick()
{
   float angle = m_inclinometre.getAngle();

   float maximumSpeed = m_maxSpeed + m_overSpeed;
   if(maximumSpeed>100) maximumSpeed = 100;
   
   int ANASpeedAvance = (int) analogRead(PIN_JOY_SPEED_AVANCE) - m_centerSpeedAvance;
   int ANASpeedRecule = (int) analogRead(PIN_JOY_DIR_RECULE) - m_centerSpeedRecule;
   m_btnState = 1-digitalRead(PIN_BTN_ATTACK);

   float ValSpeedAvance = 0;
   float ValSpeedRecule = 0;
   if(!VOLANT_KART)
   {
      ValSpeedAvance = -(float)(ANASpeedAvance) / 11.0;
      ValSpeedRecule = -(float)(ANASpeedRecule) / 11.0;
   }
   else
   {
      ValSpeedAvance = -(float)(ANASpeedAvance) / 18.0;
      ValSpeedRecule = (float)(ANASpeedRecule) / 18.0;
   }
   
   if(ValSpeedAvance <= 0) ValSpeedAvance = 0;
   if(ValSpeedAvance > 100) ValSpeedAvance = 100;

   if(ValSpeedRecule <= 0) ValSpeedRecule = 0;
   if(ValSpeedRecule > 100) ValSpeedRecule = 100;
   
   m_valDirection = 0;
   m_valSpeed = 0;
   
   if(ValSpeedAvance > 10 ) {
     float mapSpeedAvance = map(ValSpeedAvance,0,100,30,100);
     m_valSpeed = (int) (mapSpeedAvance * maximumSpeed / 100);
   }
   else if(ValSpeedRecule > 10 ){
     float mapSpeedRecule = map(ValSpeedRecule,0,100,30,100);
     m_valSpeed = (int) (-mapSpeedRecule * maximumSpeed / 100);
   }

   if(angle>45) angle = 45;
   if(angle<-45) angle = -45;

   m_valDirection = (int) (angle * 2.22222);

   if(m_valSpeed<-maximumSpeed) m_valSpeed = -maximumSpeed;
   if(m_valSpeed>maximumSpeed) m_valSpeed = maximumSpeed;
   if(m_valDirection<-100) m_valDirection = -100;
   if(m_valDirection>100) m_valDirection = 100;
}
