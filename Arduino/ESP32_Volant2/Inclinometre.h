#ifndef _INCLINOMETRE_H_
#define _INCLINOMETRE_H_

#include <Wire.h>
#include <Arduino.h>

#define DEVICE (0x53)      //ADXL345 device address
#define TO_READ (6)        //num of bytes we are going to read each time (two bytes for each axis)

class Inclinometre{
private:
  byte buff[20];  
  char str[512];              //string buffer to transform data before sending it to the serial port
  
  void writeTo(int device, byte address, byte val);
  void readFrom(int device, byte address, int num);
  
public:
  Inclinometre();

  void Setup();
  float getAngle();


};

#endif
