#ifndef _JOYSTICK_H_
#define _JOYSTICK_H_

//#include <printf.h>
#include "RadioConfig.h"
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

//#define __USE_M5CORE2

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   Mario Joystick
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SIZE 32 //taille maximum des données à transmettre

class Joystick {
  
private:
	int m_centerDirection = 0;
	int m_centerSpeed = 0;
  char m_bufferRead[80];

  int m_PcUartTimeOut = 0;
	void readJoystick();
	
public:
  //Variables globales
	int m_valDirection = 0;
	int m_valSpeed = 0;
  bool m_btnState = false;
  char m_currentBonus = '_';

  float m_maxSpeed = 70;
  float m_memMaxSpeeed = 70;

  bool m_connectedToPC = false;
  
  bool m_comNRF_OK = false;
  char m_sendData[SIZE];
	void begin(int nb);
  void setBonus(char bonus);

  void stopKart();
  void boostKart();
  void resetKartSpeed();
  
	void run();
	
};

#endif	//_JOYSTICK_H_
