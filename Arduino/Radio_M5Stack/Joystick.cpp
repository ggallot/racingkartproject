#include "Joystick.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Joystick
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RF24 radio(PIN_CE, PIN_CSN); // CE, CSN
const byte addressePipe[6] = "PIPE1";

void Joystick::begin(int nb)
{
  pinMode(PIN_BTN_ATTACK,INPUT);
  pinMode(PIN_JOY_DIR,INPUT);
  pinMode(PIN_JOY_SPEED,INPUT);
  dacWrite(25,0); // avoid sound
  
  Serial.print("radio No:");
  Serial.println(nb);
  //Mesure du point milieu des 2 joysticks
  m_centerDirection = (int) analogRead(PIN_JOY_DIR);
  m_centerSpeed = (int)analogRead(PIN_JOY_SPEED);

  //Initialisation de la radio

  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!!");
     while (1) {} // hold in infinite loop
   }
   
  //radio.setRetries(15, 15);
  radio.setChannel(100 + (nb-1)*5);// canal de communication de 0 a 125
  radio.setDataRate(RF24_2MBPS); // vitesse de communication RF24_250KBPS , RF24_1MBPS , ou RF24_2MBPS 
  //radio.enableDynamicPayloads();

  // ouverture des tuyaux d'ecriture
  radio.setPALevel(RF24_PA_MIN);// amplification RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH et RF24_PA_MAX
  radio.setPayloadSize(SIZE);
  //RX and TX must have same name in order to receive ACK
  radio.openWritingPipe(addressePipe);
  radio.openReadingPipe(0,addressePipe);
  radio.stopListening();
     
  Serial.println("radio hardware OK!!");
}


void Joystick::setBonus(char bonus)
{
  m_currentBonus = bonus;
}

//Demande d'arrêt du kart
void Joystick::stopKart()
{
  m_memMaxSpeeed = m_maxSpeed;
  m_maxSpeed = 0;
}

//Boost la vitesse max du kart
void Joystick::boostKart()
{
  m_memMaxSpeeed = m_maxSpeed;
  m_maxSpeed += 20;
  if(m_maxSpeed>100) m_maxSpeed = 100;
}

//Remet la vitesse du kart a sa vitesse nominale
void Joystick::resetKartSpeed()
{
  m_maxSpeed = m_memMaxSpeeed;
}
  
void Joystick::run()
{
	readJoystick();
	
	//Création du message à envoyer
  char bonus = '_';
  if(m_btnState) {
    bonus = m_currentBonus;
  }
	int nb = sprintf(m_sendData,"J:%i;S:%i;B:%c",m_valDirection,m_valSpeed,bonus);
	m_sendData[nb] = '\0';

  if(!m_connectedToPC) {
	  m_comNRF_OK = radio.write(&m_sendData, SIZE);
  }
 
  Serial.println(m_sendData);
  int nbRead = Serial.available();
  if (nbRead > 0) {
    Serial.readBytes(m_bufferRead,nbRead);

    if(m_bufferRead[0] == 'C') {
      m_connectedToPC = true;
      m_bufferRead[0] = '\0';
      m_PcUartTimeOut = 0;
    }
  }
  else { // gestion du timeout avec la connexion par port série avec le PC
    m_PcUartTimeOut++;
    if(m_PcUartTimeOut > 10) {
      m_PcUartTimeOut = 10;
      m_connectedToPC = false;
    }
  }

  
}

void Joystick::readJoystick()
{

   int ANA1 = (int) analogRead(PIN_JOY_SPEED);
   int ANA2 = (int) analogRead(PIN_JOY_DIR);
   m_btnState = 1-digitalRead(PIN_BTN_ATTACK);
   
   m_valDirection = 0;
   m_valSpeed = 0;
   
   if(ANA1 < m_centerSpeed) {
     m_valSpeed = (int) ((float)(ANA1-m_centerSpeed)*m_maxSpeed/(float)m_centerSpeed );
   }
   else {
     m_valSpeed = (int) ((float)(ANA1-m_centerSpeed)*m_maxSpeed/(float)(4095-m_centerSpeed) );
   }

   if(ANA2 < m_centerDirection) {
     m_valDirection = (int) ((float)(ANA2-m_centerDirection)*100/(float)m_centerDirection );
   }
   else {
     m_valDirection = (int) ((float)(ANA2-m_centerDirection)*100/(float)(4095-m_centerDirection) );
   }

   if(m_valSpeed<-m_maxSpeed) m_valSpeed = -m_maxSpeed;
   if(m_valSpeed>m_maxSpeed) m_valSpeed = m_maxSpeed;
   if(m_valDirection<-100) m_valDirection = -100;
   if(m_valDirection>100) m_valDirection = 100;
}
