#include "RadioConfig.h"

#ifdef __USE_M5CORE2
  #include <M5Core2.h>
  #include <Fonts/EVA_20px.h>
#else
  #include <M5Stack.h>
  //#include <Fonts/Font16.h>
#endif

#include "inc/wifi_config.h"
#include "Fond.h"
#include "Fond_black.h"
#include "Fond_bonus.h"
#include "USB.h"
#include "BAT.h"
#include "WifiOff.h"
#include "WifiMid.h"
#include "WifiOn.h"
#include "Chiffre1.h"
#include "Chiffre2.h"
#include "Chiffre3.h"
#include "ChiffreGo.h"
#include "Missile.h"
#include "Eclair.h"
#include "Shield.h"
#include "Oil.h"
#include "Ordinateur.h"
#include "Joystick.h"
#include "EspMQTTClient.h"

char* m_mqtt_broker = "192.168.0.12";       //Adresse IP du PC ayant le broker mosquitto
  
char texte[20] ;
EspMQTTClient* mqttClient;
Joystick joy;
int cntSend = 0;
char actualCenter = '_';
char requestedCenter = '_';

char actualFond = 'N';
char requestedFond = 'N';

char currentBonus = '_';

uint16_t I2C_Adress = 56;
int noRadio = 0;
bool use_SecondaryWifi = false;
bool isMqttConnected = false;

String topicCon;
String topicJoy;
String topicRead;
String topicConf;

uint8_t readI2C()
{
  uint8_t val = 0;
  Wire.beginTransmission(I2C_Adress);
  Wire.write(255);
  Wire.endTransmission();
  if(Wire.requestFrom(I2C_Adress, (uint8_t)1)>0)
  {
    val = Wire.read();
  }
  return ~val;
}


/**************************************************************************************
 *    Tâche de gestion de la communication MQTT avec le PC 
 * **********************************************************************************/
void taskMqtt(void * pvParameters) {
  char dataToSend[32] ;
  bool init = false;
  
  int cnt = 0;
  topicJoy = "radio";  topicJoy.concat(noRadio);  topicJoy.concat("/joystick");
  topicCon = "radio";  topicCon.concat(noRadio);  topicCon.concat("/startup");
  
  //Initialisation du client MQTT
  if(!use_SecondaryWifi) {
    mqttClient = new EspMQTTClient(m_ssid1, m_pwd1, m_mqtt_broker, "", "","Radio");
  }
  else
  {
     mqttClient = new EspMQTTClient(m_ssid2, m_pwd2, m_mqtt_broker, "", "","Radio");
  }
  String message = "D"; 
  mqttClient->enableLastWillMessage(topicCon.c_str(), message.c_str() ,true);

  mqttClient->setMqttReconnectionAttemptDelay(1000);
  mqttClient->setWifiReconnectionAttemptDelay(5000);
  
  //boucle de traitement du client MQTT
  while(1) {
 
    joy.run();
      
    if(isMqttConnected) {
      char bonus = '_';
      if(joy.m_btnState) {
        bonus = joy.m_currentBonus;
      }
      int nb = sprintf(dataToSend,"J:%i;S:%i;B:%c",joy.m_valDirection,joy.m_valSpeed,bonus);
      mqttClient->publish(topicJoy, dataToSend);
    }
    cnt = 0;
    delay(100);
  }
}

void onConnectionEstablished()
{
  mqttClient->publish(topicCon, "C",true);
   
  topicRead = "radio";  topicRead.concat(noRadio);  topicRead.concat("/screen");
  topicConf = "radio";  topicConf.concat(noRadio);  topicConf.concat("/config");
  
  mqttClient->subscribe(topicRead.c_str(), [] (const String &payload)  {
    int nb = payload.length();

    if(nb == 1) requestedCenter = payload[0];
    if(nb == 2) {
      requestedFond = payload[1];

      switch(payload[0])
      {
        case 's':
          joy.stopKart();
		  break;
        case 'b':
          joy.boostKart();
          break;
        case 'r':
          joy.resetKartSpeed();
          break;
          
      }
    }
  });

  mqttClient->subscribe(topicConf.c_str(), [] (const String &payload)  {
    int val = 0;
    if(sscanf(payload.c_str(), "%i", &val) == 1) {
      joy.m_maxSpeed = (float) val;
    }
  });

}



void updateCenter(char requestedCenter)
{
  joy.setBonus('_');
  switch(requestedCenter)
  {
    case '_':
      M5.Lcd.fillRect(100,74,120,120,BLACK);
      break;
    case '3':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Chiffre3);
      break;

    case '2':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Chiffre2);
      break;

    case '1':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Chiffre1);
      break;

    case 'G':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_ChiffreGo);
      break;

    case 'M':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Missile);
      currentBonus = 'C';
      joy.setBonus('c');
      break;

    case 'E':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Eclair);
      currentBonus = 'E';
      joy.setBonus('e');
      break;

    case 'S':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Shield);
      currentBonus = 'S';
      joy.setBonus('s');
      break;

    case 'O':
      M5.Lcd.drawBitmap(100,74,120, 120, image_data_Oil);
      currentBonus = 'P';
      joy.setBonus('p');
      break;
    
  }
   actualCenter = requestedCenter;
}

void updateFond(char requestedFond)
{
  switch(requestedFond)
  {
    case 'N':
      M5.Lcd.drawBitmap(0,25,320, 216, image_data_fond);
      break;
      
    case 'M':
      M5.Lcd.drawBitmap(0,25,320, 216, image_data_fond_black);
      break;
      
    case 'E':
      M5.Lcd.drawBitmap(0,25,320, 216, image_data_fond_bonus);
      break;
  }
  updateCenter(actualCenter);

  actualFond = requestedFond;
}

void setup() {
  // put your setup code here, to run once:
  M5.begin(true, false, true, false);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setTextSize(2);
  
  M5.Lcd.drawBitmap(0,25,320, 216, image_data_fond);
  M5.Lcd.fillRect(0,0,320,24,BLACK);

  pinMode(13,INPUT);
  
  Wire.begin(21,22);
  
  uint8_t val = readI2C();
  noRadio = (val & 0x07);

  use_SecondaryWifi = (val & 0x08)>>3;


  if(use_SecondaryWifi) {
    Serial.println("Use secondary Wifi");
  }


  joy.begin(noRadio);
  
  xTaskCreatePinnedToCore(
                  taskMqtt,     //Function to implement the task.  线程对应函数名称(不能有返回值)
                  "taskMqtt",   //线程名称
                  4096,      // The size of the task stack specified as the number of * bytes.任务堆栈的大小(字节)
                  NULL,      // Pointer that will be used as the parameter for the task * being created.  创建作为任务输入参数的指针
                  1,         // Priority of the task.  任务的优先级
                  NULL,      // Task handler.  任务句柄
                  1);        // Core where the task should run.  将任务挂载到指定内核b
}


void loop() {
  float batCurrent = 0.1;
  #ifdef __USE_M5CORE2
    batCurrent = M5.Axp.GetBatPower();
  #else
    //if(M5.Power.isCharging()) batCurrent = 0;
  #endif
  
  if((int) batCurrent == 0) {
    M5.Lcd.drawBitmap(0,0,24, 24, image_data_USB);
  }
  else {
    M5.Lcd.drawBitmap(0,0,24, 24, image_data_BAT);
  }
  
  #ifdef __USE_M5CORE2
    float batVoltage = M5.Axp.GetBatVoltage();
    float batPercentage = ( batVoltage < 3.2 ) ? 0 : ( batVoltage - 3.2 ) * 100;
    sprintf(texte,"%02d",(int)batPercentage);
  #else
    //int8_t batPercentage = M5.Power.getBatteryLevel();
    sprintf(texte,"%02d",100);
  #endif
  
  M5.Lcd.fillRect(30,0,40,24,BLACK);
  M5.Lcd.setCursor(30, 3);
  M5.Lcd.print(texte);

  if(joy.m_connectedToPC)
  {
    M5.Lcd.drawBitmap(100,0,24, 24, image_data_Ordinateur);
  }
  else
  {
    M5.Lcd.fillRect(100,0,40,24,BLACK);
  }


  mqttClient->loop();
  if(mqttClient->isConnected()) {
    isMqttConnected = true;
    M5.Lcd.drawBitmap(290,0,24, 24, image_data_WifiOn);
  }
  else if(mqttClient->isWifiConnected()) {
    isMqttConnected = false;
    M5.Lcd.drawBitmap(290,0,24, 24, image_data_WifiMid);
  }
  else {
    isMqttConnected = false;
    M5.Lcd.drawBitmap(290,0,24, 24, image_data_WifiOff);
  }

  if(joy.m_comNRF_OK)
  {
      cntSend++;
  }
  
  M5.Lcd.fillRect(240,0,40,24,BLACK);
  M5.Lcd.setCursor(240, 3);

  if( (cntSend%3) == 0) M5.Lcd.print('/');
  else if( (cntSend%3) == 1) M5.Lcd.print('-');
  else M5.Lcd.print('\\');

  if(requestedCenter != actualCenter) updateCenter(requestedCenter);
  if(requestedFond != actualFond)  updateFond(requestedFond);

  delay(200);
}
