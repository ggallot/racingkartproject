#define __USE_M5CORE2

 //definition des entrées / sorties
 #ifdef __USE_M5CORE2
    #define PIN_JOY_DIR 35
    #define PIN_JOY_SPEED 36
    #define PIN_BTN_ATTACK 13
    #define PIN_CE 27
    #define PIN_CSN 19
  #else
    #define PIN_JOY_DIR 35
    #define PIN_JOY_SPEED 36
    #define PIN_BTN_ATTACK 16
    #define PIN_CE 12
    #define PIN_CSN 13
  #endif
