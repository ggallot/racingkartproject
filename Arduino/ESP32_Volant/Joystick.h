#ifndef _JOYSTICK_H_
#define _JOYSTICK_H_

#include "RadioConfig.h"
#include "Inclinometre.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   Mario Joystick
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SIZE 32 //taille maximum des données à transmettre

class Joystick {
  
private:
	int m_centerSpeedAvance = 0;
	int m_centerSpeedRecule = 0;
  char m_bufferRead[80];

  float m_overSpeed = 0;

  bool m_isStop = false;
  bool m_invertDirection = false;
  unsigned long m_initTimerStop = 0;

	void readJoystick();

  Inclinometre m_inclinometre;
	
public:
  //Variables globales
	int m_valDirection = 0;
	int m_valSpeed = 0;
  bool m_btnState = false;

  float m_maxSpeed = 70;
  float m_memMaxSpeeed = 70;

  bool m_connectedToPC = false;
  
  char m_sendData[SIZE];
	void begin(int nb);

  void stopKart();
  void InvertDirection();
  void boostKart();
  void resetKartSpeed();
  
	void run();
	
};

#endif	//_JOYSTICK_H_
