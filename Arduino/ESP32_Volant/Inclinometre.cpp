#include "Inclinometre.h"

Inclinometre::Inclinometre()
{
  
}

void Inclinometre::Setup()
{
  Wire.begin();         // join i2c bus (address optional for master)
  //Turning on the ADXL345
  writeTo(DEVICE, 0x2D, 0);
  writeTo(DEVICE, 0x2D, 16);
  writeTo(DEVICE, 0x2D, 8);
}


float Inclinometre::getAngle()
{
  readFrom(DEVICE, 0x32, TO_READ);      //read the acceleration data from the ADXL345
                                              //each axis reading comes in 10 bit resolution, ie 2 bytes.  Least Significat Byte first!!
                                              //thus we are converting both bytes in to one int
  int16_t x = (((int16_t)buff[1]) << 8) | buff[0];
  int16_t y = (((int16_t)buff[3])<< 8) | buff[2];
  int16_t z = (((int16_t)buff[5]) << 8) | buff[4];

  float Fx = (float)x;
  float Fy = (float)y;
  float Fz = (float)z;

  float n = sqrt(Fx*Fx + Fy*Fy + Fz*Fz);

  Fx /= n;
  Fy /= n;
  Fz /= n;
  float angle = acos(Fy)*180/3.14 - 90;

  float tilt = acos(Fz) * 180 / 3.14 - 90;

  if(abs(tilt) > 50)
  {
    angle = 0;
  }
  //we send the x y z values as a string to the serial port
  /*Serial.print("The acceleration info of x, y, z are:");
  sprintf(str, "%1.3f %1.3f %1.3f", Fx, Fy, Fz);
  Serial.println(str);
  Serial.println(tilt);*/

  return -angle;
}


void Inclinometre::writeTo(int device, byte address, byte val)
{
  Wire.beginTransmission(device); //start transmission to device
  Wire.write(address);        // send register address
  Wire.write(val);        // send value to write
  Wire.endTransmission(); //end transmission
}


void Inclinometre::readFrom(int device, byte address, int num)
{
  Wire.beginTransmission(device); //start transmission to device
  Wire.write(address);        //sends address to read from
  Wire.endTransmission(); //end transmission

  Wire.beginTransmission(device); //start transmission to device
  Wire.requestFrom(device, num);    // request 6 bytes from device

  int i = 0;
  while(Wire.available())    //device may send less than requested (abnormal)
  {
    buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); //end transmission
 
}
