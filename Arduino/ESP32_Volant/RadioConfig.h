#define TFT_DC                    5
#define TFT_CS                    15
#define TFT_RST                   4


#define PIN_BTN_ATTACK            25
#define PIN_JOY_SPEED_AVANCE      34
#define PIN_JOY_DIR_RECULE        33

#define PIN_MOTOR                 26
#define PIN_POT_DROIT             35
#define PIN_POT_GAUCHE            32

#define PIN_S1                    27
#define PIN_S2                    14
#define PIN_S3                    12
#define PIN_S4                    13

#define PIN_INT                   36

#define VOLANT_KART               false
