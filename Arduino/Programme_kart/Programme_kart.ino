#include <SPI.h>
//#include <RF24.h>
#include "CustomServo.h"
//#include "filter.h"

/***************************************/
/*       DEFINITIONS                   */
/***************************************/
#define PIN_FEU_AV              12
#define PIN_FEU_AR              38
#define PIN_SERVO               5
#define PIN_SENS_MOT_GAUCHE     41
#define PIN_SENS_MOT_DROIT      40
#define PIN_PWM_IR              44
#define PIN_PWM_MOT_GAUCHE      45
#define PIN_PWM_MOT_DROIT       46

#define PIN_JUMPER1             36
#define PIN_JUMPER2             35
#define PIN_JUMPER3             34
#define PIN_JUMPER4             33

#define SIZE                    32
 
Servo direction;
//t_filtre filterDirection;
const int offsets[2] = {120,122};
int cons_centre;

//RF24 radio(53,10); //CE, CSN
const byte addressePipe[6] = "PIPE1"; //tuyau d'adresse
char radioData[SIZE];
char rpiMessage[255];
char irAnswer[SIZE];

int noRadio =0;
bool validData = false;
int timeOut = 0;

int joystick = 0;
int joy_f = 0;
int joySpeed = 0;

void setup() {
  pinMode(PIN_FEU_AV,OUTPUT);
  pinMode(PIN_FEU_AR,OUTPUT);
  pinMode(PIN_SERVO,OUTPUT);
  pinMode(PIN_SENS_MOT_GAUCHE,OUTPUT);
  pinMode(PIN_SENS_MOT_DROIT,OUTPUT);
  pinMode(PIN_PWM_IR,OUTPUT);

  pinMode(PIN_JUMPER1,INPUT_PULLUP);
  pinMode(PIN_JUMPER2,INPUT_PULLUP);
  pinMode(PIN_JUMPER3,INPUT_PULLUP);


  digitalWrite(PIN_PWM_MOT_GAUCHE,LOW);
  digitalWrite(PIN_PWM_MOT_DROIT,LOW);
  pinMode(PIN_PWM_MOT_GAUCHE,OUTPUT);
  pinMode(PIN_PWM_MOT_DROIT,OUTPUT);
  TCCR5B = TCCR5B & B11111000 | B00000001  ;
  
  digitalWrite(PIN_FEU_AV,LOW);
  digitalWrite(PIN_FEU_AR,LOW);
  digitalWrite(PIN_SENS_MOT_GAUCHE,LOW);
  digitalWrite(PIN_SENS_MOT_DROIT,LOW);
  analogWrite(PIN_PWM_IR,0);
  analogWrite(PIN_PWM_MOT_GAUCHE,0);
  analogWrite(PIN_PWM_MOT_DROIT,0);

  direction.attach(PIN_SERVO);
  direction.write(cons_centre);

  Serial.begin(115200);  //Serial for debug (USB on board)
  Serial1.begin(1200); // Serial for IR led
  Serial2.begin(1200);
  Serial3.begin(115200); //serial with raspberry

  //initialisation du module radio
  //déterination du numéro de channel de la radio
  if(digitalRead(PIN_JUMPER1) == LOW) noRadio += 1;
  if(digitalRead(PIN_JUMPER2) == LOW) noRadio += 2;
  //if(digitalRead(PIN_JUMPER3) == LOW) noRadio += 4;

  if(noRadio == 1) cons_centre = offsets[0];
  if(noRadio == 2) cons_centre = offsets[1];
  
  /*if(!radio.begin()) {
    Serial.println("radio hardware is not responding!!");
    while(1);
  }
  
  radio.setChannel(100 + (noRadio - 1)*5); // canal de communication de 0 à 125
  radio.setDataRate(RF24_2MBPS);

  radio.setPALevel(RF24_PA_MIN);
  radio.setPayloadSize(SIZE);
  radio.openReadingPipe(0,addressePipe);*/

  Serial.print("radio hardware No:");
  Serial.print(noRadio);
  Serial.println(" OK!!");

  //passe_bas(&filterDirection,0.02, 5.0, 1.0);
  direction.write(cons_centre);
  //radio.startListening();
}


void FiltreCommande()
{
  int pente = 2;

  if(abs(joySpeed) < 50)
  {
    pente = 10;
  }

  if(joystick > joy_f) 
  {
    joy_f += pente;
    if(joy_f> joystick) joy_f = joystick;
  }
  if(joystick < joy_f)
  {
    joy_f -= pente;
     if(joy_f < joystick) joy_f = joystick;
  }
 
}

void loop() {
  char bonus = '_';
  unsigned int nb = 0; 
  int cons;
   
  //Traitement du port série avec la raspberry pi
  nb = Serial3.available();
  if (nb > 0 && nb < 255)
  {
      Serial3.readBytes(rpiMessage,nb);
      rpiMessage[nb] = '\0';
      //Serial.print("Received :");
      // Write what is received to the default serial port
      //Serial.println(rpiMessage);
  
      if(rpiMessage[0] == 'N')
      {
        if(noRadio == 1) Serial3.println("1");
        if(noRadio == 2) Serial3.println("2");
      }
      else if(rpiMessage[0] == 'M')
      {
        tone(PIN_PWM_IR, 38000, 100);
        Serial2.println("MMMMMMMMMM");
      }
      else if(rpiMessage[0] == 'J')
      {
        if(sscanf(rpiMessage, "J:%d;S:%d;B:%c", &joystick, &joySpeed, &bonus) == 3) {
          timeOut = 0;
          validData = true;
        }
      }    
   }
   else if(nb>255)
   {
    //flush in case of too large message
     while(Serial3.available()) Serial3.read();
   }
 

  //Traitement des messages en provenance du récepteur infrarouge
  nb = Serial1.available();
  if (nb > 0 && nb <= SIZE)
  {
    Serial1.readBytes(irAnswer,nb);
    if(irAnswer[0] == 'M')
    {
      //Serial3.println('m');
      Serial.println("hit!");
    }
  }

  //traitement des messages de la radio
  /*if(radio.available()) {
    radio.read(&radioData, sizeof(radioData));
    Serial.println(radioData);
    if(sscanf(radioData, "J:%d;S:%d;B:%c", &joystick, &joySpeed, &bonus) == 3) {
      timeOut = 0;
      validData = true;
    }
  }*/

 // FiltreCommande();
  
  //application des consignes aux moteurs
  if(validData)
  {
    validData = false;
    if(joySpeed>5) {
        cons = 100 + abs(1.55*joySpeed);
        digitalWrite(PIN_SENS_MOT_GAUCHE,LOW);
        digitalWrite(PIN_SENS_MOT_DROIT,HIGH);
        analogWrite(PIN_PWM_MOT_GAUCHE, cons);
        analogWrite(PIN_PWM_MOT_DROIT, cons);

        digitalWrite(PIN_FEU_AR, LOW);
      }
      else if(joySpeed<-5) {
        cons = 100 + abs(1.55*joySpeed);
        digitalWrite(PIN_SENS_MOT_GAUCHE,HIGH);
        digitalWrite(PIN_SENS_MOT_DROIT,LOW);
        analogWrite(PIN_PWM_MOT_GAUCHE, cons);
        analogWrite(PIN_PWM_MOT_DROIT, cons);

        digitalWrite(PIN_FEU_AR, HIGH);
      }
      else
      {
        //moteurs à 0
        analogWrite(PIN_PWM_MOT_GAUCHE,0);
        analogWrite(PIN_PWM_MOT_DROIT,0);
        digitalWrite(PIN_FEU_AR, LOW);
      }

      
      int angle = (int) (25.0 * joystick / 100);
      //int anglef = (int) filt(&filterDirection, angle);
      direction.write(cons_centre + angle);
  }
 
  if(timeOut >= 500)
  {
    analogWrite(PIN_PWM_MOT_GAUCHE,0);
    analogWrite(PIN_PWM_MOT_DROIT,0);
    direction.write(cons_centre);
  }
  timeOut += 10;
  delay(10);
}
