#ifndef _FILTER_H_
#define _FILTER_H_

/**
 * \file filter.h
 */
 
#define MAX_COEFF	3
#define PI 3.14159
/**
 * \struct t_filtre
 * \brief Structure for a numeric filter.
 *
 * This structure contains all parameters needed to apply a numeric filter.
 */
typedef struct
{
	float B[MAX_COEFF];/*!< Coefficients of filter's numerator (B0+B1.z-1+B2.z-2...). */
	float A[MAX_COEFF];/*!< Coefficients of filter's denominator. (A0+A1.z-1+A2.z-2...) */
	
	float u_mem[MAX_COEFF+1];/*!< Buffer to memorize the input signal. */
	float y_mem[MAX_COEFF+1];/*!< Buffer to memorize the output signal. */
	
	int num_size;/*!< Number of coefficients on the numerator. */
	int den_size;/*!< Number of coefficients on the denominator. */
	float SampleTime;/*!< Sample Time of the filter. */
}t_filtre;


/********************************************
 *          Function prototypes             *
 ********************************************/

void init_filter(t_filtre *filtre,float *num,int nsize, float *den, int dsize,float sTe);
void filter_CondInit(t_filtre *filtre,float cI);
void passe_bas(t_filtre *filtre,float sTe,float fc, float Gain);
void passe_haut(t_filtre *filtre,float sTe,float fc,float Gain);
void derive_filtre(t_filtre *filtre,float sTe,float fc);
float filt(t_filtre *filtre, float u);


#endif