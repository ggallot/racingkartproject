/**
 * \file filter.c
 * \brief File that allows the configuration of numeric filters.
 * \date 04 mars 2009
 *  
 * This file contains a list of functions that are necessary to configure and
 * simulate a numeric filter. Low-pass and High-pass filter (order 1) are proposed
 * in this file. Other type of filter can be used, but user has to specify all coefficients
 * of the filter
 */
#include "filter.h"
#include <math.h>

/**
 * @fn void init_filter(t_filtre *filtre,float *num,int nsize, float *den, int dsize,float sTe)
 * @brief Initialize the filter before simulation
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float *num: pointer to a table of data, representing the numerator coefficients of the filter
 * @param int nsize: number of coefficients used in the numerator
 * @param float *num: pointer to a table of data, representing the denominator coefficients of the filter
 * @param int dsize: number of coefficients used in the denominator
 * @param float sTe; Sample Time of the filter
 * @return void.
 */
void init_filter(t_filtre *filtre,float *num,int nsize, float *den, int dsize,float sTe)
{
	
	int i;
	
	// On rempli les coefficients de A et B
	filtre->num_size = nsize;
	for(i=0;i<nsize;i++) 
	{
		filtre->B[i] = num[i];
	}
	
	filtre->den_size = dsize;
	for(i=0;i<dsize;i++)
	{
		filtre->A[i] = den[i];
	}
	
	// On initialize les buffers de mémorisation
	for(i=0; i < MAX_COEFF+1 ; i++)
	{
		filtre->u_mem[i] = 0.0;
		filtre->y_mem[i] = 0.0;
	}
	filtre->SampleTime = sTe;

}

/**
 * @fn filter_CondInit(t_filtre *filtre,float cI)
 * @brief Initialize the filter to a Initial Condition.
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float cI: Value of the Initial Condition.
 * @return void.
 */
void filter_CondInit(t_filtre *filtre,float cI)
{
	int n,m;

	for( n = 0; n<=filtre->num_size-1; n++ )
  	{
    	filtre->u_mem[n] = cI;
  	}
  	
  	for( m = 0; m<=filtre->num_size; m++ )
  	{
		filtre->y_mem[m] = cI;
  	}	

}


/**
 * @fn passe_bas(t_filtre *filtre,float sTe,float fc, float Gain)
 * @brief Initialize a filter to be a Low-pass filter.
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float sTe: Sample Time of the filter.
 * @param float fc: The cutting frequency of the filter.
 * @param float Gain: The static Gain of the filter.
 * @return void.
 *
 * When this function is used, function init_filter does not be called.
 */
void passe_bas(t_filtre *filtre,float sTe,float fc, float Gain)
{
	float B[2];
	float A[2];
	float tau;
	
	tau = 1.0/(2.0*PI*fc);
	B[0] = Gain*sTe/(2*tau+sTe); //Gain*sTe/(2.0*tau+sTe);
	B[1] = B[0];
	
	A[0] = 1.0;
	A[1] = -(2.0*tau-sTe)/(2.0*tau+sTe);
	
	init_filter(filtre,B,2,A,2,sTe);
}

/**
 * @fn passe_haut(t_filtre *filtre,float sTe,float fc, float Gain)
 * @brief Initialize a filter to be a High-pass filter.
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float sTe: Sample Time of the filter.
 * @param float fc: The cutting frequency of the filter.
 * @param float Gain: The static Gain of the filter.
 * @return void.
 *
 * When this function is used, function init_filter does not be called.
 */
void passe_haut(t_filtre *filtre,float sTe,float fc,float Gain)
{
	float B[2];
	float A[2];
	float tau;
	
	tau = 1.0/(2.0*PI*fc);
	B[0] = Gain/(1+sTe/(2.0*tau));
	B[1] = -B[0];
	
	A[0] = 1.0;
	A[1] = -(2.0*tau-sTe)/(2.0*tau+sTe);
	
	init_filter(filtre,B,2,A,2,sTe);
}

/**
 * @fn derive_filtre(t_filtre *filtre,float sTe,float fc)
 * @brief Initialize a filter to be a Low-pass derivative filter.
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float sTe: Sample Time of the filter.
 * @param float fc: The cutting frequency of the filter.
 * @return void.
 *
 * When this function is used, function init_filter does not be called.
 */
void derive_filtre(t_filtre *filtre,float sTe,float fc)
{
	float B[3];
	float A[2];
	float tau;
	
	tau = 1.0/(2.0*PI*fc);
	
	B[0] = 1.0/(2.0*tau+sTe);
	B[1] = 0.0;
	B[2] = -B[0];
	
	A[0] = 1.0;
	A[1] = -(2.0*tau-sTe)/(2.0*tau+sTe);
	
	init_filter(filtre,B,3,A,2,sTe);
	
}

/**
 * @fn filt(t_filtre *filtre,float u)
 * @brief Simulate the filter respect to time.
 * @param t_filtre *filtre: pointer to a filter structure.
 * @param float u: Input of the filter.
 * @return float: the output of the filter.
 */
float filt(t_filtre *filtre, float u)
{
	float y=0;
	int n,m;
	// u: Signal d'entrée
	// y: Signal de sortie
  
  	filtre->u_mem[0] = u;
  	
  	for( n = filtre->num_size-1 ; n>=0 ; n-- )
  	{
    	y += filtre->B[n] * filtre->u_mem[n];
    	filtre->u_mem[n+1] = filtre->u_mem[n];
  	}
  	
  	for( m = filtre->num_size-1 ; m >=1 ; m-- )
  	{
       y -= filtre->A[m] * filtre->y_mem[m];
       filtre->y_mem[m+1] = filtre->y_mem[m];	
  	}	
  	filtre->y_mem[1] = y;
  	
  	return y;

}