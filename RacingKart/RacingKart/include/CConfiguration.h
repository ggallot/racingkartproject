#ifndef _CCONFIGURATION_H_
#define _CCONFIGURATION_H_

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>

#define NameFileIni "Config.ini"

using namespace std;

/// <summary>
/// Classe permettant de charger les paramètres de configuration du jeu
/// </summary>
class CConfiguration
{
private:
	static CConfiguration* m_instance;		//pointeur pour avoir la classe en singleton
	CConfiguration();

	string m_FilePath;
	bool m_loadedOK;

	bool GetConfiguration();
	string trim(const string& s);
	
public:
	
	bool isConfigOK();
	static CConfiguration* getInstance();
	
	string m_brokerAdresse = "127.0.0.1";
	string m_GstreamerLibPath = "";
	string m_GstreamerBinPath = "";
	double m_DistanceGetBonus = 0;
	double m_DureeTagValid = 0.1;
	int m_NombreToursPartie = 1;

	string m_comPortKart1 = "COM4";
	string m_comPortKart2 = "COM5";
	string m_comPortKartLap = "COM6";
	
};
#endif // !_CCONFIGURATION_H_
