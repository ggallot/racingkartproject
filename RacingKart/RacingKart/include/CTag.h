#ifndef _CTAG_H_
#define _CTAG_H_

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>
#include "CConfiguration.h"

using namespace std;

/// <summary>
/// Classe de gestion des tags d�tect�s dans l'image
/// </summary>
class CTag
{
private:

	clock_t m_startDetected;		//Timer indiquant le moment ou un tag a commenc� a �tre d�tect�
	cv::Vec3d m_decGauche;			//Vecteur de positionnement du tag par rapport au centre de la porte ci celui-ci est imprim� a gauche
	cv::Vec3d m_decDroit;			//Vecteur de positionnement du tag par rapport au centre de la porte ci celui-ci est imprim� a droite
	cv::Mat m_rotMatrix;			//Matrice de positionnement 3D du Tag par rapport � la cam�ra

	CConfiguration* m_config;
public:
	CTag();
	~CTag();

	void UpdateNotFound();
	void UpdatePosition(cv::Vec3d position, cv::Vec3d orientation);

	void EstimationTagDroit();
	void EstimationTagGauche();

	bool m_TagIsValid;				//Indique si le tag est valide (reste valide un certain temps apr�s la perte de d�tection pour �viter les clignotements)
	cv::Vec3d m_position;			
	
	cv::Vec3d m_estimatedPosition;	//Estimation de la position du centre de la porte
};
#endif