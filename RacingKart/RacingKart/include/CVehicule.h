#ifndef _CVEHICULE_H_
#define _CVEHICULE_H_
#include <GL/glut.h>
#include "STLLoader.h"

/// <summary>
/// Classe permettant l'affichage de la 3D du kart dans l'image
/// </summary>
class CVehicule
{
private:
	GLuint m_Calandre;
	GLuint m_RoueGauche;
	GLuint m_Console;
	GLuint m_Direction;
	GLuint m_Fusee_droite;
	GLuint m_Fusee_gauche;
	GLuint m_MoyeuDroit;
	GLuint m_MoyeuGauche;
	GLuint m_RoueDroite;

	float m_angleRoue = 0;				//Angle de braquage de la roue
	float m_speedRotation = 0;			//Vitesse de rotation des roues
public:

	CVehicule(int noKart);
	~CVehicule();

	void DrawVehicule(int direction, int speed);
};
#endif // !_CVEHICULE_H_
