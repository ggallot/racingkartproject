#ifndef _COPENGL_H_
#define _COPENGL_H_
#include <GL/glut.h>
#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

using namespace std;

/// <summary>
/// Classe de base pour le chargement OpenGL des textures et images
/// </summary>
class COpenGL
{
private:

public:
	COpenGL();
	~COpenGL();

	static GLuint LoadTexture(const char* filename, int width, int height, int transparency = 255);

	static GLuint LoadTexturePNG(const char* filename, bool verticalFlip = true);

	virtual void Draw(float x, float y, float z, float Rz);
};
#endif // !_COPENGL_H_H
