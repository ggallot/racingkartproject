#ifndef _CSOUNDMANAGER_H_
#define _CSOUNDMANAGER_H_

#include <iostream>
#include <SDL_mixer.h>
#include <vector>
#include "CSound.h"
#include "CTirages.h"

using namespace std;

#define BK_SOUND_COUNTDOWN 1
#define BK_SOUND_BONUS 2
#define BK_CHANGEMENU 3
#define BK_ENTERMENU 4
#define BK_RETURNMENU 5

/// <summary>
/// Classe de gestion des sons
/// </summary>
class CSoundManager
{
private:
	static CSoundManager* m_instance;			//Pointeur qui permet � la classe d'�tre en singleton
	bool m_runningThread;						//Indique si le thread de gestion des sons doit tourner en boucle
	HANDLE  m_Soundthread;						//Handle vers le thread

	vector<CSound*> m_listSoundMenu;			//Liste des sons pour les menus
	vector<CSound*> m_listSoundGame;			//Liste des sons pour le jeu

	CSound* m_soundCountdown;					//Son pour le compte a rebourd avant le d�part
	CSound* m_soundBonus;						//Son pour le tirage du bonus
	CSound* m_changeMenu;						//Son lorsqu'on d�file le menu
	CSound* m_enterMenu;						//Son lorsqu'un menu est s�lectionn�
	CSound* m_returnMenu;						//Son lorsqu'il y a retour en arri�re dans les menus

	int m_doPlayingMenuMusic;					//Indique l'identifiant du son des menus � jouer
	int m_doPlayingGameMusic;					//Indique l'identifiant du son a jouer dans le jeu

	unsigned long WINAPI ThreadSound(void* param);
	static DWORD WINAPI _GameThreadEntry(LPVOID);

public:
	static CSoundManager* getInstance();

	CSoundManager();
	~CSoundManager();
	
	void Close();
	void StartMusicMenu();
	void StopMusic();
	bool IsMusicPlaying();

	void StartMusicGame(); 
	void PlaySoundEffect(int idSound);

	bool SoundEffectPlaying(int idSound);
};
#endif