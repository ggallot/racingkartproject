#pragma once

#define IMAGE_WIDTH	1280
#define IMAGE_HEIGHT 720

#define CENTER_X 640
#define CENTER_Y 360

#define WINDOW_NEAR 0.001
#define WINDOW_FAR 100