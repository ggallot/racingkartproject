#ifndef _CSTART_H_
#define _CSTART_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <GL/glut.h>

#include "COpenGL.h"

/// <summary>
/// Classe affichant le damier sous la porte de d�part
/// </summary>
class CStart : public COpenGL
{
private:
	GLuint m_StartList = 0;				//Identifiant de la liste OpenGL
	GLuint m_textureStart = 0;			//Identifiant OpenGL pour la texture du damier

	double m_speedRotation = 100;	//100� par seconde
public:
	CStart();
	~CStart();

	void Draw(float x, float y, float z, float Rz);
};

#endif