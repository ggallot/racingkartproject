#ifndef _CATTAQUE_H_
#define _CATTAQUE_H_

#include <GL/glut.h>
#include <string.h>
#include <vector>
#include "../config.h"
#include "../COpenGL.h"
#include "../CMqtt.h"

using namespace std;

/// <summary>
/// Classe de base pour afficher les attaques sur l'�cran
/// </summary>
class CAttaque
{
protected:
	int m_noKart;						//Numero du jart
	bool m_ended;						//Indique que le temps d'activation de l'attaque est termin�
	GLuint m_texture;					//Identification OpenGL de l'image a afficher
	clock_t m_initTime;					//Temps de d�part du lancement de l'attaque

	CMqtt* m_mqtt = nullptr;			//Pointeur vers la classe MQTT pour envoyer les ordres

	bool ended = false;
	string m_startMqttMsg = "";			//Message � envoyer � la radio au d�but de l'attaque
	string m_endMqttMsg = "";			//Message � envoyer � la radio � la fin de l'attaque

	float m_centerX, m_centerY;			//Position du centre de l'image � afficher sur l'�cran
	float m_X1, m_Y1;					//Position du premier bord de l'image
	float m_X2, m_Y2;					//Position du second bord de l'image

	float m_thetaMax = 360;				//Angle maximum de la barre de progression du temps de l'attaque

	/// <summary>
	/// Envoi du message � la radio au d�but de l'attaque
	/// </summary>
	void SendMqttBefore() {
		if (m_mqtt != nullptr && m_startMqttMsg != "")
		{
			m_mqtt->SendToRadio(m_noKart, m_startMqttMsg, true);
		}
	}

	/// <summary>
	/// Envoi du message � la radio � la fin de l'attaque
	/// </summary>
	void SendMqttAfter() {
		if (m_mqtt != nullptr && m_endMqttMsg != "")
		{
			m_mqtt->SendToRadio(m_noKart, m_endMqttMsg, true);
		}
	}

public:

	CAttaque(CMqtt* mqtt, int noKart) {
		m_mqtt = mqtt;
		m_noKart = noKart;

		m_protectionAttaques = false; 
		m_ended = false;
		m_texture = 0;

		m_centerX = IMAGE_WIDTH - 100;
		m_centerY = IMAGE_HEIGHT - 100;
		m_X1 = m_centerX - 100;
		m_Y1 = m_centerY - 100;
		m_X2 = m_centerX + 100;
		m_Y2 = m_centerY + 100;

	}

	~CAttaque() {
		if (m_texture != 0)
		{
			glDeleteTextures(1, &m_texture);
			m_texture = 0;
		}
	}

	bool m_protectionAttaques = false;			//Indique si le joueur est prot�g� d'une attaque adverse
	
	/// <summary>
	/// Fonction virtuelle pour initialiser l'attaque
	/// </summary>
	virtual void Init()
	{
		SendMqttBefore();
	}

	/// <summary>
	/// Fonction virtuelle appel�e � chaque rafraichissement de l'�cran
	/// </summary>
	virtual void Run() {
	}

	/// <summary>
	/// Fonction indiquant si l'attaque est termin�e
	/// </summary>
	/// <returns>True si l'attaque est termin�e</returns>
	bool isEnded() { 
		if (m_ended && !ended)
		{
			ended = true;
			SendMqttAfter();
		}
		return m_ended; 
	}

	/// <summary>
	/// Met fin imm�diatement � l'attaque
	/// </summary>
	void CancelAttaque()
	{
		m_ended = true;
		SendMqttAfter();
	}

	/// <summary>
	/// Affichage du timer de l'attaque
	/// </summary>
	void drawTimeBonus()
	{
		float R, G, B;
		glBegin(GL_QUAD_STRIP);
		for (float theta = 0; theta <= m_thetaMax; theta += 2.0)
		{
			hsv2rgb(theta / 720.0f, R, G, B);
			glColor3f(R, G, B);
			glVertex3f(m_centerX + 70 * (float)sin(theta * M_PI / 180), m_centerY + 70 * (float)cos(theta * M_PI / 180), 9.999f);
			glVertex3f(m_centerX + 80 * (float)sin(theta * M_PI / 180), m_centerY + 80 * (float)cos(theta * M_PI / 180), 9.999f);
		}
		glEnd();
	}

	/// <summary>
	/// Conversion hsv vers rgb
	/// </summary>
	/// <param name="h">[IN] param�tre de couleur</param>
	/// <param name="R">[OUT] composante de couleur rouge</param>
	/// <param name="G">[OUT] comosante de couleur verte</param>
	/// <param name="B">[OUT] composante de couleur bleue</param>
	void hsv2rgb(float h, float &R, float &G, float &B)
	{
		float s = 1;
		float v = 1;

		h = 6 * h;
		float k = (float)floor(h);
		float p = h - k;
		float t = 1 - s;
		float n = 1 - s * p;
		p = 1 - (s * (1 - p));

		R = G = B = 0;

		if (k == 0 || k == 6)
		{
			R = 1;
			G = p;
			B = t;
		}
		else if (k == 1)
		{
			R = n;
			G = 1;
			B = t;
		}
		else if (k == 2)
		{
			R = t;
			G = 1;
			B = p;
		}
		else if (k == 3)
		{
			R = t;
			G = n;
			B = 1;
		}
		else if (k == 4)
		{
			R = p;
			G = t;
			B = 1;
		}
		else if (k == 5)
		{
			R = 1;
			G = t;
			B = n;
		}
	};

};

#endif