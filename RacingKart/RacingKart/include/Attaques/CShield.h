#ifndef _CSHIELD_H_
#define _CSHIELD_H_

#include "CAttaque.h"

using namespace std;

class CShield : public CAttaque
{
private:
	
public:
	
	CShield(CMqtt* mqtt, int noKart);
	~CShield();
	

	void Init();
	void Run();
	

};

#endif