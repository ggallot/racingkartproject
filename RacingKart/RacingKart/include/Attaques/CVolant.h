#ifndef _CVOLANT_H_
#define _CVOLANT_H_

#include "CAttaque.h"

using namespace std;

class CVolant : public CAttaque
{
private:


public:
	
	CVolant(CMqtt *mqtt, int noKart);
	~CVolant();

	void Init();
	void Run();
};

#endif