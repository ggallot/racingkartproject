#ifndef _CBLOOPER_H_
#define _CBLOOPER_H_

#include "CAttaque.h"

using namespace std;

class CBlooper : public CAttaque
{
private:
	
public:
	
	CBlooper(CMqtt* mqtt, int noKart);
	~CBlooper();

	void Init();
	void Run();
};

#endif