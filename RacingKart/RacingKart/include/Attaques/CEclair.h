#ifndef _CECLAIR_H_
#define _CECLAIR_H_

#include "CAttaque.h"

using namespace std;

class CEclair : public CAttaque
{
private:


public:
	
	CEclair(CMqtt *mqtt, int noKart);
	~CEclair();

	void Init();
	void Run();
};

#endif