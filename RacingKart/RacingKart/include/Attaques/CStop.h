#ifndef _CSTOP_H_
#define _CSTOP_H_

#include "CAttaque.h"

using namespace std;

class CStop : public CAttaque
{
private:
	
public:
	
	CStop(CMqtt* mqtt, int noKart);
	~CStop();

	void Init();
	void Run();
	
};

#endif