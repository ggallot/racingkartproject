#pragma once
#include <GL/glut.h>  // glut.h header file from freeglut\include\GL folder
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class OBJLoader
{
private:
	static vector<string> split(string s, string delimiter);

public:
	static GLuint LoadFile(string objfile);

};

