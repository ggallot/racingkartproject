#pragma once

#include "config.h"
#include "CPorte.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <Windows.h>
#include <GL/GLU.h>
#include "../include/CBonus.h"
#include "../include/CStart.h"
#include "../include/Circuit/CARBarrels.h"
#include "../include/Circuit/CARCible.h"

using namespace cv;
using namespace std;

/// <summary>
/// Classe de gestion de la communication et du traitement des images
/// </summary>
class CCamPipe
{
private:
	string m_pipe = "";					//Nom du pipe de communication avec la raspberry pour recevoir les images
	int m_NoCam = 0;					//Num�ro de la cam�ra USB [Utilis� pour des tests s'il n'y a pas le kart de connect�]
	VideoCapture m_cap;					//Classe OpenCV pour l'acquisition des images
	bool m_useWebcam = false;			//Indique que l'on utilise le pipe plut�t que la webcam
	
	BOOL m_runThread;					//Bool�en indiquant que les threads peuvent continuer � tourner en boucle
	HANDLE  m_Camthread;				//Handle du thread g�rant la r�ception des images
	HANDLE m_CvThread;					//Handle du thread g�rant la d�tection des Tag AR et leur position 3D

	bool m_requestFrame = false;		//Indique que le thread OpenCV a termin� ses calculs et qu'il attend une nouvelle image � traiter

	Mat m_ImageForTreatement;			//Image � traiter par le thread OpenCV
	CRITICAL_SECTION  m_mutex;			//Mutex de protection d'acc�s aux donn�es

	Mat cameraMatrix;					//Matrice de comportement de la cam�ra. permet de d�terminer la position 3D des Tag � partir de l'image 2D
	Mat distCoeffs;						//Coefficients de distortion de l'image
	float m_Fx;							//Valeur de la focale sur l'axe	X
	float m_Fy;							//Valeur de la focale sur l'axe Y
	float m_Cx;							//Position du centre optique en X dans l'image
	float m_Cy;							//Position du centre optique en Y dans l'image
	float m_K1;							//Param�tre K1 des coefficients de distortion
	float m_K2;							//Param�tre K2 des coefficients de distortion
	float m_P1;							//Param�tre P1 des coefficients de distortion
	float m_P2;							//Param�tre P2 des coefficients de distortion
	float m_K3;							//Param�tre K3 des coefficients de distortion

	CBonus* m_bonus;
	CStart* m_start;

	void initCameraView();
	int Contains(int id);

	bool m_cameraConnected;				//Indique que la cam�ra est connect�e

	unsigned long WINAPI ThreadCam(void* param);
	static DWORD WINAPI _CamThreadEntry(LPVOID);
	bool m_threadCamRunning = false;

	unsigned long WINAPI ThreadOpenCV(void* param);
	static DWORD WINAPI _OpenCVThreadEntry(LPVOID);
	bool m_ThreadOpenCVRunning = false;

public:
	//Constructeur avec un pipe
	CCamPipe(string port, Mat* frame);

	//constructeur pou l'utilisation d'une Webcam
	CCamPipe(int NoCam, Mat* frame);

	~CCamPipe();

	void lock();
	void unlock();

	void start();
	void stop();

	bool isCameraConnected() { return m_cameraConnected;  }

	Mat* m_ApplicationFrame;				//Pointeur vers l'image avec r�alit� augment�e qui sera affich�e surla fen�tre du jeu
	Mat m_CameraFrame;						//Image issue de la cam�ra

	GLdouble *m_perspectiveMatrix;			//Matrice de perspective pour faire la correspondance entre la cam�ra r�elle et la cam�ra OpenGL
	std::vector<int> m_markerIds;			//Liste des identifiants de TAG d�tect�s dans l'image
	std::vector<cv::Vec3d> rvecs, tvecs;	//Liste des param�tres de positionnement 3D des tags (position et orientation)

	std::vector<CPorte*> m_listPortes;		//Liste des classes permettant d'afficher les num�ros de portes dans l'image
	CARBarrels* m_ARBarrel;
	CARCible* m_ARCible;
};

