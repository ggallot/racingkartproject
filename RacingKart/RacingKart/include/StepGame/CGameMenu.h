#ifndef _CGAMEMENU_H_
#define _CGAMEMENU_H_

#include "CStep.h"
#include "../CGame.h"

class CGameMenu : public CStep
{
private:
	GLuint m_textureFondMenu = 0;
	int m_selectedMenu;

public:
	CGameMenu();
	~CGameMenu();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
	
};

#endif