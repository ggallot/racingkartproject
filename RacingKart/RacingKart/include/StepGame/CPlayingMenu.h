#ifndef _CPLAYINGMENU_H_
#define _CPLAYINGMENU_H_

#include "CStep.h"
#include "../CGame.h"

class CPlayingMenu : public CStep
{
private:
	GLuint m_textureFondJeu = 0;
	int m_selectedMenu;

public:
	CPlayingMenu();
	~CPlayingMenu();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
	
};

#endif