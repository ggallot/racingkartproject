#ifndef _CCHARGEMENT_H_
#define _CCHARGEMENT_H_

#include "CStep.h"
#include "../CConfiguration.h"

class CChargement : public CStep
{
private:
	GLuint m_textureChargement = 0;
	
	bool m_initOK = false;
	bool m_firstRun = false;
	double m_totalTime = 0;

	CConfiguration* m_config = NULL;
public:

	CChargement();
	~CChargement();

	void LoadLibraries();
	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
};

#endif