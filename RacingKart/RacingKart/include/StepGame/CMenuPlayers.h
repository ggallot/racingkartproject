#ifndef _CMENUPLAYERS_H_
#define _CMENUPLAYERS_H_

#include "CStep.h"
#include "../CGame.h"

class CMenuPlayers : public CStep
{
private:
	GLuint m_textureFondMenu = 0;
	int m_selectedMenu;

public:
	CMenuPlayers();
	~CMenuPlayers();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
	
};

#endif