#ifndef _CCOUNTDOWN_H_
#define _CCOUNTDOWN_H_

#include "CStep.h"

class CCountDown : public CStep
{
private:
	GLuint m_textureFondJeu = 0;
	clock_t mem_time;
	float  m_ellapsed = 0;
	int m_countDown;


public:
	CCountDown();
	~CCountDown();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
	
	
};

#endif