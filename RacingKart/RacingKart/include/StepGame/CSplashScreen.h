#ifndef _CSPLASHSCREEN_H_
#define _CSPLASHSCREEN_H_

#include "CStep.h"

class CSplashScreen : public CStep
{
private:
	GLuint m_textureSplash = 0;
	SDL_Texture* m_textureSplash2 = nullptr;
	bool m_startOK = false;

public:

	CSplashScreen();
	~CSplashScreen();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
};

#endif