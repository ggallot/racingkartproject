#ifndef _CWAITINGPLAYERS_H_
#define _CWAITINGPLAYERS_H_

#include "CStep.h"
#include "../CGame.h"

class CWaitingPlayers : public CStep
{
private:
	GLuint m_textureFondJeu = 0;
	int m_selectedMenu;

public:
	CWaitingPlayers();
	~CWaitingPlayers();

	void Begin();
	void Run(double time_ellapsed, int width, int height);
	void End();


	char UpdateKeyPress(char key);
	string NextState();
	
};

#endif