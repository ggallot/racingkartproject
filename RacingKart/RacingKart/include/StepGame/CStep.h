#ifndef _CSTEP_H_
#define _CSTEP_H_

#include <GL/glut.h>
#include <SDL.h>
#include <SDL_image.h>
#include <string.h>
#include <vector>
#include "../config.h"
#include "../COpenGL.h"
#include "../CSoundManager.h"
#include "../CGame.h"

using namespace std;

/// <summary>
/// classe de base pour la machine � �tats
/// </summary>
class CStep
{
private:
	string m_Id = "";							//Identifiant unique de l'�tat
		

protected:
	bool m_showMenu;							//Indique si le menu doit �tre affich�
	int m_noMenu = 0;							//Identifiant de l'item du menu s�lectionn�
	bool m_isVerticalMenu = true;
	
	CSoundManager* m_soundManager;				//Pointeur vers la classe de gestion des sons

	vector<string> m_fileMenuOff;				//Liste des chemins d'acc�s vers les images du menu lorsque l'item n'est pas s�lectionn�
	vector<string> m_fileMenuOn;				//Liste des chemins d'acc�s vers les images du menu lorsque l'item est s�lectionn�
	vector<char> m_simulatedKey;				//Liste des touches � simuler lorsqu'un item du menu est s�lectionn�
	vector<GLuint> m_textureMenuOff;			//Liste vers les identifiants OpenGL pour les images du menu dans leur �tat non s�lectionn�
	vector<GLuint> m_textureMenuOn;				//Liste vers les identifiants OpenGL pour les images du menu dans leur �tat s�lectionn�

	void RenderFondMenu(GLuint texture);
	void RenderMenu(int width, int height);

public:
	bool m_isInitialStep = false;				//Indique si l'�tat est l'�tat initial
	string m_nextMode;							//Nom du prochain �tat � charger
	int m_ImageWidth = 500;						//Largeur de l'image du menu
	int m_Image_Height = 100;					//Hauteur de l'image du menu
	
	CGame* m_Game;								//Pointeur vers la classe de gestion du jeu

	CStep(string Id);
	~CStep();

	string GetId() {return m_Id;}
	
	virtual void Begin();
	virtual void Run(double time_ellapsed, int width, int height);
	virtual void End();
	
	virtual char UpdateKeyPress(char key);
	virtual string NextState();
};

#endif