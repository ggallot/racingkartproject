#pragma once
#include <iostream>
#include <Windows.h>
#include <vector>
#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <mqtt/MQTTAsync.h>
#include "CConfiguration.h"

#define QOS         1
#define TIMEOUT     10000L

using namespace std;

/// <summary>
/// Classe de gestion de la communication MQTT avec les karts et les joysticks
/// </summary>
class CMqtt
{
private:
	static CMqtt* m_instance;						//Pointeur pour mettre la classe en singleton

	CMqtt();
	~CMqtt();

	CConfiguration* m_config;
	MQTTAsync m_client;								//Classe de gestion du protocole
	
	string  m_GamewillTopic;						//Nom du topic pour le WillMessage

	//Liste des Topics pour chaque kart
	vector<string> m_radioDataReceiveTopic;
	vector<string> m_rpiDataReceiveTopic;
	vector<string> m_radioDisconnectTopic;
	vector<string> m_PCsendAttackTopic;
	vector<string> m_PCsendJoystickTopic;
	vector<string> m_radioUpdateScreenTopic;

	//Liste des valeurs en provenance des joysticks
	vector<int> m_joystick;
	vector<int> m_joySpeed;
	vector<char> m_BonusSendValue;
	vector<char> m_AttackReceivedValue;
	vector<bool> m_radioConnected;

	static void on_connected(void* context, MQTTAsync_successData* response);
	static void on_connection_lost(void* context, char* cause);
	static void on_Subscribe(void* context, MQTTAsync_successData* response);
	static void on_SubscribeFailure(void* context, MQTTAsync_failureData* response);
	static void on_disconnected(void* context, MQTTAsync_successData* response);
	static void on_send(void* context, MQTTAsync_successData* response);
	static void on_delivered(void* context, MQTTAsync_deliveryComplete dt);
	static int  on_message_arrived(void* context, char* topicName, int topicLen, MQTTAsync_message* msg);
	static void onConnectFailure(void* context, MQTTAsync_failureData* response);

	void RegisterToRpi(bool connected);

public:
	static CMqtt* getInstance();

	void Connect();
	void Disconnect();

	int getJoystick(int noKart);
	int getSpeed(int noKart);

	void testSendBonus(int noKart, char val) { m_BonusSendValue[noKart] = val; }
	void testReceiveAttack(int noKart, char val){m_AttackReceivedValue[noKart] = val;}

	char getBonusSendValue(int noKart);
	char getReceiveAttackValue(int noKart);

	bool isRadioConnected(int noKart);
	void SendToRadio(int noKart, string data, bool retain = false);
	void SendToRaspberry(int noKart, string data);

	void SendDataJoystick(int noKart, string data);
};

