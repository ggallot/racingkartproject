#ifndef _CKART_H_
#define _CKART_H_

#include <string>
#include <chrono>
#include "config.h"
#include <GL/glut.h>
#include <opencv2/core/core.hpp>
#include "STLLoader.h"
#include "CCamPipe.h"
#include "COpenGL.h"
#include "CMqtt.h"
#include "CSoundManager.h"
#include "CTirages.h"
#include "Attaques/CAttaque.h"
#include "Attaques/CBlooper.h"
#include "Attaques/CEclair.h"
#include "Attaques/CShield.h"
#include "Attaques/CStop.h"
#include "Attaques/CVolant.h"

#include "CFinish.h"
#include "CVehicule.h"
#include "CJoystickSerial.h"
#include "CLapSerial.h"
#include "CConfiguration.h"

#include "./Circuit/IARelement.h"
#include "./Circuit/CARMissile.h"

//#define _DrawVehicule

using namespace cv;
using namespace std;
using namespace std::chrono;

typedef enum {
	NOTHING,
	JOY_CONNECTED,
	CAMERA_OK,
	ALL_OK
}t_ConnexionStatus;

/// <summary>
/// Classe de gestion du jeu et des affichages pour un kart
/// </summary>
class CKart
{
private:
	int m_noKart = 0;										//Num�ro du kart avec lequel communiquer

	bool m_BackGroundTextureGenerated;						//Permet de savoir si les textures ont �t� initialis�es
	bool m_bonusTextureGenerated;

	void renderBackgroundGL(const cv::Mat& image);
	void renderFront();
	void DrawBonus();

	//Identifiants OpenGL pour les textures
	GLuint m_textureStart;
	GLuint m_textureFond;
	GLuint m_textureRondBonus;
	GLuint m_textureNoJoystick;
	vector<GLuint> m_textureLap;
	string m_currentLap;

	bool m_initChrono = false;
	std::chrono::system_clock::time_point m_initChronoTime;
	std::chrono::system_clock::time_point m_endChronoTime;
	
	vector<GLuint> m_listBonus;
	vector<string> m_listBonusKey;							//Liste des chaines de caract�re � envoyer au kart quand on lance une attaque

	std::map<string, CAttaque*> m_listAttaque;				//Dictionnaire des attaques

	bool m_renderRollingBonus;								//Indique que le tirage al�atoire d'un bonus est en cours

	CJoystickSerial* m_serialPort;							//Port s�rie pour r�cup�rer les donn�es de la t�l�commande a la place de la liaison radio

	CCamPipe* m_camera;										//Pointeur vers la classe g�rant la r�ception et le traitement de la cam�ra dans le kart 
	Mat m_CameraFrame;										//Image issue de la cam�ra
	Mat m_NoCameraFrame;									//Image affich�e tant que la cam�ra n'est pas connect�e

	CSoundManager* m_sound;									//Pointeur vers la classe de gestion des sons
	int m_currentBonus = -1;								//Indique le bonus courant disponible (-1 s'il n'y en a pas)
	int m_lastPorte = 0;									//initialise a la porte N�1 pour �viter de compter un tour de plus au d�marrage
	int m_lap = 1;											//Compteur du nombre de tours r�alis�s
	bool m_PartieTermine = false;							//Indique si la partie est termin�e

	vector<CKart*> m_listOpponent;							//Liste des karts concurrents
	CAttaque* m_attackReceived = nullptr;					//Pointeur vers l'attaque re�ue
	CAttaque* m_bonusAttack = nullptr;						//Pointeur vers l'attaque envoy�e
	CFinish* m_finish = nullptr;							//Pointeur vers la classe affichant le finish � la fin de la partie

	CVehicule* m_vehicule = nullptr;						//Pointeur vers la classe affichant le 3D du kart dans l'image
	CConfiguration* m_config;								//Param�tres de configuration
	
	vector<IARelement *> m_ARelements;						//liste des �l�ment de jeu en AR � ajouter

	CLapSerial* m_LapSerial;

	void BonusActiveParJoueur(char val);

public:
	CMqtt* m_kartMQTT;										//Pointeur vers la classe g�rant la communication MQTT
	t_ConnexionStatus m_kartStatus;							//Statut de la connexion de la cam�ra et du joystick
	bool m_gameStarted = false;								//Indique que le jeu est d�marr�

	CKart(int noKart);
	~CKart();

	void InitRace();
	void StartRace();

	char UpdateKeyPress(char key);

	void RegisterOpponent(CKart* kart);

	void AttaqueRecue(char val);
	void DisplayScreen();
};

#endif