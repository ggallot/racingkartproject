#ifndef _CBARRELS_H_
#define _CBARRELS_H_

#include <opencv2/core/core.hpp>
#include "../COpenGL.h"
#include "../OBJloader.h"

using namespace std;

/// <summary>
/// Classe de gestion des �l�ments en r�alit� augmenter � ajouter sur le circuit
/// </summary>
class CARBarrels
{
private:
	int m_idTag = 40;			//Id du Tag de l'obstacle
	clock_t m_initTime;

	bool m_hit = false;			//Indique si le kart a touch� un barrile

	float m_distanceBarrel = 0.25;
	float m_baseAngle = 0;

	GLuint m_textureBidons = 0;
	GLuint m_3DBidons = 0;

	GLuint m_textureVoiture = 0;
	GLuint m_3DVoiture = 0;
	vector<cv::Vec3d> m_listPositions;

	void Draw(float ellapsed, float x, float y, float z);
public:
	

	CARBarrels();
	~CARBarrels();

	void Update(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb);

	void DrawOpenGL();
	bool GetHistStatus();
};
#endif