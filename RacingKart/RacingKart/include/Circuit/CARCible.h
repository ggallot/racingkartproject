#ifndef _CARCIBLE_H_
#define _CARCIBLE_H_

#include <opencv2/core/core.hpp>
#include "../COpenGL.h"
#include "../OBJloader.h"

using namespace std;

/// <summary>
/// Classe de gestion des �l�ments en r�alit� augmenter � ajouter sur le circuit
/// </summary>
class CARCible
{
private:
	GLuint m_textureCible = 0;
	GLuint m_3DCible = 0;

	vector<cv::Vec3d> m_listPositions;

	cv::Vec3d m_noPosition;

public:
	int m_idTag = 45;			//Id du Tag de l'obstacle

	CARCible();
	~CARCible();

	void Update(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb);

	void DrawOpenGL();

	cv::Vec3d GetCiblePosition();
};
#endif