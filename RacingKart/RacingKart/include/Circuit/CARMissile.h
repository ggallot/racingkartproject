#ifndef _CARMISSILE_H_
#define _CARMISSILE_H_

#include <opencv2/core/core.hpp>
#include "../COpenGL.h"
#include "../OBJloader.h"
#include "IARelement.h"

using namespace std;

/// <summary>
/// Classe de gestion des �l�ments en r�alit� augmenter � ajouter sur le circuit
/// </summary>
class CARMissile : public IARelement
{
private:
	GLuint m_textureMissile = 0;
	GLuint m_3DMissile = 0;

	cv::Vec3d m_position;
	cv::Vec3d m_direction;

	clock_t m_initTime;
	clock_t m_memNow;
	float m_Te = 0;
	
	float m_speed = 1.0f;
	float m_angleMissile = 0.0f;

	float m_err_mem = 0;
	float m_Kp = 0.01f;
	float m_Ki = 0.002f;
	float m_Ti = 0;

	float CorrPI(float err);

public:
	int m_idTag = 45;			//Id du Tag de l'obstacle

	CARMissile();
	~CARMissile();

	void UpdatetargetPosition(cv::Vec3d position);
	void DrawOpenGL();

};
#endif