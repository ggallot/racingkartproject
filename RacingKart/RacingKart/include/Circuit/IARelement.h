#ifndef _IARELEMENT_H_
#define _IARELEMENT_H_



/// <summary>
/// Classe de gestion des �l�ments en r�alit� augmenter � ajouter sur le circuit
/// </summary>
class IARelement
{
private:

public:
	bool m_ended = false;
	bool ended = false;


	IARelement()
	{
		m_ended = false;
		ended = false;
	}

	virtual void UpdatetargetPosition(cv::Vec3d position)
	{

	}


	virtual void DrawOpenGL()
	{

	}

	/// <summary>
	/// Fonction indiquant si l'attaque est termin�e
	/// </summary>
	/// <returns>True si l'attaque est termin�e</returns>
	bool isEnded() {
		if (m_ended && !ended)
		{
			ended = true;
		}
		return m_ended;
	}
};
#endif