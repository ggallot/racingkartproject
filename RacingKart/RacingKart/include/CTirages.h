#ifndef _CTIRAGES_H_
#define _CTIRAGES_H_

#include <cstdlib>
#include <cmath>
#include <random>

using namespace std;

/// <summary>
/// Classe pour r�aliser des tirages al�atoires
/// </summary>
class UniformVar
{
public:
	UniformVar(double min , double max);
	~UniformVar();
	double Tirage();
private:
	random_device  m_generator;
	uniform_real_distribution<double> m_distribution;
};


#endif
