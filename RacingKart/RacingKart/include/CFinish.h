#ifndef _CDEFINE_H_
#define _CDEFINE_H_

#include <GL/glut.h>
#include "COpenGL.h"
#include "config.h"

/// <summary>
/// Classe permettant d'afficher la fin du jeu � l'�cran
/// </summary>
class CFinish
{
private:
	GLuint m_texture = 0;		//Identifiant de texture � afficher
	
public:
	CFinish();
	void Run();
};
#endif // !_CDEFINE_H_
