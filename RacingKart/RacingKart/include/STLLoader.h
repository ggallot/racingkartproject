#pragma once   
#include <GL/glut.h>  // glut.h header file from freeglut\include\GL folder
#include <iostream>
#include <fstream>
#include <windows.h>

using namespace std;

/// <summary>
/// Classe de chargement de fichiers STL binaires
/// </summary>
class STLLoader
{

public:
	static unsigned int LoadFile(string file, double ColorR, double ColorG, double ColorB, string name = "");
};

