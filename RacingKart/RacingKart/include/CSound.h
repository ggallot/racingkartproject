#ifndef _CSOUND_H_
#define _CSOUND_H_

#include <windows.h>
#include <string>
#include <SDL_mixer.h>

using namespace std;

/// <summary>
/// Classe de chargement d'un son 
/// </summary>
class CSound
{
private:
	string m_soundName;					//Nom du son � charger
	HANDLE  m_Soundthread;				//Handle vers le thread de gestion de l'�x�cution du son

	bool m_isMP3 = false;				//Indique si c'est un MP3
	bool m_playing = false;				//Indique si le son est en train d'�tre  jou�
	
	Mix_Music* m_mp3Music = nullptr;	//Pointeur vers le son charg� si c'est un MP3	
	Mix_Chunk* m_wavMusic = nullptr;	//Pointeur vers le son charg� si c'est un wav

	unsigned long WINAPI ThreadSound(void* param);
	static DWORD WINAPI _SoundThreadEntry(LPVOID);


public:
	CSound(string soundName);
	~CSound();

	void Play();
	bool IsPlaying();
};

#endif