#ifndef _CJOYSTICKSERIAL_H_
#define _CJOYSTICKSERIAL_H_
#include <windows.h>
#include <iostream>
#include "CMqtt.h"

using namespace std;

class CJoystickSerial
{
private:
	HANDLE m_SerialThread = 0;
	HANDLE m_serialHandle = 0;
	DWORD m_baudRate = CBR_115200;
	BYTE m_bitSize = DATABITS_8;
	BYTE m_stopBits = ONESTOPBIT;
	BYTE m_parity = NOPARITY;
	string m_delimiter = "\r";
	int m_noKart;
	string m_serialPort;
	bool m_sendDataToKart = true;
	bool m_activeBoost = false;
	bool m_invertSteering = false;
	bool m_stopKart = false;

	CMqtt* m_mqtt;


	bool OpenPort();
	unsigned long WINAPI ThreadSerial(void* param);
	static DWORD WINAPI _SerialThreadEntry(LPVOID);
	bool m_ThreadSerialRunning = false;

public:

	CJoystickSerial(int noKart, string portCom);
	~CJoystickSerial();

	void EnableSendData(bool);
	void ActiveBoost();
	void InverseSteering();
	void StopKart();
	void ResetBonus();

	bool m_connected = false;
	int m_direction = 0;
	int m_speed = 0;
	char m_bonus = '_';
};
#endif