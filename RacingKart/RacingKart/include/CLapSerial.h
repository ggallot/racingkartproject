#ifndef _CLAPSERIAL_H_
#define _CLAPSERIAL_H_
#include <windows.h>
#include <iostream>
#include "CConfiguration.h"

using namespace std;

class CLapSerial
{
private:
	static CLapSerial* m_instance;						//Pointeur pour mettre la classe en singleton
	CLapSerial();
	~CLapSerial();

	HANDLE m_SerialThread = 0;
	HANDLE m_serialHandle = 0;
	DWORD m_baudRate = CBR_115200;
	BYTE m_bitSize = DATABITS_8;
	BYTE m_stopBits = ONESTOPBIT;
	BYTE m_parity = NOPARITY;

	string m_delimiter = "\r";
	string m_serialPort;

	unsigned long WINAPI ThreadSerial(void* param);
	static DWORD WINAPI _SerialThreadEntry(LPVOID);
	bool m_ThreadSerialRunning = false;

	bool m_flagNextLap1 = false;
	bool m_flagNextLap2 = false;

public:
	static CLapSerial* getInstance();
	bool OpenPort();

	void ResetLapCount();
	bool IsNextLap(int noKart);
	bool m_connected = false;
};
#endif