#ifndef _CPORTE_H_
#define _CPORTE_H_

#include "CTag.h"
#include "COpenGL.h"
#include "CMarker.h"
#include "CConfiguration.h"

using namespace std;

/// <summary>
/// Classe de gestion des portes. Permet de d�terminer le num�ro de la porte et sa position 3D
/// </summary>
class CPorte
{
private:
	int m_id1;						//Id du Tag1 sur la porte
	int m_id2;						//Id du Tag2 sur la porte
	CTag m_Tag1;					//Classe de calcul des �l�ments du tag1
	CTag m_Tag2;					//Classe de calcul des �l�ments du tag2

	COpenGL* m_objToDraw;			//Pointeur vers l'objet a positionner par rapport � la porte
	CMarker* m_marker;				//Pointeur vers le num�ro de la porte � afficher par rapport � la position de la porte

	double m_distance;				//Distance de la cam�ra par rapport � la porte
	bool m_getBonus = false;		//Indique si le kart � captur� le bonus

	int m_idPorte;					//identifiant de la porte
	bool m_porteActive = true;		//Indique si la porte est active, c-a-d qu'n peut capturer un bonus

	CConfiguration* m_config;
public:

	CPorte(COpenGL* obj, int idPorte, int idTag1, int idTag2);
	~CPorte();

	void UpdatePorte(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb);

	void DrawOpenGL();
	bool IsGetBonus();
	void ActivePorte();
	void DesactivePorte();
	int GetPorteId();
};

#endif