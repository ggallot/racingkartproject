#ifndef _CGAME_H_
#define _CGAME_H_

#include <opencv2/core/core.hpp>
#include <iostream>
#include <map>
#include <algorithm>
#include <GL/glut.h>
#include <SDL_mixer.h>

#include "COpenGL.h"
#include "config.h"
#include "CMqtt.h"
#include "CLapSerial.h"
#include "CKart.h"
#include "CSoundManager.h"
#include <vector>
using namespace std;

/// <summary>
/// Classe de gestion du jeu
/// </summary>
class CGame
{
private:
	static CGame* m_instance;		//pointeur pour avoir la classe en singleton

	CMqtt* m_mqtt;					//pointeur vers la classe g�rant la communication MQTT
	CLapSerial* m_lapSerial;

	bool m_ShowMenu = false;		//Indique s'il faut afficher le menu

	vector<int> m_listIdKart;

	//Identifiants vers les textures a afficher dans le jeu (d�compte et le petit nuage)
	GLuint m_textureChiffre3, m_textureChiffre2, m_textureChiffre1, m_textureChiffreGo;
	GLuint m_textureNuage3, m_textureNuage2, m_textureNuage1, m_textureNuageGo;

	vector<CKart *>m_listKart;		//Liste vers les classes g�rant le comportement pour chaque Kart

	void DrawOneKart(int countDownValue);
	void DrawTwoKart(int countDownValue);
	void DrawCountDown(int step);
			
	float m_transparency = 0;		//Transparence des textures a afficher


public:
	static CGame* getInstance();
	int m_window_width = 0;			//Largeur de l'�cran
	int m_window_heigth = 0;		//Hauteur de l'�cran

	CGame();
	~CGame();

	void Close();

	void ConnectKart(int nbOfKart);
	void RebootKart();
	void DisconnectKart();
	bool PlayersReady();

	void SetGameStart();
	void SetTransparency(float value) { m_transparency = value; };
	void DrawGameElements(int countDownValue = -1);

	char UpdateKeyPress(char key);
	void SendToAllKart(string val);
	
};
#endif