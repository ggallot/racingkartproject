#ifndef _CBONUS_H_
#define _CBONUS_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <GL/glut.h>

#include "COpenGL.h"

/// <summary>
/// Classe d'affichage des cubes tournants ajout�s en r�alit� augment� � l'�cran
/// </summary>
class CBonus : public COpenGL
{
private:
	clock_t m_initTimer;			//Timer permettant de contr�ler la rotation des cubes
	GLuint m_CubeList;				//Identifiant de la liste OpenGL
	GLuint m_textureBonus = 0;			//Identifiant OpenGL pour la texture du point d'interrogation 

	float m_speedRotation = 100;	//100� par seconde
public:
	CBonus();
	~CBonus();

	void Draw(float x, float y, float z, float Rz);
};

#endif