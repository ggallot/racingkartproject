#ifndef _CMARKER_H_
#define _CMARKER_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <GL/glut.h>

#include "COpenGL.h"

/// <summary>
/// Classe permettant d'afficher le num�ro des portes dans l'image
/// </summary>
class CMarker : public COpenGL
{
private:
	GLuint m_MarkerList;
	GLuint m_textureMarker;

	bool m_textureGenerated = false;
	string m_image;
	double m_speedRotation = 100; //100� par seconde
public:
	CMarker(string image);
	~CMarker();

	void Draw(float x, float y, float z, float Rz);
};

#endif