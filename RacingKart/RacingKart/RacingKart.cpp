// BattleKart.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <winsock2.h>
#include <ws2tcpip.h>
#include <IPTypes.h>
#include <iphlpapi.h>
#include <chrono>
#include <atlstr.h>
#include "resource.h"

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Iphlpapi.lib")

#include "include/config.h"
#include "include/CGame.h"
#include "include/CKart.h"
#include "include/CSoundManager.h"
#include <iostream>


#include <GL/glut.h>  // glut.h header file from freeglut\include\GL folder
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <algorithm>

#include "./include/StepGame/CStep.h"
#include "./include/StepGame/CChargement.h"
#include "./include/StepGame/CSplashScreen.h"
#include "./include/StepGame/CGameMenu.h"
#include "./include/StepGame/CCountDown.h"
#include "./include/StepGame/CMenuPlayers.h"
#include "./include/StepGame/CWaitingPlayers.h"
#include "./include/StepGame/CPlayingMenu.h"

#include "./include/CConfiguration.h"


#undef main
using namespace cv;
using namespace std;

int window_width = 0;
int window_height = 0;
bool m_runApplication = true;
bool is_fullscreen = false;
int pos_x = 250;
int pos_y = 50;

chrono::steady_clock::time_point m_timeStart;

CGame* m_GameSupervisor;
CSoundManager* m_soundManager;
CMqtt* m_kartMQTT;

std::map<std::string, CStep*> m_listStepGame;
string m_currentState = "";


void keyPressed(unsigned char key, int x, int y) {
    
    if (m_currentState != "")
    {
        key = m_listStepGame[m_currentState]->UpdateKeyPress(key);
    }

    if (key == 'f')
    {
        if (!is_fullscreen) {
            glutFullScreen();
        }
        else
        {
            glutReshapeWindow(IMAGE_WIDTH, IMAGE_HEIGHT);
            glutPositionWindow(pos_x, pos_y);
        }
        is_fullscreen = !is_fullscreen;
    }
    //Exit application
    if (key == 'q')
    {
        m_runApplication = false;
        m_GameSupervisor->Close();
        m_soundManager->Close();
        glutDestroyWindow(glutGetWindow());
    }
}


void Reshape(int w, int h)
{
    window_width = w;
    window_height = h;

    m_GameSupervisor->m_window_width = w;
    m_GameSupervisor->m_window_heigth = h;

    //adjusts the pixel rectangle for drawing to be the entire new window    
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0);
    //glMultMatrixd(perspectiveMatrix);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0);
}


void UpdateStepGame()
{
    if (m_currentState != "")
    {
        try
        {
            //On regarde si on a changement d'état
            string NextState = m_listStepGame[m_currentState]->NextState();

            //si l'état Courant est différent du prochain état
            if (NextState != m_currentState)
            {
                m_listStepGame[m_currentState]->End();
                m_currentState = NextState;
                m_listStepGame[m_currentState]->Begin();
            }


        }
        catch (exception e)
        {
            cout << e.what() << endl;
        }
    }
}



void Display_Objects(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    UpdateStepGame();

    if (window_width != 0 && window_height != 0)
    {
        glViewport(0, 0, (GLsizei)window_width, (GLsizei)window_height);
     
        if (m_currentState != "")
        {
            auto timeEnd = chrono::steady_clock::now();
            double diff = (double)(chrono::duration_cast<chrono::milliseconds>(timeEnd - m_timeStart).count()) / 1000.0;
            m_listStepGame[m_currentState]->Run(diff, window_width, window_height);
        }
    }
    
    m_timeStart = chrono::steady_clock::now();
    glFlush();
    glutSwapBuffers();
}

void idle(void) {
    if (!is_fullscreen) {
        pos_x = glutGet(GLUT_WINDOW_X);
        pos_y = glutGet(GLUT_WINDOW_Y);
    }
}

void glutTimer(int value)
{
    glutPostRedisplay();
    glutTimerFunc(40, glutTimer, 1);
}

void initOpenGL()
{
    // initialize glut    

    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  
    // set window size  

    window_width = IMAGE_WIDTH;
    window_height = IMAGE_HEIGHT;
    m_GameSupervisor->m_window_width = IMAGE_WIDTH;
    m_GameSupervisor->m_window_heigth = IMAGE_HEIGHT;

    // create window with window text    
    glutCreateWindow("Racing kart");
    HWND hwnd = FindWindow(NULL, _T("Racing kart"));
    HANDLE icon = LoadImage(GetModuleHandle(TEXT("RacingKart.exe")), MAKEINTRESOURCE(IDI_ICON1), IMAGE_ICON, 128, 128, LR_COLOR);
    SendMessage(hwnd, (UINT)WM_SETICON, ICON_BIG, (LPARAM)icon);
    
    glutReshapeWindow(window_width, window_height);
    // set window location    
    glutPositionWindow(pos_x, pos_y);
    
    glutDisplayFunc(Display_Objects);    
    glutReshapeFunc(Reshape);
    glutIdleFunc(idle);
    glutKeyboardFunc(keyPressed);
    
    //fréquence de rafraichissment
    glutTimerFunc(40, glutTimer, 1);
    
    // call Init_OpenGL() function    
    glClearColor(0.0, 0.0, 0.0, 0.0);
    //glShadeModel(GL_FLAT);


    //glDepthFunc(GL_ALWAYS);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);

    GLfloat* L0ambiant = new GLfloat[4]{ 0.8f, 0.8f, 0.8f, 1.0f };
    GLfloat* mat0_diffuse = new GLfloat[4]{ 0.2f, 0.2f, 0.2f, 1.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, L0ambiant);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, mat0_diffuse);
    glEnable(GL_LIGHT0);

    // Light 1
    GLfloat* L1position = new GLfloat[4]{ 0.0f, 0, 1.0f, 1.0f };
    GLfloat* L1ambiant = new GLfloat[4]{ 0.3f, 0.3f, 0.3f, 1.0f };
    glLightfv(GL_LIGHT1, GL_DIFFUSE, L1ambiant);
    glLightfv(GL_LIGHT1, GL_POSITION, L1position);
    glEnable(GL_LIGHT1);

    GLfloat* material_diffuse = new GLfloat[4]{ 0.01f, 0.01f, 0.01f, 1.0f };
    GLfloat* Lnoire = new GLfloat[4]{ 0.0f, 0.0f, 0.0f, 1.0f };
    
    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    glCullFace(GL_FRONT_AND_BACK);
    glFrontFace(GL_CCW);
    glShadeModel(GL_SMOOTH);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Lnoire);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_diffuse);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 10.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //temps_initial = clock();
}


bool SelectComPort() //added function to find the present serial 
{
    TCHAR lpTargetPath[MAX_PATH];
    //LPWSTR lpTargetPath; // buffer to store the path of the COMPORTS
    bool gotPort = false; // in case the port is not found

    for (int i = 0; i < 255; i++) // checking ports from COM0 to COM255
    {
        CString str;
        str.Format(_T("%d"), i);
        CString ComName = CString("COM") + CString(str); // converting to COM0, COM1, COM2

        DWORD test = QueryDosDevice(ComName, lpTargetPath, MAX_PATH);

        // Test the return value and error if any
        if (test != 0) //QueryDosDevice returns zero if it didn't find an object
        {
            std::wcout << str << ": " << lpTargetPath << std::endl;
            gotPort = true;
        }

        if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
        {
        }
    }

    return gotPort;
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    //GetLocalIpAddress();
    //ListIpAddresses();

    m_kartMQTT = CMqtt::getInstance();
 
    //instantiation des classes de la machine d'état du jeu
    m_listStepGame["Chargement"] = new CChargement();
    m_listStepGame["SplashScreen"] = new CSplashScreen();
    m_listStepGame["GameMenu"] = new CGameMenu();
    m_listStepGame["CountDown"] = new CCountDown();
    m_listStepGame["MenuPlayers"] = new CMenuPlayers();
    m_listStepGame["WaitingPlayers"] = new CWaitingPlayers();
    m_listStepGame["PlayingMenu"] = new CPlayingMenu();

    m_currentState = "Chargement";

    m_soundManager = CSoundManager::getInstance();
    m_GameSupervisor = CGame::getInstance();

    CConfiguration* config = CConfiguration::getInstance();

    if (!config->isConfigOK())
    {
        return -1;
    }

    SelectComPort();

    initOpenGL();

    m_timeStart = chrono::steady_clock::now();
    glutMainLoop();
    

    std::cout << "Hello World!\n";
    

    return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
