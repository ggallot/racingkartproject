#include "../../include/StepGame/CSplashScreen.h"

static string m_StepName = "SplashScreen";

CSplashScreen::CSplashScreen() :  CStep(m_StepName)
{
   
}

CSplashScreen::~CSplashScreen()
{
    if (m_textureSplash != 0)
    {
        glDeleteTextures(1, &m_textureSplash);
        m_textureSplash = 0;
    }
}

/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CSplashScreen::Begin()
{
    m_isInitialStep = false;
    m_nextMode = m_StepName;

    
    m_startOK = false;
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CSplashScreen::Run(double time_ellapsed, int width, int height)
{
    
    if (m_textureSplash == 0)
    {
        m_textureSplash = COpenGL::LoadTexturePNG("./images/SplashScreen.png");
    }

    RenderFondMenu(m_textureSplash);
 
    if (m_startOK)
    {
        cout << "Start OK" << endl;
        m_startOK = false;
        m_nextMode = "GameMenu";
    }
}

/// <summary>
/// Fin de l'�tape
/// </summary>
void CSplashScreen::End()
{

}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CSplashScreen::NextState()
{
    return m_nextMode;
}


/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CSplashScreen::UpdateKeyPress(char key)
{
    if (key == 's')
    {
        m_startOK = true;
    }

    return key;
}
