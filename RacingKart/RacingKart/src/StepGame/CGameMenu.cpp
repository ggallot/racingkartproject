#include "../../include/StepGame/CGameMenu.h"

static string m_StepName = "GameMenu";

CGameMenu::CGameMenu() : CStep(m_StepName)
{
    //param�trage des menus
    m_fileMenuOn.push_back("./images/MenuStartOn.png");
    m_fileMenuOn.push_back("./images/MenuExitOn.png");

    m_fileMenuOff.push_back("./images/MenuStartOff.png");
    m_fileMenuOff.push_back("./images/MenuExitOff.png");

    m_simulatedKey.push_back('s');
    m_simulatedKey.push_back('q');

}

CGameMenu::~CGameMenu()
{
    if (m_textureFondMenu != 0)
    {
        glDeleteTextures(1, &m_textureFondMenu);
        m_textureFondMenu = 0;
    }
}


/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CGameMenu::Begin()
{
    m_nextMode = m_StepName; 
    m_showMenu = true;

    m_selectedMenu = -1;
    m_soundManager->StopMusic();
    if (!m_soundManager->IsMusicPlaying()) //Lance la musique s'il n'y en a pas de lanc�
    {
        m_soundManager->StartMusicMenu();
    }
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CGameMenu::Run(double time_ellapsed, int width, int height)
{
    if (m_textureFondMenu == 0)
    {
        m_textureFondMenu = COpenGL::LoadTexturePNG("./images/FondMenu.png");
    }
    RenderFondMenu(m_textureFondMenu);

    RenderMenu(width, height);

    if (m_selectedMenu == 0)
    {
        m_selectedMenu = -1;
        m_nextMode = "MenuPlayers";
    }
}

/// <summary>
/// Fin de l'�tape
/// </summary>
void CGameMenu::End()
{

}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CGameMenu::NextState()
{
    return m_nextMode;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CGameMenu::UpdateKeyPress(char key)
{
    if (key == 'm')
    {
        m_showMenu = !m_showMenu;
    }
    else if (key == '2')
    {
        m_noMenu++;
        if (m_noMenu > 1) m_noMenu = 0;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }
    else if (key == '8') {
        m_noMenu--;
        if (m_noMenu < 0) m_noMenu = 1;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }

    else if (key == 13) {
        m_selectedMenu = m_noMenu;
        m_soundManager->PlaySoundEffect(BK_ENTERMENU);

        key = m_simulatedKey[m_selectedMenu];
    }
    m_Game->UpdateKeyPress(key);
    return key;
}