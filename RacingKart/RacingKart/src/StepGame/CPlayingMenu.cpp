#include "../../include/StepGame/CPlayingMenu.h"

static string m_StepName = "PlayingMenu";

CPlayingMenu::CPlayingMenu() : CStep(m_StepName)
{
    //param�trage des menus
    m_fileMenuOn.push_back("./images/MenuContinuerOn.png");
    m_fileMenuOn.push_back("./images/MenuRecommencerOn.png");
    m_fileMenuOn.push_back("./images/MenuExitOn.png");

    m_fileMenuOff.push_back("./images/MenuContinuerOff.png");
    m_fileMenuOff.push_back("./images/MenuRecommencerOff.png");
    m_fileMenuOff.push_back("./images/MenuExitOff.png");

    m_simulatedKey.push_back(' ');
    m_simulatedKey.push_back('q');

}

CPlayingMenu::~CPlayingMenu()
{
    if (m_textureFondJeu != 0)
    {
        glDeleteTextures(1, &m_textureFondJeu);
        m_textureFondJeu = 0;
    }
}


/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CPlayingMenu::Begin()
{
    m_nextMode = m_StepName; 
    m_showMenu = false;

    m_selectedMenu = -1;

    if (!m_soundManager->IsMusicPlaying()) //Lance la musique s'il n'y en a pas de lanc�
    {
        m_soundManager->StartMusicMenu();
    }
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CPlayingMenu::Run(double time_ellapsed, int width, int height)
{
    if (m_textureFondJeu == 0)
    {
        m_textureFondJeu = COpenGL::LoadTexturePNG("./images/FondJeux.png");
    }
    RenderFondMenu(m_textureFondJeu);
    m_Game->DrawGameElements();
    RenderMenu(width, height);

    if (m_selectedMenu == 0)
    {
        m_selectedMenu = -1;
        m_showMenu = false;
    }
    else if (m_selectedMenu == 1)
    {
        m_selectedMenu = -1;
        m_showMenu = false;
        m_soundManager->StopMusic();
        Sleep(500);
        m_nextMode = "CountDown";

    }
    else if (m_selectedMenu == 2)
    {
        m_selectedMenu = -1;
        m_soundManager->StopMusic();
        Sleep(500);
        m_nextMode = "GameMenu";
        m_Game->DisconnectKart();
    }
}

/// <summary>
/// Fin de l'�tape
/// </summary>
void CPlayingMenu::End()
{
    
}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CPlayingMenu::NextState()
{
    return m_nextMode;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CPlayingMenu::UpdateKeyPress(char key)
{
    if (key == 'm')
    {
        m_showMenu = true;
    }
    else if (key == '2' && m_showMenu)
    {
        m_noMenu++;
        if (m_noMenu > 2) m_noMenu = 0;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }
    else if (key == '8' && m_showMenu) {
        m_noMenu--;
        if (m_noMenu < 0) m_noMenu = 2;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }

    else if (key == 13 && m_showMenu) {
        m_selectedMenu = m_noMenu;
        m_soundManager->PlaySoundEffect(BK_ENTERMENU);
    }
    m_Game->UpdateKeyPress(key);
    return key;
}