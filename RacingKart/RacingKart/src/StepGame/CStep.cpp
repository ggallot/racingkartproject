#include "../../include/StepGame/CStep.h"

CStep::CStep(string Id)
{
    m_Id = Id;
	m_nextMode = m_Id;

    //chargement de la classe de gestion des sons
    m_soundManager = CSoundManager::getInstance();
    m_showMenu = true;

    m_Game = CGame::getInstance();
}

CStep::~CStep()
{
   
    for (size_t i = 0; i < m_textureMenuOn.size(); i++)
    {
        glDeleteTextures(1, &m_textureMenuOn[i]);
        glDeleteTextures(1, &m_textureMenuOff[i]);
    }
    m_textureMenuOn.clear();
    m_textureMenuOff.clear();

    m_fileMenuOff.clear();
    m_fileMenuOn.clear();
}


/// <summary>
/// Fonction appel�e lorsque ce menu est lanc� pour la premi�re fois
/// </summary>
void CStep::Begin()
{
    m_noMenu = 0;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CStep::UpdateKeyPress(char key)
{
    if (key == '8')
    {
        m_noMenu--;
        if (m_noMenu < 0) m_noMenu = 2;
    }

    if (key == '2')
    {
        m_noMenu++;
        if (m_noMenu > 2) m_noMenu = 0;
    }

    //appui sur entr�e
    if ((int)key == 13)
    {
        key = m_simulatedKey[m_noMenu];

    }

    return key;
}

/// <summary>
/// Fonction virtuelle appel�e � chaque rafraichissement de l'�cran
/// </summary>
/// <param name="time_ellapsed">Temps pass� depuis le dernier appel</param>
/// <param name="width">largeur de l'ecran</param>
/// <param name="height">hauteur de l'�cran</param>
void CStep::Run(double time_ellapsed, int width, int height)
{
	
}

/// <summary>
/// Fonction appel�e � la fin de l'�tape et avant de passer � la prochaine
/// </summary>
void CStep::End()
{
	
}

/// <summary>
/// 
/// </summary>
/// <returns></returns>
string CStep::NextState()
{
    return "";
}

/// <summary>
/// Affichage de l'image de fond
/// </summary>
/// <param name="texture"></param>
void CStep::RenderFondMenu(GLuint texture)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

   
    glBegin(GL_QUADS);
    glColor4f(1, 1, 1, 1);
    glTexCoord2f(0, 0); glVertex3f(0, 0, -9.999f);
    glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, 0, -9.999f);
    glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, -9.999f);
    glTexCoord2f(0, 1); glVertex3f(0, IMAGE_HEIGHT, -9.999f);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/// <summary>
/// M�thodes d'affichage du menu
/// </summary>
void CStep::RenderMenu(int width, int height)
{
    if (!m_showMenu) return;

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, 0, height, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    if (m_textureMenuOff.size() == 0)
    {
        for (size_t i = 0; i < m_fileMenuOff.size(); i++)
        {
            m_textureMenuOn.push_back(COpenGL::LoadTexturePNG(m_fileMenuOn[i].c_str()));
            m_textureMenuOff.push_back(COpenGL::LoadTexturePNG(m_fileMenuOff[i].c_str()));
        }
    }

    GLfloat centerX = width / 2.0f;
    GLfloat centerY = height / 2.0f;

    GLfloat sizeByMenu, centerMenu;
    GLfloat debX = 0;
    GLfloat debY = 0;

    if (m_isVerticalMenu)
    {
        sizeByMenu = std::floorf(height / (float)m_textureMenuOff.size());
        centerMenu = sizeByMenu / 2.0f;
        debY = height - centerMenu;
        debX = centerX;
    }
    else
    {
        sizeByMenu = std::floorf(width / (float)m_textureMenuOff.size());
        centerMenu = sizeByMenu / 2.0f;
        debY = centerY;
        debX = centerMenu;
    }
   

    for (size_t i = 0; i < m_textureMenuOff.size(); i++)
    {
        if (m_noMenu == i) glBindTexture(GL_TEXTURE_2D, m_textureMenuOn[i]);
        else glBindTexture(GL_TEXTURE_2D, m_textureMenuOff[i]);


        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 0.9f);
        glTexCoord2f(0, 0); glVertex3f(debX - m_ImageWidth / 2, debY - m_Image_Height / 2, 9.999f);
        glTexCoord2f(1, 0); glVertex3f(debX + m_ImageWidth / 2, debY - m_Image_Height / 2, 9.999f);
        glTexCoord2f(1, 1); glVertex3f(debX + m_ImageWidth / 2, debY + m_Image_Height / 2, 9.999f);
        glTexCoord2f(0, 1); glVertex3f(debX - m_ImageWidth / 2, debY + m_Image_Height / 2, 9.999f);
        glEnd();
        glDisable(GL_TEXTURE_2D);

        if (m_isVerticalMenu)
        {
            debY -= sizeByMenu;
        }
        else
        {
            debX += sizeByMenu;
        }

    }
    glPopMatrix();
}