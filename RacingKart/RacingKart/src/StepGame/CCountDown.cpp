#include "../../include/StepGame/CCountDown.h"

static string m_StepName = "CountDown";

CCountDown::CCountDown() : CStep(m_StepName)
{
    
}

CCountDown::~CCountDown()
{
    if (m_textureFondJeu != 0)
    {
        glDeleteTextures(1, &m_textureFondJeu);
        m_textureFondJeu = 0;
    }
}


/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CCountDown::Begin()
{
    m_nextMode = m_StepName; 
    mem_time = clock();
    m_countDown = 100;
    m_Game->SendToAllKart("_");
    m_Game->RebootKart();

    m_soundManager->StartMusicGame(); //d�marrage de la musique de fond pour le jeu
        
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CCountDown::Run(double time_ellapsed, int width, int height)
{
    float transparency;
    if (m_textureFondJeu == 0)
    {
        m_textureFondJeu = COpenGL::LoadTexturePNG("./images/FondJeux.png");
    }
    RenderFondMenu(m_textureFondJeu);

    m_ellapsed = ((float)clock() - (float)mem_time) / (float)CLOCKS_PER_SEC;

    //Affichage du compte � rebours
    switch (m_countDown)
    {
    
    case 100:
        transparency = m_ellapsed;
        if (m_ellapsed >= 1.0)
        {
            mem_time = clock();
            m_countDown = 99;
        }
        break;

    case 99:
        transparency = 1.0;
        if (m_ellapsed >= 2.0)
        {
            mem_time = clock();
            m_countDown = 3;

            m_soundManager->PlaySoundEffect(BK_SOUND_COUNTDOWN);
            m_Game->SendToAllKart("3"); //demande d'affichage du chiffre sur la t�l�commande
        }
        break;

    case 3:
        transparency = 1.0f - m_ellapsed;

        if (m_ellapsed >= 1.0)
        {
            mem_time = clock();
            m_countDown--;

            m_Game->SendToAllKart("2"); //demande d'affichage du chiffre sur la t�l�commande
        }
        break;

    case 2:
        transparency = 1.0f - m_ellapsed;
        if (m_ellapsed >= 1.0)
        {
            mem_time = clock();
            m_countDown--;

            m_Game->SendToAllKart("1"); //demande d'affichage du chiffre sur la t�l�commande
        }
        break;

    case 1:
        transparency = 1.0f - m_ellapsed;

        if (m_ellapsed >= 1.0)
        {
            transparency = 1.0f;
            mem_time = clock();
            m_countDown--;
            m_Game->SendToAllKart("G"); //demande d'affichage du go sur la t�l�commande
        }
        break;

    case 0:
        transparency = 1.0f - m_ellapsed;
        if (m_ellapsed >= 1.0)
        {
            mem_time = clock();
            m_countDown--;
            m_Game->SendToAllKart("_"); // demande d'effacer le chiffre sur la t�l�commande
            m_Game->SetGameStart();
            m_nextMode = "PlayingMenu";
        }
        break;

    default:
        break;
    }

    m_Game->SetTransparency(transparency);
    m_Game->DrawGameElements(m_countDown);
}


/// <summary>
/// Fin de l'�tape
/// </summary>
void CCountDown::End()
{

}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CCountDown::NextState()
{
    return m_nextMode;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CCountDown::UpdateKeyPress(char key)
{
    return key;
}