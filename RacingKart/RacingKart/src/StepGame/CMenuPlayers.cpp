#include "../../include/StepGame/CMenuPlayers.h"

static string m_StepName = "MenuPlayers";

CMenuPlayers::CMenuPlayers() : CStep(m_StepName)
{
    //param�trage des menus
    m_fileMenuOn.push_back("./images/OnePlayerOn.png");
    m_fileMenuOn.push_back("./images/TwoPlayerOn.png");
   
    m_fileMenuOff.push_back("./images/OnePlayerOff.png");
    m_fileMenuOff.push_back("./images/TwoPlayerOff.png");
   

    m_simulatedKey.push_back('s');
    m_simulatedKey.push_back(' ');

    m_ImageWidth = 500;
    m_Image_Height = 500;
    
    m_isVerticalMenu = false;
}

CMenuPlayers::~CMenuPlayers()
{
    if (m_textureFondMenu != 0)
    {
        glDeleteTextures(1, &m_textureFondMenu);
        m_textureFondMenu = 0;
    }
}


/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CMenuPlayers::Begin()
{
    m_nextMode = m_StepName; 
    m_showMenu = true;

    m_selectedMenu = -1;
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CMenuPlayers::Run(double time_ellapsed, int width, int height)
{
    if (m_textureFondMenu == 0)
    {
        m_textureFondMenu = COpenGL::LoadTexturePNG("./images/FondMenu.png");
    }
    RenderFondMenu(m_textureFondMenu);

    RenderMenu(width, height);

    if (m_selectedMenu == 0)
    {
        m_selectedMenu = -1;
        m_Game->ConnectKart(1);             //Param�tre le jeu pour n'avoir qu'un joueur
        m_nextMode = "WaitingPlayers";
    }
    else if (m_selectedMenu == 1)
    {
        m_selectedMenu = -1;
        m_Game->ConnectKart(2);             //Param�tre le jeu pour avoir deux joueurs
        m_nextMode = "WaitingPlayers";
    }
    else if (m_selectedMenu == 255)
    {
        m_selectedMenu = -1;
        m_nextMode = "GameMenu";
    }
}

/// <summary>
/// Fin de l'�tape
/// </summary>
void CMenuPlayers::End()
{

}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CMenuPlayers::NextState()
{
    return m_nextMode;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CMenuPlayers::UpdateKeyPress(char key)
{
    if (key == '6')
    {
        m_noMenu++;
        if (m_noMenu > 1) m_noMenu = 0;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }
    else if (key == '4') {
        m_noMenu--;
        if (m_noMenu < 0) m_noMenu = 1;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }

    else if (key == 13) {
        m_selectedMenu = m_noMenu;
        m_soundManager->PlaySoundEffect(BK_ENTERMENU);
    }
    else if (key == 27) {
        m_selectedMenu = 255;
        m_soundManager->PlaySoundEffect(BK_RETURNMENU);
    }

    return key;
}