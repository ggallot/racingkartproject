#include "../../include/StepGame/CWaitingPlayers.h"

static string m_StepName = "WaitingPlayers";

CWaitingPlayers::CWaitingPlayers() : CStep(m_StepName)
{
    //param�trage des menus
    m_fileMenuOn.push_back("./images/MenuStartOn.png");
    m_fileMenuOff.push_back("./images/MenuStartOff.png");

}

CWaitingPlayers::~CWaitingPlayers()
{
    if (m_textureFondJeu != 0)
    {
        glDeleteTextures(1, &m_textureFondJeu);
        m_textureFondJeu = 0;
    }
}


/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CWaitingPlayers::Begin()
{
    m_nextMode = m_StepName; 
    m_showMenu = true;

    m_selectedMenu = -1;
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CWaitingPlayers::Run(double time_ellapsed, int width, int height)
{
    if (m_textureFondJeu == 0)
    {
        m_textureFondJeu = COpenGL::LoadTexturePNG("./images/FondJeux.png");
    }
    RenderFondMenu(m_textureFondJeu);

    m_Game->DrawGameElements();
    
    
    if (m_Game->PlayersReady())
    {    
        RenderMenu(width, height);
        if (m_selectedMenu == 0)
        {
            m_soundManager->StopMusic();

            m_selectedMenu = -1;
            m_nextMode = "CountDown";
        }
    } 

    if (m_selectedMenu == 255)
    {
        m_selectedMenu = -1;
        m_soundManager->StopMusic();
        m_Game->DisconnectKart();
        Sleep(500);
        m_nextMode = "GameMenu";
    }
}

/// <summary>
/// Fin de l'�tape
/// </summary>
void CWaitingPlayers::End()
{
    
}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CWaitingPlayers::NextState()
{
    return m_nextMode;
}

/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CWaitingPlayers::UpdateKeyPress(char key)
{
    if (key == 'm')
    {
        m_showMenu = !m_showMenu;
    }
    else if (key == '2')
    {
        m_noMenu++;
        if (m_noMenu > 1) m_noMenu = 0;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }
    else if (key == '8') {
        m_noMenu--;
        if (m_noMenu < 0) m_noMenu = 1;
        m_soundManager->PlaySoundEffect(BK_CHANGEMENU);
    }
    else if (key == 13) {
        m_selectedMenu = m_noMenu;
        m_soundManager->PlaySoundEffect(BK_ENTERMENU);
    }
    else if (key == 27) {
        m_selectedMenu = 255;
        m_soundManager->PlaySoundEffect(BK_RETURNMENU);
    }
    return key;
}