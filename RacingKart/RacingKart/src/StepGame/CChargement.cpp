#include "../../include/StepGame/CChargement.h"

static string m_StepName = "Chargement";

CChargement::CChargement() :  CStep(m_StepName)
{
    m_config = CConfiguration::getInstance();
}

CChargement::~CChargement()
{
    if (m_textureChargement != 0)
    {
        glDeleteTextures(1, &m_textureChargement);
        m_textureChargement = 0;
    }
}

/// <summary>
/// Initialisation de l'�tape
/// </summary>
void CChargement::Begin()
{
    m_isInitialStep = true; //On indique que c'est l'�tape initiale
    m_nextMode = m_StepName;

    m_totalTime = 0;
    m_initOK = false;
}

/// <summary>
/// M�thode cyclique
/// </summary>
/// <param name="time_ellapsed"></param>
void CChargement::Run(double time_ellapsed, int width, int height)
{
    m_totalTime += time_ellapsed;

    if (m_textureChargement == 0)
    {
        m_textureChargement = COpenGL::LoadTexturePNG("./images/Chargement.png");
    }

    RenderFondMenu(m_textureChargement);

    if (!m_firstRun && m_totalTime > 0.5)
    {
        m_firstRun = true;
        LoadLibraries();
    }

    if (m_initOK)
    {
        m_initOK = false;
        m_nextMode = "SplashScreen";
    }
}

/// <summary>
/// chargement des librairies gstreamer au d�marrage du jeu pour �viter les temps de chargement ensuite
/// </summary>
void CChargement::LoadLibraries()
{
    vector<string> listLibrary;
    listLibrary.push_back(".\\gstreamer\\gstudp.dll");
    listLibrary.push_back(".\\gstreamer\\gstnet-1.0-0.dll");
    listLibrary.push_back(".\\gstreamer\\gio-2.0-0.dll");
    listLibrary.push_back(".\\gstreamer\\gstrtp.dll");
    listLibrary.push_back(".\\gstreamer\\gstrtp-1.0-0.dll");
    listLibrary.push_back(".\\gstreamer\\gstlibav.dll");
    listLibrary.push_back(".\\gstreamer\\avutil-56.dll");
    listLibrary.push_back(".\\gstreamer\\avcodec-58.dll");
    listLibrary.push_back(".\\gstreamer\\avformat-58.dll");
    listLibrary.push_back(".\\gstreamer\\avfilter-7.dll");
    listLibrary.push_back(".\\gstreamer\\swresample-3.dll");
    listLibrary.push_back(".\\gstreamer\\libwinpthread-1.dll");
    listLibrary.push_back(".\\gstreamer\\bz2.dll");
    listLibrary.push_back(".\\gstreamer\\gstvideoconvert.dll");
    listLibrary.push_back(".\\gstreamer\\gstapp.dll");
    listLibrary.push_back(".\\gstreamer\\gstcoreelements.dll");

    for (int i = 0; i < listLibrary.size(); i++)
    {
        HINSTANCE hGetProcIDDLL = LoadLibraryA(listLibrary[i].c_str());
        if (!hGetProcIDDLL) {
            std::cout << "could not load the dynamic library " << GetLastError() << std::endl;
        }
    }

    m_initOK = true;
}


/// <summary>
/// Fin de l'�tape
/// </summary>
void CChargement::End()
{

}

/// <summary>
/// Indique quelle sera la prochaine �tape
/// </summary>
/// <returns></returns>
string CChargement::NextState()
{
    return m_nextMode;
}


/// <summary>
/// L'utilisateur a appuy� sur une touche
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CChargement::UpdateKeyPress(char key)
{
    return key;
}
