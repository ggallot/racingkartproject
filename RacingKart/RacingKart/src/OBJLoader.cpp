#include "../include/OBJLoader.h"

/// <summary>
/// Chargement d'un fichier STL cod� en binaire
/// </summary>
/// <param name="objfile">Chemin d'acc�s du fichier obj</param>
/// <returns>Identifiant OpenGL de l'objet</returns>
GLuint OBJLoader::LoadFile(string objfile)
{
    FILE* file;
    vector<float> listVertex;
    vector<float> listNormale;
    vector<float> listCoordTexture;

    ifstream infile(objfile);
    string line;
    string delimiter = " ";

    GLuint ObjObject = glGenLists(1);
    glNewList(ObjObject, GL_COMPILE);

    glEnable(GL_TEXTURE_2D);
    glColor3d(1, 1, 1);

    int oldNbVertex = 0;

    while (getline(infile, line))
    {
        vector<string> v = split(line, delimiter);

        if (v[0] == "v")
        {
            int idx = 1;
            if (v[1].length() == 0)
            {
                idx++;
            }
            float x = atof(v[idx].c_str());
            float y = atof(v[idx + 1].c_str());
            float z = atof(v[idx + 2].c_str());

            listVertex.push_back(x);
            listVertex.push_back(y);
            listVertex.push_back(z);
        }
        else if (v[0] == "vt")
        {
            float x = atof(v[1].c_str());
            float y = atof(v[2].c_str());

            listCoordTexture.push_back(x);
            listCoordTexture.push_back(y);
        }
        else if (v[0] == "vn")
        {
            float x = atof(v[1].c_str());
            float y = atof(v[2].c_str());
            float z = atof(v[3].c_str());

            listNormale.push_back(x);
            listNormale.push_back(y);
            listNormale.push_back(z);
        }
        else if (v[0] == "f")
        {
            int NbVertex = v.size() - 1;

            int t = v[NbVertex].length();
            if (v[NbVertex].length() == 0)
            {
                NbVertex--;
            }

            if (NbVertex != oldNbVertex)
            {
                if (oldNbVertex > 0) glEnd();

                if (NbVertex == 3) glBegin(GL_TRIANGLES);
                if (NbVertex == 4) glBegin(GL_QUADS);
                oldNbVertex = NbVertex;
            }


            for (int k = 1; k <= NbVertex; k++)
            {
                vector<string> sommet = split(v[k], "/");

                int idVertex = (atoi(sommet[0].c_str()) - 1) * 3;
                int idCoord = (atoi(sommet[1].c_str()) - 1) * 2;
                int idNormal = (atoi(sommet[2].c_str()) - 1) * 3;

                glTexCoord2f(listCoordTexture[idCoord], listCoordTexture[idCoord + 1]);
                glVertex3f(listVertex[idVertex], listVertex[idVertex + 1], listVertex[idVertex + 2]);
                glNormal3f(listNormale[idNormal], listNormale[idNormal + 1], listNormale[idNormal + 2]);
            }
        }
    }
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glEndList();
    
    return ObjObject;
}


vector<string> OBJLoader::split(string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}