#include "../include/STLLoader.h"

/// <summary>
/// Chargement d'un fichier STL cod� en binaire
/// </summary>
/// <param name="file">Chemin d'acc�s du fichier</param>
/// <param name="ColorR">Composante rouge de la couleur de l'objet</param>
/// <param name="ColorG">Composante verte de la couleur de l'objet</param>
/// <param name="ColorB">Composante bleue de la couleur de l'objet</param>
/// <param name="name">Nom de l'objet</param>
/// <returns>Identifiant OpenGL de l'objet</returns>
unsigned int STLLoader::LoadFile(string file, double ColorR, double ColorG, double ColorB, string name)
{
    unsigned int STLobject = 0;

    ifstream reader;
    try
    {
        reader.exceptions(std::ifstream::failbit);
        reader.open(file, ios::in | ios::binary);

        char buffer[100];
        int32_t nbOfTriangle;
        float vect[3];

        int i, j, k;
        bool opened = reader.is_open();

        if (opened)
        {

            STLobject =  glGenLists(1);
            glNewList(STLobject, GL_COMPILE);
            glBegin(GL_TRIANGLES);
            glColor3d(ColorR, ColorG, ColorB);

            reader.read(buffer, 80);
            reader.read((char*)&nbOfTriangle, sizeof(nbOfTriangle));


            for (i = 0; i < nbOfTriangle; i++)
            {
                //lecture de la normale
                for (j = 0; j < 3; j++)
                {
                    reader.read((char*)&vect[j], sizeof(float));
                }

                glNormal3f(vect[0], vect[1], vect[2]);
                
                  //Lecture des vertex du triangle
                for (j = 0; j < 3; j++)
                {
                    for (k = 0; k < 3; k++)
                    {
                        reader.read((char*)&vect[k], sizeof(float));
                    }

                    glVertex3f(vect[0], vect[1], vect[2]);
                }

                //cout << "OK chargement "<< i <<endl;
                //Lecture de l'attribut
                reader.read(buffer, 2);
            }

            reader.close();
            glEnd();

            glEndList();
        }

    }
    catch (exception e)
    {
        cout << "lecture echec " << endl;
    }


    return STLobject;
}
