#include "../include/CSoundManager.h"
#include <SDL.h>


/* Null, because instance will be initialized on demand. */
CSoundManager* CSoundManager::m_instance = 0;

/// <summary>
/// Chargement de la classe en singleton
/// </summary>
/// <returns></returns>
CSoundManager* CSoundManager::getInstance()
{
    if (m_instance == 0)
    {
        m_instance = new CSoundManager();
    }

    return m_instance;
}

/// <summary>
/// Initialisation du player et chargement des sons du jeu
/// </summary>
CSoundManager::CSoundManager()
{
    unsigned long ThreadId;
   
    m_doPlayingMenuMusic = -1;
    m_doPlayingGameMusic = -1;

    int flags = MIX_INIT_MP3;
    int sts = Mix_Init(flags);

    
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) //Initialisation de l'API Mixer
    {
        cout << "Failed configure audio library" << Mix_GetError() << endl;
    }
    Mix_AllocateChannels(4);

    m_runningThread = true;
    m_Soundthread = CreateThread(NULL, NULL, _GameThreadEntry, (void*)this, NULL, &ThreadId);

    //Chargement des sons et cr�ation des playlists
    m_soundCountdown = new CSound("./sounds/StartGame.wav");
    m_soundBonus = new CSound("./sounds/Bonus.wav");
    m_changeMenu = new CSound("./sounds/ChangeMenu.wav");
    m_enterMenu = new CSound("./sounds/EnterMenu.wav");
    m_returnMenu = new CSound("./sounds/RetourMenu.wav");

    m_listSoundMenu.push_back(new CSound("./sounds/MenuMusic1.mp3"));
    m_listSoundMenu.push_back(new CSound("./sounds/MenuMusic2.mp3"));

    m_listSoundGame.push_back(new CSound("./sounds/GameMusic1.mp3"));
    m_listSoundGame.push_back(new CSound("./sounds/GameMusic2.mp3"));
    m_listSoundGame.push_back(new CSound("./sounds/GameMusic3.mp3"));
    
    
}

CSoundManager::~CSoundManager()
{
}

/// <summary>
/// Nettoyage des �l�ments
/// </summary>
void CSoundManager::Close()
{
    //Terminate thread
    m_runningThread = false;
   
    delete m_soundCountdown;
    delete m_soundBonus;
    delete m_changeMenu;
    delete m_enterMenu;
    delete m_returnMenu;

    Mix_CloseAudio();

    for (size_t i = 0; i < m_listSoundMenu.size(); i++) 
    {
        delete m_listSoundMenu[i];
    }
    m_listSoundMenu.clear();

    for (size_t i = 0; i < m_listSoundGame.size(); i++)
    {
        delete m_listSoundGame[i];
    }
    m_listSoundGame.clear();
}


/// <summary>
/// Chargement al�atoire d'une musique pour les menus
/// </summary>
void CSoundManager::StartMusicMenu()
{
    UniformVar* var = new UniformVar(0, m_listSoundMenu.size() - 1);
    int id = (int)std::round(var->Tirage());
    m_listSoundMenu[id]->Play();
    m_doPlayingMenuMusic = id;
}

/// <summary>
/// Demande d'arr�t d'une musique avec fondu sonore
/// </summary>
void CSoundManager::StopMusic()
{
    m_doPlayingMenuMusic = -1;
    m_doPlayingGameMusic = -1;
    Mix_FadeOutMusic(500);
}

/// <summary>
/// Chargement al�atoire d'une musique pour le jeu
/// </summary>
void CSoundManager::StartMusicGame()
{
    UniformVar* var = new UniformVar(0, m_listSoundGame.size() - 1);
    int id = (int)std::round(var->Tirage());
    m_listSoundGame[id]->Play();
    m_doPlayingGameMusic = id;
}

/// <summary>
/// Indique si une musique est en cours de lecture
/// </summary>
/// <returns></returns>
bool CSoundManager::IsMusicPlaying()
{
    if (Mix_PlayingMusic() == 0) return false;
    else return true;
}

/// <summary>
/// Ex�cution d'un effet sonore
/// </summary>
/// <param name="idSound">Identifiant du son a jouer</param>
void CSoundManager::PlaySoundEffect(int idSound)
{
    switch(idSound)
    {
        case BK_SOUND_COUNTDOWN:
            m_soundCountdown->Play();
            break;

        case BK_SOUND_BONUS:
            m_soundBonus->Play();
            break;

        case BK_CHANGEMENU:
            m_changeMenu->Play();
            break;

        case BK_ENTERMENU:
            m_enterMenu->Play();
            break;

        case BK_RETURNMENU:
            m_returnMenu->Play();
            break;
    }
}

/// <summary>
/// D�termine si un son particuli� est en cours de lecture
/// </summary>
/// <param name="idSound">Identifiant du son</param>
/// <returns></returns>
bool CSoundManager::SoundEffectPlaying(int idSound)
{
    switch (idSound)
    {
    case BK_SOUND_COUNTDOWN:
        return m_soundCountdown->IsPlaying();
        break;

    case BK_SOUND_BONUS:
        return m_soundBonus->IsPlaying();
        break;

    case BK_CHANGEMENU:
        return m_changeMenu->IsPlaying();
        break;

    case BK_ENTERMENU:
        return m_enterMenu->IsPlaying();
        break;

    case BK_RETURNMENU:
        return m_returnMenu->IsPlaying();
        break;
    }
    return false;
}

/// <summary>
/// Thread de gestion des sons
/// </summary>
/// <param name="param"></param>
/// <returns></returns>
unsigned long WINAPI CSoundManager::ThreadSound(void* param)
{
    
    int id;

    while (m_runningThread)
    {
        try
        {
            //Arr�t de l'ancien son et ex�cution du nouveau pour les menus
            if (m_doPlayingMenuMusic != -1 && !this->IsMusicPlaying())
            {
                Mix_FadeOutMusic(10);
                do
                {
                    UniformVar* tirageMenuGame = new UniformVar(0, m_listSoundMenu.size() - 1);
                    id = (int)std::round(tirageMenuGame->Tirage());
                    delete tirageMenuGame;
                } while (id == m_doPlayingMenuMusic);
                m_listSoundMenu[id]->Play();
                m_doPlayingMenuMusic = id;

            }

            //Arr�t de l'ancien son et ex�cution du nouveau pour le jeu
            if (m_doPlayingGameMusic != -1 && !this->IsMusicPlaying())
            {
                Mix_FadeOutMusic(10);
                do
                {
                    UniformVar* tirageSoundGame = new UniformVar(0, m_listSoundGame.size() - 1);
                    id = (int)std::round(tirageSoundGame->Tirage());
                    delete tirageSoundGame;
                    cout << id << endl;
                } while (id == m_doPlayingGameMusic);
                m_listSoundGame[id]->Play();
                m_doPlayingGameMusic = id;
            }
        }
        catch (exception e)
        {

        }

        Sleep(1000);
    }

    return -1;
}


DWORD CSoundManager::_GameThreadEntry(LPVOID lpthis)
{
    CSoundManager* pClass = reinterpret_cast <CSoundManager*>(lpthis);
    if (pClass != NULL) {
        return (DWORD)pClass->ThreadSound(lpthis);
    }
    else
        TerminateThread(0, 0);
    return 1;
}