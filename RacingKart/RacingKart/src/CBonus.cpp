#include "../include/CBonus.h"

CBonus::CBonus()
{
    m_initTimer = clock();
}

CBonus::~CBonus()
{
    if (m_textureBonus != 0)
    {
        glDeleteTextures(1, &m_textureBonus);
        m_textureBonus = 0;
    }
    glDeleteLists(m_CubeList, 1);
}

/// <summary>
/// Affichage des boites bonus dans l'image
/// </summary>
/// <param name="x">Position en X</param>
/// <param name="y">Position en Y</param>
/// <param name="z">Position en Z</param>
/// <param name="Rz">Rotation autour de l'axe vertical</param>
void CBonus::Draw(float x, float y, float z, float Rz)
{
    clock_t now = clock();

    float ellapsed = (float) (now - m_initTimer) / (float)CLOCKS_PER_SEC;
    float angle = m_speedRotation * ellapsed;

    glPushMatrix();
    glTranslatef(x, y + 0.135f, z + 0.05f);
    glRotatef(angle, 0, 1, 0);
    
    
    glRotatef(90, 1, 0, 0);
    glScalef(0.03f, 0.03f, 0.03f);
    
    
    //Cr�ation de la liste pour les boites
    if (m_textureBonus == 0)
    {
        m_textureBonus = LoadTexturePNG("./images/box.png");
        glBindTexture(GL_TEXTURE_2D, m_textureBonus);

        m_CubeList = glGenLists(1);
        glNewList(m_CubeList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);

        glBegin(GL_QUADS);
        glColor3d(1, 1, 1);

        glNormal3f(1, 0, 0);
        glTexCoord2d(0, 0); glVertex3f(1, -1, -1);
        glTexCoord2d(1, 0); glVertex3f(1, 1, -1);
        glTexCoord2d(1, 1); glVertex3f(1, 1, 1);
        glTexCoord2d(0, 1); glVertex3f(1, -1, 1);

        glNormal3f(0, 1, 0);
        glTexCoord2d(0, 0); glVertex3f(1, 1, -1);
        glTexCoord2d(1, 0); glVertex3f(-1, 1, -1);
        glTexCoord2d(1, 1); glVertex3f(-1, 1, 1);
        glTexCoord2d(0, 1); glVertex3f(1, 1, 1);

        glNormal3f(-1, 0, 0);
        glTexCoord2d(0, 0); glVertex3f(-1, 1, -1);
        glTexCoord2d(1, 0); glVertex3f(-1, -1, -1);
        glTexCoord2d(1, 1); glVertex3f(-1, -1, 1);
        glTexCoord2d(0, 1); glVertex3f(-1, 1, 1);

        glNormal3f(0, -1, 0);
        glTexCoord2d(0, 0); glVertex3f(-1, -1, -1);
        glTexCoord2d(1, 0); glVertex3f(1, -1, -1);
        glTexCoord2d(1, 1); glVertex3f(1, -1, 1);
        glTexCoord2d(0, 1); glVertex3f(-1, -1, 1);

        glNormal3f(0, 0, 1);
        glTexCoord2d(0, 0); glVertex3f(1, -1, 1);
        glTexCoord2d(1, 0); glVertex3f(1, 1, 1);
        glTexCoord2d(1, 1); glVertex3f(-1, 1, 1);
        glTexCoord2d(0, 1); glVertex3f(-1, -1, 1);

        glNormal3f(0, 0, -1);
        glTexCoord2d(0, 0); glVertex3f(-1, -1, -1);
        glTexCoord2d(1, 0); glVertex3f(-1, 1, -1);
        glTexCoord2d(1, 1); glVertex3f(1, 1, -1);
        glTexCoord2d(0, 1); glVertex3f(1, -1, -1);

        glEnd();
        glDisable(GL_TEXTURE_2D);
        glEndList();
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, m_textureBonus);
        glCallList(m_CubeList);
    }

    //Affichage cons�cutif des 3 boites
    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y + 0.135f, z + 0.05f);
    glRotatef(Rz, 0, 1, 0);

    glTranslatef(-0.15f, 0, 0);

    glRotatef(angle, 0, 1, 0);

    glRotatef(90, 1, 0, 0);
    glScalef(0.03f, 0.03f, 0.03f);
    glCallList(m_CubeList);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y + 0.135f, z + 0.05f);
    glRotatef(Rz, 0, 1, 0);

    glTranslatef(0.15f, 0, 0);

    glRotatef(angle, 0, 1, 0);

    glRotatef(90, 1, 0, 0);
    glScalef(0.03f, 0.03f, 0.03f);
    glCallList(m_CubeList);
    glPopMatrix();

}