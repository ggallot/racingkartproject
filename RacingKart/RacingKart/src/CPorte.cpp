#include "../include/CPorte.h"

/// <summary>
/// Chargement des images a afficher au dessus des portes et des identifiants de tag les constituants
/// </summary>
/// <param name="obj">pointeur vers les �l�ments a afficher sous la porte</param>
/// <param name="idPorte">Num�ro de la porte</param>
/// <param name="idTag1">Identifiant de l'AR Tag N�1</param>
/// <param name="idTag2">Identifiant de l'AR Tag N�2</param>
CPorte::CPorte(COpenGL* obj, int idPorte,  int idTag1, int idTag2)
{
	m_id1 = idTag1;
	m_id2 = idTag2;
	m_objToDraw = obj;
	m_idPorte = idPorte;
	m_porteActive = true;
	m_distance = 1e12;

	m_config = CConfiguration::getInstance();

	//Chargement des images
	switch (m_idPorte)
	{
	case 0:
		m_marker = new CMarker("images/PorteStart.png");
		break;
	case 1:
		m_marker = new CMarker("images/Porte1.png");
		break;
	case 2:
		m_marker = new CMarker("images/Porte2.png");
		break;
	case 3:
		m_marker = new CMarker("images/Porte3.png");
		break;
	}
}

CPorte::~CPorte()
{
	delete m_marker;
}

/// <summary>
/// Mise a jour d'une porte � partir des tags identifi�s dans l'image
/// </summary>
/// <param name="markerIds">Liste des identifiants de tag d�tect�s</param>
/// <param name="tvecs">Liste des positions 3D des tags</param>
/// <param name="rvecs">Liste des orientation 3D des tags</param>
/// <param name="nb">Nombre de tag d�tect�s dans l'image</param>
void CPorte::UpdatePorte(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb)
{
	size_t found1 = -1;
	size_t found2 = -1;

	//D�termine si les tags d�tect�s appartiennent � la porte
	for (size_t i = 0; i < nb; i++) 
	{
		if (markerIds[i] == m_id1) found1 = i;
		if (markerIds[i] == m_id2) found2 = i;
	}

	//Si le tag 1 a �t� d�tect�, mise � jour de sa position
	if (found1 != -1)
	{
		m_Tag1.UpdatePosition(tvecs[found1], rvecs[found1]);
		m_Tag1.EstimationTagDroit();
	}
	else {
		m_Tag1.UpdateNotFound();
	}

	//Si le tag 2 a �t� d�tect�, mise � jour de sa position
	if (found2 != -1) {
		m_Tag2.UpdatePosition(tvecs[found2], rvecs[found2]);
		m_Tag2.EstimationTagGauche();
	}
	else {
		m_Tag2.UpdateNotFound();
	}

	//Si l'un ou l'autre des tags est d�tect�, calcul de la distance la plus proche
	if (m_Tag1.m_TagIsValid)
	{
		m_distance = std::min(m_distance, m_Tag1.m_position[2]);
	}

	if (m_Tag2.m_TagIsValid)
	{
		m_distance = std::min(m_distance, m_Tag2.m_position[2]);
	}

	//Si la distance est inf�rieure au seuil et que la porte est active (c-a-d qu'on ne vient pas d�j� de la passer)
	//on indique que le bonus est captur�
	if (m_distance < m_config->m_DistanceGetBonus && m_porteActive)
	{
		cout << "Get bonus" << endl;
		m_getBonus = true;
	}
	m_distance = 1e12;
	
}

/// <summary>
/// Indique si un bonus a �t� captur�
/// </summary>
/// <returns></returns>
bool CPorte::IsGetBonus()
{
	bool result = false;
	if (m_getBonus)
	{
		m_getBonus = false;
		result = true;
	}

	return result;
}

/// <summary>
/// Affiche les �l�ments li�s � la porte
/// </summary>
void CPorte::DrawOpenGL()
{
	cv::Vec3d posTag1;
	cv::Vec3d posTag2;

	//D�termine la position et l'orientation de la porte � partir des tag1 et/ou tag2
	if (m_Tag1.m_TagIsValid && !m_Tag2.m_TagIsValid)
	{
		posTag1[0] = m_Tag1.m_position[0];
		posTag1[1] = m_Tag1.m_position[1];
		posTag1[2] = m_Tag1.m_position[2];
		
		posTag2[0] = m_Tag1.m_estimatedPosition[0];
		posTag2[1] = m_Tag1.m_estimatedPosition[1];
		posTag2[2] = m_Tag1.m_estimatedPosition[2];
	}
	else if (!m_Tag1.m_TagIsValid && m_Tag2.m_TagIsValid)
	{
		posTag2[0] = m_Tag2.m_position[0];
		posTag2[1] = m_Tag2.m_position[1];
		posTag2[2] = m_Tag2.m_position[2];

		posTag1[0] = m_Tag2.m_estimatedPosition[0];
		posTag1[1] = m_Tag2.m_estimatedPosition[1];
		posTag1[2] = m_Tag2.m_estimatedPosition[2];
	}
	else if (m_Tag1.m_TagIsValid && m_Tag2.m_TagIsValid)
	{
		posTag1[0] = m_Tag1.m_position[0];
		posTag1[1] = m_Tag1.m_position[1];
		posTag1[2] = m_Tag1.m_position[2];

		posTag2[0] = m_Tag2.m_position[0];
		posTag2[1] = m_Tag2.m_position[1];
		posTag2[2] = m_Tag2.m_position[2];
	}

	//Affichage des objets � la bonne position si au moins un des tags est d�tect�
	if(m_Tag1.m_TagIsValid || m_Tag2.m_TagIsValid)
	{
		float ux = (float)(posTag2[0] - posTag1[0]);
		float uy = (float)(posTag2[1] - posTag1[1]);
		float uz = (float)(posTag2[2] - posTag1[2]);

		float Rz = -atan2(uz, ux) * 180 / 3.14159f;

		float n = sqrt(ux * ux + uy * uy + uz * uz);
		ux /= n;
		uy /= n;
		uz /= n;

		float x = (float)(posTag1[0] + posTag2[0]) / 2.0f;
		float y = (float)(posTag1[1] + posTag2[1]) / 2.0f;
		float z = (float)(posTag1[2] + posTag2[2]) / 2.0f;
	
		//affichage des cadeaux ou du bandeau de start
		if(m_objToDraw != nullptr)
		{
			m_objToDraw->Draw(x, y, z, Rz);
		}

		if (m_marker != nullptr)
		{
			m_marker->Draw(x, y, z, Rz);
		}
	}
}

/// <summary>
/// Rend la porte active. Un bonus pourra �tre r�colt�
/// </summary>
void CPorte::ActivePorte()
{
	m_porteActive = true;
}

/// <summary>
/// D�sactive la porte. Aucun bonus ne pourra �tre r�colt�
/// </summary>
void CPorte::DesactivePorte()
{
	m_porteActive = false;
}

/// <summary>
/// Renvoi le num�ro de la porte
/// </summary>
/// <returns></returns>
int CPorte::GetPorteId()
{
	return m_idPorte;
}