#include "../include/CSound.h"
#include <iostream>

/// <summary>
/// Chargement d'un son depuis un fichier wav ou mp3
/// </summary>
/// <param name="soundName"></param>
CSound::CSound(string soundName)
{
    m_soundName = soundName;

    string extension = m_soundName.substr(soundName.length() - 3, 3);

    if (extension.compare("mp3")==0)
    {
        m_mp3Music = Mix_LoadMUS(m_soundName.c_str());
        m_isMP3 = true;
    }
    else
    {
        m_wavMusic = Mix_LoadWAV(m_soundName.c_str());
    }
}

CSound::~CSound()
{
    if (m_mp3Music != nullptr) {
        Mix_FreeMusic(m_mp3Music);
    }

    if (m_wavMusic != nullptr) {
        Mix_FreeChunk(m_wavMusic);
    }
}

/// <summary>
/// Lancement de la musique dans un thread s�par�
/// </summary>
void CSound::Play()
{
    unsigned long ThreadId;
    m_playing = true;
    m_Soundthread = CreateThread(NULL, NULL, _SoundThreadEntry, (void*)this, NULL, &ThreadId);
}

/// <summary>
/// Indique si le son est toujours jou�
/// </summary>
/// <returns></returns>
bool CSound::IsPlaying()
{
    return m_playing;
}

/// <summary>
/// Joue le son et d�termine si celui-ci est termin� ou non
/// </summary>
/// <param name="param"></param>
/// <returns></returns>
unsigned long WINAPI CSound::ThreadSound(void* param)
{
    if (m_isMP3)
    {     
        Mix_PlayMusic(m_mp3Music, 0);
        while (Mix_PlayingMusic() != 0) {
            Sleep(250);
        }
    }
    else
    {   
        int channel = Mix_PlayChannel(-1, m_wavMusic, 0);
        while (Mix_Playing(channel) != 0) {
            Sleep(250);
        }
        
    }
    m_playing = false;
    return -1;
}

DWORD CSound::_SoundThreadEntry(LPVOID lpthis)
{
    CSound* pClass = reinterpret_cast <CSound*>(lpthis);
    if (pClass != NULL) {
        return (DWORD)pClass->ThreadSound(lpthis);
    }
    else
        TerminateThread(0, 0);
    return 1;
}