#include "../include/COpenGL.h"

COpenGL::COpenGL()
{

}


COpenGL::~COpenGL()
{

}


void COpenGL::Draw(float x, float y, float z, float Rz)
{

}

/// <summary>
/// Fonction de cr�ation d'un texture � partir d'un fichier PNG
/// </summary>
/// <param name="filename">Chemin d'acc�s du fichier</param>
/// <param name="verticalFlip">Indique s'il faut inverser le haut et le bas de l'image</param>
/// <returns>Identifiant OpenGL de la texture</returns>
GLuint COpenGL::LoadTexturePNG(const char* filename, bool verticalFlip)
{
    GLuint texture = 0;
   
    SDL_Surface* image = IMG_Load(filename);
    if (image == nullptr)
    {
        cout << IMG_GetError() << endl;
        return 0;
    }
    
    if (verticalFlip)
    {
        SDL_LockSurface(image);

        int pitch = image->pitch; // row size
        char* temp = new char[pitch]; // intermediate buffer
        char* pixels = (char*)image->pixels;

        for (int i = 0; i < image->h / 2; ++i) {
            // get pointers to the two rows to swap
            char* row1 = pixels + i * pitch;
            char* row2 = pixels + (image->h - i - 1) * pitch;

            // swap rows
            memcpy(temp, row1, pitch);
            memcpy(row1, row2, pitch);
            memcpy(row2, temp, pitch);
        }

        delete[] temp;

        SDL_UnlockSurface(image);
    }

    int width = image->w; 
    int height = image->h;
    
    unsigned char* dataRGBA;
    int size2 = width * height * 4;
    dataRGBA = (unsigned char*)malloc(size2);
    
    int bpp = image->format->BytesPerPixel;

    unsigned char R, G, B, A;

    for (int i = 0; i < width * height; ++i)
    {
        int index2 = i * 4;
        Uint8* p = (Uint8*)image->pixels + i * bpp;
        Uint32 color = p[0] | p[1] << 8 | p[2] << 16;

        SDL_GetRGB(color, image->format, &R, &G, &B);
        dataRGBA[index2] = R;
        dataRGBA[index2 + 1] = G;
        dataRGBA[index2 + 2] = B;
        if (bpp == 3)
        {
            dataRGBA[index2 + 3] = 255;
        }

        if (bpp == 4)
        {
            //SDL_GetRGBA(*p, image->format, &R, &G, &B, &A);
            A = p[3];
            dataRGBA[index2 + 3] = A;
        }
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    //gluBuild2DMipmaps(GL_TEXTURE_2D, 3 , width, height, GL_RGBA, GL_UNSIGNED_BYTE, dataRGBA);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, dataRGBA);

    free(dataRGBA);

    return texture;
}

/// <summary>
/// Chargement d'une texture BMP depuis un fichier
/// </summary>
/// <param name="filename">Chemin d'acc�s du fichier</param>
/// <param name="width">largeur de l'image</param>
/// <param name="height">hauteur de l'image</param>
/// <param name="transparency">Valeur de transparence pour les pixels de couleur (0,255,100)</param>
/// <returns>Identifiant OpenGL de la texture</returns>
GLuint COpenGL::LoadTexture(const char* filename, int width, int height, int transparency)
{
    GLuint texture = 0;

    unsigned char* data;
    unsigned char* dataRGBA;
    FILE* file;
    fopen_s(&file, filename, "rb");

    if (file == NULL) return 0;

    data = (unsigned char*)malloc(54);
    fread(data, 54, 1, file);
    free(data);

    int size = width * height * 3;
    data = (unsigned char*)malloc(size);

    int size2 = width * height * 4;
    dataRGBA = (unsigned char*)malloc(size2);

    fread(data, size, 1, file);
    fclose(file);

    for (int i = 0; i < width * height; ++i)
    {
        int index = i * 3;
        int index2 = i * 4;

        unsigned char B = data[index];
        unsigned char G = data[index + 1];
        unsigned char R = data[index + 2];

        dataRGBA[index2] = R;
        dataRGBA[index2 + 1] = G;
        dataRGBA[index2 + 2] = B;

        if ((int)R == 0 && (int)G == 255 && (int)B == 100)
        {
            dataRGBA[index2 + 3] = 0;
        }
        else dataRGBA[index2 + 3] = transparency;
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    //gluBuild2DMipmaps(GL_TEXTURE_2D, 3 , width, height, GL_RGBA, GL_UNSIGNED_BYTE, dataRGBA);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, dataRGBA);

    free(data);
    free(dataRGBA);
    
    return texture;
}