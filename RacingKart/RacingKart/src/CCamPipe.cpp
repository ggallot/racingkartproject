#include "../include/CCamPipe.h"

/// <summary>
/// Initialisation des param�tres de communication pour r�cup�rer le flux vid�o du kart
/// Param�trage des param�tres de la cam�ra
/// </summary>
/// <param name="port">Port de communication du pipe</param>
/// <param name="frame">Pointeur vers l'image � afficher dans le jeu</param>
CCamPipe::CCamPipe(string port, Mat* frame)
{
    m_cameraConnected = false;
    m_Fx = 659.341f; // 1184.785f;
    m_Fy = 663.550f; // 1191.515f;
    m_Cx = 640.0f; // 635.336f;
    m_Cy = 360; // 515.7026;
    m_K1 = -0.0931f; //0.1683f;
    m_K2 = 0.03100f; // -0.9182f;
    m_P1 = 0.000189f; // 0.00362f;
    m_P2 = 0.00113f; // -0.00199f;
    m_K3 = -0.02537f; // 1.5934f;

    cameraMatrix = Mat(3, 3, CV_32FC1);
    cameraMatrix.ptr<float>(0)[0] = m_Fx;
    cameraMatrix.ptr<float>(1)[1] = m_Fy;

    cameraMatrix.ptr<float>(0)[2] = m_Cx;
    cameraMatrix.ptr<float>(1)[2] = m_Cy;
    cameraMatrix.ptr<float>(2)[2] = 1;

    distCoeffs = Mat(1, 5, CV_32FC1);
    distCoeffs.ptr<float>(0)[0] = m_K1;
    distCoeffs.ptr<float>(0)[1] = m_K2;
    distCoeffs.ptr<float>(0)[2] = m_P1;
    distCoeffs.ptr<float>(0)[3] = m_P2;
    distCoeffs.ptr<float>(0)[4] = m_K3;

    m_ApplicationFrame = frame;
    m_pipe = "udpsrc port = " + port + " ! application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264 ! rtph264depay ! avdec_h264 !videoconvert !appsink";
    m_useWebcam = false;

    initCameraView();
}

/// <summary>
/// Initialisation des param�tres de communication pour r�cup�rer le flux vid�o de la webcam
/// Param�trage des param�tres de la cam�ra
/// </summary>
/// <param name="NoCam">Num�ro de la cam�ra usb</param>
/// <param name="frame">Pointeur vers l'image � afficher dans le jeu</param>
CCamPipe::CCamPipe(int NoCam, Mat* frame)
{
    m_cameraConnected = false;
    m_Fx = 964.277773355599f;
    m_Fy = 966.353357110802f;
    m_Cx = 639.371131385734f;
    m_Cy = 360; // 405.505414522549;
    m_K1 = 0.166011351665122f;
    m_K2 = -0.430982663418529f;
    m_P1 = -0.00233285487693658f;
    m_P2 = -0.00189876265255404f;
    m_K3 = 0;
    m_useWebcam = true;
    m_NoCam = NoCam;

    cameraMatrix = Mat(3, 3, CV_32FC1);
    cameraMatrix.ptr<float>(0)[0] = m_Fx;
    cameraMatrix.ptr<float>(1)[1] = m_Fy;

    cameraMatrix.ptr<float>(0)[2] = m_Cx;
    cameraMatrix.ptr<float>(1)[2] = m_Cy;
    cameraMatrix.ptr<float>(2)[2] = 1;

    distCoeffs = Mat(1, 5, CV_32FC1);
    distCoeffs.ptr<float>(0)[0] = m_K1;
    distCoeffs.ptr<float>(0)[1] = m_K2;
    distCoeffs.ptr<float>(0)[2] = m_P1;
    distCoeffs.ptr<float>(0)[3] = m_P2;
    distCoeffs.ptr<float>(0)[4] = m_K3;

    m_useWebcam = true;

    m_ApplicationFrame = frame;

    initCameraView();
}

/// <summary>
/// Initialisation de la correspondance entre la cam�ra OpenGL et la cam�ra du kart
/// </summary>
void CCamPipe::initCameraView()
{
    InitializeCriticalSection(&m_mutex);

    m_perspectiveMatrix = new GLdouble[16];
    for (int i = 0; i < 16; i++) m_perspectiveMatrix[i] = 0;
    m_perspectiveMatrix[0] = m_Fx / m_Cx;
    m_perspectiveMatrix[5] = m_Fy / m_Cy;
    m_perspectiveMatrix[10] = (double) (WINDOW_NEAR + WINDOW_FAR) / (double) (WINDOW_NEAR - WINDOW_FAR);
    m_perspectiveMatrix[11] = -1;
    m_perspectiveMatrix[14] = (2.0 * WINDOW_FAR * WINDOW_NEAR) / (double) (WINDOW_NEAR - WINDOW_FAR);

    m_CameraFrame = Mat::zeros(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);  

    m_bonus = new CBonus();
    m_start = new CStart();
}

CCamPipe::~CCamPipe()
{
    stop();

    for (size_t i = 0; i < m_listPortes.size(); i++) 
    {
        delete m_listPortes[i];
    }
    m_listPortes.clear();

    delete m_ARBarrel;
    delete m_ARCible;

    delete m_bonus;
    delete m_start;
    delete m_perspectiveMatrix;
}

/// <summary>
/// Initialisation des portes et lancement des threads de traitement
/// </summary>
void CCamPipe::start()
{
    unsigned long ThreadId;
    unsigned long ThreadId2;
    
    for (int i = 0; i < 4; i++) 
    {
        int porteNb = i;
        int tag1 = i + 1;
        int tag2 = 11 + i;
        if(i==0) m_listPortes.push_back(new CPorte(m_start, porteNb, tag1, tag2));
        else m_listPortes.push_back(new CPorte(m_bonus, porteNb, tag1, tag2));
    }

    m_ARBarrel = new CARBarrels();
    m_ARCible = new CARCible();

    m_runThread = true;

    this->m_Camthread = CreateThread(NULL, NULL, _CamThreadEntry, (void*)this, NULL, &ThreadId);
    if (this->m_Camthread == NULL) {
        cout << "CreateThread a �chou� avec l'erreur " << GetLastError();
    }

    m_CvThread = CreateThread(NULL, NULL, _OpenCVThreadEntry, (void*)this, NULL, &ThreadId2);
    
}

void CCamPipe::stop()
{
    m_runThread = false;
    
    while (m_ThreadOpenCVRunning || m_threadCamRunning)
    {
        Sleep(100);
    }
}

void CCamPipe::lock()
{
    EnterCriticalSection(&m_mutex);
}

void CCamPipe::unlock()
{
    LeaveCriticalSection(&m_mutex);
}

/// <summary>
/// Indique si l'image contient le num�ro de tag
/// </summary>
/// <param name="id">Num�ro du tag</param>
/// <returns></returns>
int CCamPipe::Contains(int id)
{
    int result = -1;

    auto it = find(m_markerIds.begin(), m_markerIds.end(), id);

    if (it != m_markerIds.end())
    {
        result = (int) (it - m_markerIds.begin());
    }

    return result;
}

/// <summary>
/// Thread de r�cup�ration des images en provenance de la cam�ra
/// Envoi de l'image courante � l'autre thread quand celui-ci n'a plus rien a traiter
/// </summary>
/// <param name="param"></param>
/// <returns></returns>
unsigned long WINAPI CCamPipe::ThreadCam(void* param)
{
    Mat frame;
    Mat temp;
 
    if (m_useWebcam)
    {
       
        m_cap.open(m_NoCam);
        m_cap.set(cv::CAP_PROP_FRAME_WIDTH, IMAGE_WIDTH);
        m_cap.set(cv::CAP_PROP_FRAME_HEIGHT, IMAGE_HEIGHT);
    }
    else
    {
        m_cap.open(m_pipe, CAP_GSTREAMER);
    }


    if (!m_cap.isOpened())
    {
        m_runThread = false;
    }

    cv::Rect roi_rect = cv::Rect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
    m_requestFrame = true;
    m_cameraConnected = true;

    m_threadCamRunning = true;
    while (m_runThread)
    {
        m_cap >> frame;
        
        lock();
        try
        {
            //copie de l'image dans l'image de l'application
            if (m_ApplicationFrame != NULL)
            {            
                temp = (*m_ApplicationFrame)(roi_rect);

                if (m_requestFrame)
                {
                    m_ImageForTreatement.copyTo(temp);
                }
               
            }
        }
        catch (Exception e)
        {

        }
        unlock();

        if (m_requestFrame)
        {
            frame.copyTo(m_ImageForTreatement);
            m_requestFrame = false;
        }

        if (frame.empty())
        {
            m_runThread = false; // end of video stream
        }

        Sleep(25);
    }

    m_threadCamRunning = false;
    return -1;
}

/// <summary>
/// Thread de d�tection des tags et d'estimation de leur positionnement
/// Met a jour les donn�es des portes pour permettre de calculer leur positionnement par rapport au kart
/// </summary>
/// <param name="param"></param>
/// <returns></returns>
unsigned long WINAPI CCamPipe::ThreadOpenCV(void* param)
{
    cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    Ptr<aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(aruco::DICT_6X6_250);

    std::vector<std::vector<cv::Point2f> > markerCorners, rejectedCandidates; 

    m_requestFrame = true;
    m_ThreadOpenCVRunning = true;
    while (m_runThread)
    {
        if (!m_requestFrame)
        {
            try
            {
                m_markerIds.clear();
                cv::aruco::detectMarkers(m_ImageForTreatement, dictionary, markerCorners, m_markerIds, parameters, rejectedCandidates);

                if (m_markerIds.size() > 0)
                {
                    //cv::aruco::drawDetectedMarkers(m_ImageForTreatement, markerCorners, markerIds);
                    cv::aruco::estimatePoseSingleMarkers(markerCorners, 0.08f, cameraMatrix, distCoeffs, rvecs, tvecs);

                    // draw axis for each marker
                    for (size_t i = 0; i < m_markerIds.size(); i++) {
                        //cv::aruco::drawAxis(m_ImageForTreatement, cameraMatrix, distCoeffs, rvecs[i], tvecs[i], 0.1f);
                        //cout << markerIds[i] << endl;
                        //cout << tvecs[0][2] << endl;
                    }
                }

                for (size_t i = 0; i < m_listPortes.size(); i++)
                {
                    m_listPortes[i]->UpdatePorte(m_markerIds, tvecs, rvecs, m_markerIds.size());
                }

                m_ARBarrel->Update(m_markerIds, tvecs, rvecs, m_markerIds.size());
                m_ARCible->Update(m_markerIds, tvecs, rvecs, m_markerIds.size());
            }
            catch(Exception e)
            {}
            
            m_requestFrame = true;
        }
        Sleep(25);
    }

    m_ThreadOpenCVRunning = false;
    return -1;
}

//---------------------------------------------------------------------------
DWORD CCamPipe::_CamThreadEntry(LPVOID lpthis)
{
    CCamPipe* pClass = reinterpret_cast <CCamPipe*>(lpthis);
    if (pClass != NULL) {
        return (DWORD)pClass->ThreadCam(lpthis);
    }
    else
        TerminateThread(0, 0);
    return 1;
}

DWORD CCamPipe::_OpenCVThreadEntry(LPVOID lpthis)
{
    CCamPipe* pClass = reinterpret_cast <CCamPipe*>(lpthis);
    if (pClass != NULL) {
        return (DWORD)pClass->ThreadOpenCV(lpthis);
    }
    else
        TerminateThread(0, 0);
    return 1;
}