#include "../include/CTag.h"

/// <summary>
/// Classe de gestion de la position des tags
/// </summary>
CTag::CTag()
{
	m_startDetected = clock();
	m_TagIsValid = false;

	m_decGauche[0] = -0.25;
	m_decGauche[1] = 0.0;
	m_decGauche[2] = 0.0;

	m_decDroit[0] = 0.25;
	m_decDroit[1] = 0.0;
	m_decDroit[2] = 0.0;

	m_config = CConfiguration::getInstance();
}

CTag::~CTag()
{

}

/// <summary>
/// Fonction permettant d'avoir une r�manence de quelques dizi�mes de seconde m�me apr�s qu'un tag ne soit plus visible
/// permet d'�viter les effets de scintillement quand un tag n'est pas syst�matiquement d�tect� sur toutes les images
/// </summary>
void CTag::UpdateNotFound()
{
	clock_t now = clock();
	
	double ellapsed = (double) (now - m_startDetected) / (double) CLOCKS_PER_SEC;

	if (ellapsed > m_config->m_DureeTagValid) {
		m_TagIsValid = false;
	}
}

/// <summary>
/// Mise � jour de la position du tag
/// </summary>
/// <param name="position">Position du tag</param>
/// <param name="orientation">Orientation du tag</param>
void CTag::UpdatePosition(cv::Vec3d position, cv::Vec3d orientation)
{
	m_position[0] = position[0];
	m_position[1] = position[1];
	m_position[2] = position[2];

	cv::Rodrigues(orientation, m_rotMatrix);

	m_startDetected = clock();
	m_TagIsValid = true;
}

/// <summary>
/// Estimation du centre de la porte � partir de la position du tag droit
/// </summary>
void CTag::EstimationTagDroit()
{
	cv::Mat res = m_rotMatrix * cv::Mat(m_decDroit);
	m_estimatedPosition[0] = (float)res.at<double>(0, 0) + m_position[0];
	m_estimatedPosition[1] = (float)res.at<double>(1, 0) + m_position[1];
	m_estimatedPosition[2] = (float)res.at<double>(2, 0) + m_position[2];
}

/// <summary>
/// Estimation du centre de la porte � partir de la position du tag gauche
/// </summary>
void CTag::EstimationTagGauche()
{
	cv::Mat res = m_rotMatrix * cv::Mat(m_decGauche);	
	m_estimatedPosition[0] = (float)res.at<double>(0, 0) + m_position[0];
	m_estimatedPosition[1] = (float)res.at<double>(1, 0) + m_position[1];
	m_estimatedPosition[2] = (float)res.at<double>(2, 0) + m_position[2];
}