#include "../include/CMarker.h"

CMarker::CMarker(string image)
{
    m_image = image;
}

CMarker::~CMarker()
{
    if(m_textureGenerated)
    {
        glDeleteTextures(1, &m_textureMarker);
    }
}

/// <summary>
/// Affichage du num�ro de la porte au dessus de celle-ci dans l'image 
/// </summary>
/// <param name="x">Position en X</param>
/// <param name="y">Position en Y</param>
/// <param name="z">Position en Z</param>
/// <param name="Rz">Rotatio verticale de la porte</param>
void CMarker::Draw(float x, float y, float z, float Rz)
{

    glPushMatrix();
    glTranslatef(x, y - 0.12f, z + 0.05f);
    glRotatef(90, 0, 1, 0);
    
    glRotatef(90, 1, 0, 0);
    glScalef(0.05f, 0.05f, 0.05f);
    
    if (!m_textureGenerated)
    {
        m_textureMarker = LoadTexturePNG(m_image.c_str());
        glBindTexture(GL_TEXTURE_2D, m_textureMarker);

        m_textureGenerated = true;

        m_MarkerList = glGenLists(1);
        glNewList(m_MarkerList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);

        glBegin(GL_QUADS);
        glColor3d(1, 1, 1);

        glNormal3f(1, 0, 0);
        glTexCoord2d(0, 0); glVertex3f(1, -1, -1);
        glTexCoord2d(1, 0); glVertex3f(1, 1, -1);
        glTexCoord2d(1, 1); glVertex3f(1, 1, 1);
        glTexCoord2d(0, 1); glVertex3f(1, -1, 1);

        glEnd();
        glDisable(GL_TEXTURE_2D);
        glEndList();
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, m_textureMarker);
        glCallList(m_MarkerList);
    }

    glPopMatrix();

}