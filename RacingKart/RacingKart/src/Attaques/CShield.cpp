#include "../../include/Attaques/CShield.h"

CShield::CShield(CMqtt *mqtt, int noKart) : CAttaque(mqtt, noKart)
{
    m_protectionAttaques = true;
}

CShield::~CShield()
{

}

void CShield::Init()
{
    CAttaque::Init();
    m_initTime = clock();
    m_ended = false;
}

/// <summary>
/// Fonction appel�e � chaque rafraichissement de l'�cran
/// </summary>
void CShield::Run()
{
    bool draw = true;
	if (m_texture == 0)
	{
		m_texture = COpenGL::LoadTexturePNG("./images/Shield.png");
        m_initTime = clock();
	}

    //Calcul de l'angle du timer circulaire a afficher
    float time = ((float)clock() - (float)m_initTime) / (float)CLOCKS_PER_SEC;
    float transprency = 1.0f;

    m_thetaMax = 360 - 360 / 6.0f * time;

    if (time > 6) {
        draw = false;
        m_ended = true;
    }

    //Affichage du bonus et du timer
    if (draw)
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, m_texture);


        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, transprency);
        glTexCoord2f(0, 0); glVertex3f(m_X1, m_Y1, 9.995f);
        glTexCoord2f(1, 0); glVertex3f(m_X2, m_Y1, 9.995f);
        glTexCoord2f(1, 1); glVertex3f(m_X2, m_Y2, 9.995f);
        glTexCoord2f(0, 1); glVertex3f(m_X1, m_Y2, 9.995f);
        glEnd();
        glDisable(GL_TEXTURE_2D);

        drawTimeBonus();
    }
}
