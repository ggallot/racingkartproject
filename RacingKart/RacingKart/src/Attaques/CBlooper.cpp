#include "../../include/Attaques/CBlooper.h"

CBlooper::CBlooper(CMqtt *mqtt, int noKart) : CAttaque(mqtt, noKart)
{
	
}

CBlooper::~CBlooper()
{

}

void CBlooper::Init()
{
    CAttaque::Init();
    m_initTime = clock();
    m_ended = false;
}

/// <summary>
/// Fonction appel�e � chaque rafraichissement de l'�cran
/// </summary>
void CBlooper::Run()
{
    bool draw = true;
	if (m_texture == 0)
	{
		m_texture = COpenGL::LoadTexturePNG("./images/Blooper.png");     
	}

    float time = ((float)clock() - (float)m_initTime) / (float)CLOCKS_PER_SEC;
    float transprency = 1.0f;

    //disparition progressive de l'image � partir d'un certain temps
    if (time > 3 && time <= 6)
    {
        transprency = 1.0f - 1 / 3.0f * (time - 3);
    }

    if (time > 6) {
        draw = false;
        m_ended = true;
    }

    //affichage de l'image sous condition
    if (draw)
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, m_texture);


        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, transprency);
        glTexCoord2f(0, 0); glVertex3f(0, 0, 9.999f);
        glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, 0, 9.999f);
        glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, 9.999f);
        glTexCoord2f(0, 1); glVertex3f(0, IMAGE_HEIGHT, 9.999f);
        glEnd();
        glDisable(GL_TEXTURE_2D);
    }
}
