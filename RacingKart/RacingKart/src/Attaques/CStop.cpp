#include "../../include/Attaques/CStop.h"

CStop::CStop(CMqtt *mqtt, int noKart) : CAttaque(mqtt, noKart)
{
    m_startMqttMsg = "sM";  //m�morisation de la vitesse et m�morisation affichage en mode black
    m_endMqttMsg = "rN";    //restoration de l'ancienne vitesse et repassage de l'affichage en mode normal
    m_protectionAttaques = true;
}

CStop::~CStop()
{
    
}

void CStop::Init()
{
    CAttaque::Init();
    m_initTime = clock();
    m_ended = false;
}

/// <summary>
/// Fonction appel�e � chaque rafraichissement de l'�cran
/// </summary>
void CStop::Run()
{
    //Call parent
    CAttaque::Run();

    bool draw = true;
	if (m_texture == 0)
	{
		m_texture = COpenGL::LoadTexturePNG("./images/Toucher.png");
        m_initTime = clock();
	}

    //Calcul de l'angle du timer circulaire a afficher
    float time = ((float)clock() - (float)m_initTime) / (float)CLOCKS_PER_SEC;
    float transprency = 0.5f;

    m_thetaMax = 360 - 360 / 6.0f * time;

    if (time > 3) {
        draw = false;
        m_ended = true;
    }

    //Affichage du bonus et du timer
    if (draw)
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, m_texture);


        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, transprency);
        glTexCoord2f(0, 0); glVertex3f(0, 0, 9.995f);
        glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, 0, 9.995f);
        glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, 9.995f);
        glTexCoord2f(0, 1); glVertex3f(0, IMAGE_HEIGHT, 9.995f);
        glEnd();
        glDisable(GL_TEXTURE_2D);

    }
}
