#include "../include/CConfiguration.h"

/* Null, because instance will be initialized on demand. */
CConfiguration* CConfiguration::m_instance = 0;

/// <summary>
/// instantiation de la classe en singleton
/// </summary>
/// <returns></returns>
CConfiguration* CConfiguration::getInstance()
{
    if (m_instance == 0)
    {
        m_instance = new CConfiguration();
    }

    return m_instance;
}

CConfiguration::CConfiguration()
{
	m_FilePath = "./Config.ini";
	m_loadedOK = GetConfiguration();
}

bool CConfiguration::isConfigOK()
{
	return m_loadedOK;
}

bool CConfiguration::GetConfiguration()
{
	bool fRet = false;
	fstream  pFile;

	pFile.open(m_FilePath.c_str(), ios::in);

	if (!pFile.is_open())  // si le fichier n'existe pas on le cr��
	{
		cout << "Config.ini file not found" << endl;
		return false;
	}
	else
	{
		try
		{
			string line;
			std::vector<std::string> result;

			while (std::getline(pFile, line))
			{
				result.clear();
				istringstream iss(line);
				for (string token; getline(iss, token, '='); )
				{
					result.push_back(std::move(token));
				}

				if (result.size() == 2)
				{
					string key = trim(result[0]);
					string value = trim(result[1]);

					if (key.compare("BrokerMqtt") == 0)
					{
						m_brokerAdresse = value;
					}
					else if (key.compare("GstreamerLibPath") == 0)
					{
						replace(value.begin(), value.end(), '/', '\\');
						m_GstreamerLibPath = value;
					}
					else if (key.compare("GstreamerBinPath") == 0)
					{
						replace(value.begin(), value.end(), '/', '\\');
						m_GstreamerBinPath = value;
					}
					else if (key.compare("DistanceGetBonus") == 0)
					{
						m_DistanceGetBonus = atof(value.c_str());
					}
					else if (key.compare("DureeTagValid") == 0)
					{
						m_DureeTagValid = atof(value.c_str());
					}
					else if (key.compare("NombreToursPartie") == 0)
					{
						m_NombreToursPartie = atoi(value.c_str());
					}
					else if (key.compare("ComPort1") == 0)
					{
						replace(value.begin(), value.end(), '/', '\\');
						m_comPortKart1 = value;
					}
					else if (key.compare("ComPort2") == 0)
					{
						replace(value.begin(), value.end(), '/', '\\');
						m_comPortKart2 = value;
					}
					else if (key.compare("ComPortLap") == 0)
					{
						replace(value.begin(), value.end(), '/', '\\');
						m_comPortKartLap = value;
					}
				}
			}

			fRet = true;
		}
		catch (...)
		{
			fRet = false;
		}

		pFile.close();
	}
	
}

string CConfiguration::trim(const string& s)
{
	auto start = s.begin();
	while (start != s.end() && std::isspace(*start)) {
		start++;
	}

	auto end = s.end();
	do {
		end--;
	} while (std::distance(start, end) > 0 && std::isspace(*end));

	return std::string(start, end + 1);
}