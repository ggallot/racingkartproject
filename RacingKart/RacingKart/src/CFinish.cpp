#include "../include/CFinish.h"

CFinish::CFinish()
{
    if (m_texture != 0)
    {
        glDeleteTextures(1, &m_texture);
        m_texture = 0;
    }
}

/// <summary>
/// Affichage de l'image finish lorsque la partie est termin�e
/// </summary>
void CFinish::Run()
{
	if (m_texture == 0)
	{
		m_texture = COpenGL::LoadTexturePNG("./images/Finish.png");
	}

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texture);


    glBegin(GL_QUADS);
    glColor4f(1, 1, 1, 1.0);
    glTexCoord2f(0, 0); glVertex3f(0, 0, 9.99f);
    glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, 0, 9.99f);
    glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, 9.99f);
    glTexCoord2f(0, 1); glVertex3f(0, IMAGE_HEIGHT, 9.99f);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}