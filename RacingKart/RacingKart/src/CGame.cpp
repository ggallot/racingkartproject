#include "../include/CGame.h"


/* Null, because instance will be initialized on demand. */
CGame* CGame::m_instance = 0;

/// <summary>
/// instantiation de la classe en singleton
/// </summary>
/// <returns></returns>
CGame* CGame::getInstance()
{
    if (m_instance == 0)
    {
        m_instance = new CGame();
    }

    return m_instance;
}

CGame::CGame()
{
    m_mqtt = CMqtt::getInstance();

    m_lapSerial = CLapSerial::getInstance();
    
    m_listIdKart.push_back(1);
    m_listIdKart.push_back(2);
}

CGame::~CGame()
{
    DisconnectKart(); 
}

void CGame::Close()
{
    DisconnectKart();
}

/// <summary>
/// Cr�ation des classes de gestion d'un kart pour chacun des joueurs
/// </summary>
/// <param name="nbOfKart">Nombre de joueurs dans la partie</param>
void CGame::ConnectKart(int nbOfKart)
{
    m_mqtt->Disconnect();
    Sleep(1000);

    //instantiation de la classe li�e � un joueur
    
    for (int i = 0; i < nbOfKart; i++) 
    {
        CKart* kart = new CKart(m_listIdKart[i]);
        m_listKart.push_back(kart);
        kart->InitRace();
    }

    //Lancement de la connexion mqtt
    m_mqtt->Connect();
}

/// <summary>
/// Reboot le jeu
/// </summary>
void CGame::RebootKart()
{
    int nb = (int)m_listKart.size();
    for (int i = nb - 1; i >= 0; i--)
    {
        m_listKart[i]->InitRace();
    }
}

/// <summary>
/// Arr�te la connexion vid�o avec les kart
/// </summary>
void CGame::DisconnectKart()
{
    //destruction des classes li�es au joueur
    int nb = (int)m_listKart.size();
    for (int i = nb-1; i >=0; i--)
    {
        try
        {
            delete m_listKart[i];
        }
        catch (exception e) {}
    }
    m_listKart.clear();

    //d�connection du mqtt
    m_mqtt->Disconnect();
}

/// <summary>
/// Indique si tous les joueurs sont connect�s (cam�ra et joystick en fonctionnement)
/// </summary>
/// <returns></returns>
bool CGame::PlayersReady()
{
    size_t nb = m_listKart.size();
    size_t cnt = 0;

    for (size_t i = 0; i < nb; i++)
    {
        if (m_listKart[i]->m_kartStatus == t_ConnexionStatus::ALL_OK || m_listKart[i]->m_kartStatus == t_ConnexionStatus::CAMERA_OK)
        {
            cnt++;
        }
    }

    if (cnt == nb) return true;
    else return false;
}

/// <summary>
/// Indique � chaque kart qu'une touche a �t� press� sur l'ordnateur
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CGame::UpdateKeyPress(char key)
{ 
    for (int i = 0; i < m_listKart.size(); i++)
    {
        m_listKart[i]->UpdateKeyPress(key);
    }
    return key;
}

/// <summary>
/// Envoi un message aux radios de chaque kart
/// </summary>
/// <param name="val"></param>
void CGame::SendToAllKart(string val)
{
    for (size_t i = 0; i < m_listKart.size(); i++)
    {
        m_listKart[i]->m_kartMQTT->SendToRadio(i+1, val);
    }
}

/// <summary>
/// Affichage des �l�ments de jeu (notamment le compte � rebours) pour chaque kart
/// </summary>
/// <param name="countDownValue"></param>
void CGame::DrawGameElements(int countDownValue)
{
    if (m_listKart.size() == 1) DrawOneKart(countDownValue);
    if (m_listKart.size() == 2) DrawTwoKart(countDownValue);   
}

/// <summary>
/// Indique le d�but de la partie
/// </summary>
void CGame::SetGameStart()
{
    for (size_t i = 0; i < m_listKart.size(); i++)
    {
        m_listKart[i]->StartRace();
    }
    m_lapSerial->ResetLapCount();
}

/// <summary>
/// Affichage de l'image et de ses �l�ments sur tout l'�cran (un seul joueur)
/// </summary>
/// <param name="countDownValue"></param>
void CGame::DrawOneKart(int countDownValue)
{
    double wmin = std::fmin(m_window_width, m_window_heigth * IMAGE_WIDTH / IMAGE_HEIGHT);
    double hmin = wmin * IMAGE_HEIGHT / IMAGE_WIDTH;

    double decX = (m_window_width - wmin) / 2.0;
    double decY = (m_window_heigth - hmin) / 2.0;

    int W = (int)wmin;
    int H = (int)hmin;

    int X1 = (int)decX;
    int Y1 = (int)decY;

    glViewport(X1, Y1, (GLsizei)W, (GLsizei)H);
    m_listKart[0]->DisplayScreen();
    DrawCountDown(countDownValue);

}

/// <summary>
/// Affichage de l'image et de ses �l�ments sur un �cran splitt�(pour deux joueurs)
/// </summary>
/// <param name="countDownValue"></param>
void CGame::DrawTwoKart(int countDownValue)
{
    double hWidth = (double)m_window_width / 2.0;
    double hHeight = (double)m_window_heigth;

    double wmin = std::fmin(hWidth, hHeight * IMAGE_WIDTH / IMAGE_HEIGHT);
    double hmin = wmin * IMAGE_HEIGHT / IMAGE_WIDTH;

    double decX = (m_window_width - 2 * wmin) / 2.0;
    double decY = (m_window_heigth - hmin) / 2.0;

    int W = (int)wmin;
    int H = (int)hmin;

    int X1 = (int)decX;
    int Y1 = (int)decY;

    int X2 = X1 + W;

    glViewport(X1, Y1, (GLsizei)W, (GLsizei)H);
    m_listKart[0]->DisplayScreen();
    DrawCountDown(countDownValue);

    glViewport(X2, Y1, (GLsizei)W, (GLsizei)H);
    m_listKart[1]->DisplayScreen();
    DrawCountDown(countDownValue);
}

/// <summary>
/// Affichage du compte � rebours
/// </summary>
/// <param name="step"></param>
void CGame::DrawCountDown(int step)
{
    if (step == -1) return;

    static bool textureGenerated = false;
    bool draw = true;
    float transparency = 1;
    float actualTime = (float)clock() / (float)CLOCKS_PER_SEC;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();

    if (!textureGenerated)
    {
        m_textureChiffre3 = COpenGL::LoadTexturePNG("./images/Chiffre3.png");
        m_textureChiffre2 = COpenGL::LoadTexturePNG("./images/Chiffre2.png");
        m_textureChiffre1 = COpenGL::LoadTexturePNG("./images/Chiffre1.png");
        m_textureChiffreGo = COpenGL::LoadTexturePNG("./images/ChiffreGo.png");

        m_textureNuage3 = COpenGL::LoadTexturePNG("./images/Nuage3.png");
        m_textureNuage2 = COpenGL::LoadTexturePNG("./images/Nuage2.png");
        m_textureNuage1 = COpenGL::LoadTexturePNG("./images/Nuage1.png");
        m_textureNuageGo = COpenGL::LoadTexturePNG("./images/NuageGo.png");

        textureGenerated = true;
    }

    if (step == 3) {
        glBindTexture(GL_TEXTURE_2D, m_textureChiffre3);
    }
    else if (step == 2) {
        glBindTexture(GL_TEXTURE_2D, m_textureChiffre2);
    }
    else if (step == 1) {
        glBindTexture(GL_TEXTURE_2D, m_textureChiffre1);
    }
    else if (step == 0) {
        glBindTexture(GL_TEXTURE_2D, m_textureChiffreGo);
    }
    else
    {
        draw = false;
    }

    glEnable(GL_TEXTURE_2D);
    if (draw)
    {
        //Affichage avec fondu progressif des chiffres
        float hX = IMAGE_HEIGHT * 0.3f;
        float hY = IMAGE_HEIGHT * 0.3f;

        glBegin(GL_QUADS);

        if (m_transparency > 1) m_transparency = 1.0f;
        if (m_transparency < 0) m_transparency = 0.0f;
        glColor4f(1, 1, 1, m_transparency);
        glTexCoord2f(0, 0); glVertex3f(CENTER_X - hX, CENTER_Y - hY, 9.97f);
        glTexCoord2f(1, 0); glVertex3f(CENTER_X + hX, CENTER_Y - hY, 9.97f);
        glTexCoord2f(1, 1); glVertex3f(CENTER_X + hX, CENTER_Y + hY, 9.97f);
        glTexCoord2f(0, 1); glVertex3f(CENTER_X - hX, CENTER_Y + hY, 9.97f);
        glEnd();
    }

    //Affcihage du nuage avec le feu
    draw = true;
    float nuageTransparency = 1.0f;
    if (step > 3) {
        glBindTexture(GL_TEXTURE_2D, m_textureNuage3);
        nuageTransparency = m_transparency;
    }
    else if (step == 3) {
        glBindTexture(GL_TEXTURE_2D, m_textureNuage3);
        nuageTransparency = 1.0f;
    }
    else if (step == 2) {
        glBindTexture(GL_TEXTURE_2D, m_textureNuage2);
        nuageTransparency = 1.0f;
    }
    else if (step == 1) {
        glBindTexture(GL_TEXTURE_2D, m_textureNuage1);
        nuageTransparency = 1.0f;
    }
    else if (step == 0) {
        glBindTexture(GL_TEXTURE_2D, m_textureNuageGo);
        nuageTransparency = m_transparency;
    }
    else {
        draw = false;
    }

    if (draw)
    {
        //calcul du mouvement du nuage � partir de 2 fonctions trigonom�triques
        float Xnuage = 20 * std::cosf(7.28f * actualTime / 2.0f) + 1000;
        float Ynuage = 10 * std::sinf(7.28f * actualTime / 5.0f) + 550;
        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, nuageTransparency);
        glTexCoord2f(0, 0); glVertex3f(Xnuage - 150, Ynuage - 150, 9.98f);
        glTexCoord2f(1, 0); glVertex3f(Xnuage + 150, Ynuage - 150, 9.98f);
        glTexCoord2f(1, 1); glVertex3f(Xnuage + 150, Ynuage + 150, 9.98f);
        glTexCoord2f(0, 1); glVertex3f(Xnuage - 150, Ynuage + 150, 9.98f);
        glEnd();
    }
    glDisable(GL_TEXTURE_2D);
}
