#include "../include/CStart.h"

CStart::CStart()
{
   
}

CStart::~CStart()
{
    if (m_textureStart != 0)
    {
        glDeleteTextures(1, &m_textureStart);
        m_textureStart = 0;
    }
    glDeleteLists(m_StartList, 1);
}

/// <summary>
/// Fonction d'affichage du damier de d�but et fin de tour
/// </summary>
/// <param name="x">Position X</param>
/// <param name="y">Position Y</param>
/// <param name="z">Position Z</param>
/// <param name="Rz">Rotation verticale</param>
void CStart::Draw(float x, float y, float z, float Rz)
{

    glPushMatrix();
    glTranslatef(x, y + 0.135f, z + 0.05f);
    glRotatef(Rz, 0, 1, 0);
    
   

    if (m_textureStart == 0)
    {
        m_textureStart = LoadTexturePNG("./images/damier.png");
        glBindTexture(GL_TEXTURE_2D, m_textureStart);

        m_StartList = glGenLists(1);
        glNewList(m_StartList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);

        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 0.7f);

        glNormal3f(0, 0, 1);
        glTexCoord2d(0, 0); glVertex3f(0.23f, -0.02f, 0);
        glTexCoord2d(0, 1); glVertex3f(0.23f, 0.02f, 0);
        glTexCoord2d(1, 1); glVertex3f(-0.23f, 0.02f, 0);
        glTexCoord2d(1, 0); glVertex3f(-0.23f, -0.02f, 0);

        glEnd();
        glDisable(GL_TEXTURE_2D);
        glEndList();
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, m_textureStart);
        glCallList(m_StartList);
    }

    glPopMatrix();
}