#include "../include/CTirages.h"
#define M_PI 3.14159265358979323846


/// <summary>
/// Tirage d'une variable alatoire subissant une loi uniforme
/// </summary>
/// <param name="min">Valeur min de la variable alatoire</param>
/// <param name="max">Valeur max de la variable alatoire</param>
/// <returns></returns>
UniformVar::UniformVar(double min, double max)
{
	mt19937 m_generator(rand());
	m_distribution = uniform_real_distribution<double>(min, max);
}

UniformVar::~UniformVar()
{
}

double UniformVar::Tirage()
{
	double randNormal = m_distribution(m_generator);
	return randNormal;
}