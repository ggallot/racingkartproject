#include "../../include/Circuit/CARMissile.h"

CARMissile::CARMissile()
{
	m_textureMissile = COpenGL::LoadTexturePNG("./OBJ/rocketo.png");
	m_3DMissile = OBJLoader::LoadFile("./OBJ/rocketo.obj.txt");

	m_initTime = clock();
	m_memNow = clock();

	m_direction[0] = 0;
	m_direction[1] = 0;
	m_direction[2] = 1;

	m_position[0] = 0;
	m_position[1] = 0.2;
	m_position[2] = 0;
}


CARMissile::~CARMissile()
{
	glDeleteTextures(1, &m_textureMissile);
	m_textureMissile = 0;

	glDeleteLists(m_3DMissile, 1);
	m_3DMissile = 0;
}



/// <summary>
/// Affiche les �l�ments correspondant � la liste des tags identifi�s dans l'image
/// </summary>
void CARMissile::DrawOpenGL()
{
	clock_t now = clock();

	float totalTime = (float)(now - m_initTime) / (float)CLOCKS_PER_SEC;
	
	if (totalTime > 5)
	{
		m_ended = true;
	}

	m_position[0] += m_speed* m_Te* m_direction[0];
	m_position[1] += m_speed* m_Te* m_direction[1];
	m_position[2] += m_speed* m_Te* m_direction[2];


	glPushMatrix();
	glTranslatef(m_position[0], m_position[1], m_position[2]);
	glRotatef(-m_angleMissile * 180 / M_PI, 0, 1, 0);

	glPushMatrix();
	glScalef(0.0017, 0.0017, 0.0017);

	glBindTexture(GL_TEXTURE_2D, m_textureMissile);
	glCallList(m_3DMissile);
	glPopMatrix();

	glPopMatrix();
}


void CARMissile::UpdatetargetPosition(cv::Vec3d targetPosition)
{
	clock_t now = clock();
	m_Te = (float)(now - m_memNow) / (float)CLOCKS_PER_SEC;
	m_memNow = now;

	targetPosition[0] *= 1.3;
	targetPosition[1] *= 1.3;
	targetPosition[2] *= 1.3;

	//si la position indique que la cible est bien pr�sente dans l'image
	if (targetPosition[2] > 0)
	{
		cv::Vec3d v = targetPosition - m_position;
		//v[1] = 0;
		
		float n = sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		if (n > 0.1 && v[2] > 0)
		{
			v[0] /= n;
			v[1] /= n;
			v[2] /= n;

			cv::Vec3d vp;
			vp[0] = -v[2];
			vp[1] = 0;
			vp[2] = v[0];

			float dot = v[0] * m_direction[0] + v[2] * m_direction[2];

			if (dot >= 1.0f) dot = 0.99999f;
			if (dot <= -1.0f) dot = -0.99999f;

			float corrAngle = acos(dot);

			if (vp[0] * m_direction[0] + vp[2] * m_direction[2] > 0) corrAngle *= -1;
			m_angleMissile += corrAngle;

			m_direction[0] = -sin(m_angleMissile);
			m_direction[2] = cos(m_angleMissile);
		}
		else
		{
			m_ended = true;
		}
	}
}



/// <summary>
/// Correcteur Proportionnel / Int�gral
/// </summary>
/// <param name="err"></param>
/// <returns></returns>
float CARMissile::CorrPI(float err)
{
	m_Ti += (err + m_err_mem) / 2.0f * m_Te;

	m_err_mem = err;

	float S = m_Kp * err + m_Ki * m_Ti;

	return S;
}