#include "../../include/Circuit/CARBarrels.h"

CARBarrels::CARBarrels()
{
	m_textureVoiture = COpenGL::LoadTexturePNG("./OBJ/voiture.png");
	m_3DVoiture = OBJLoader::LoadFile("./OBJ/voiture.obj.txt");

	m_textureBidons = COpenGL::LoadTexturePNG("./OBJ/barrel.png");
	m_3DBidons = OBJLoader::LoadFile("./OBJ/barrel.obj.txt");

	m_initTime = clock();
}


CARBarrels::~CARBarrels()
{
	glDeleteTextures(1, &m_textureVoiture);
	m_textureVoiture = 0;

	glDeleteLists(m_3DVoiture, 1);
	m_3DVoiture = 0;

	glDeleteTextures(1, &m_textureBidons);
	m_textureBidons = 0;

	glDeleteLists(m_3DBidons, 1);
	m_3DBidons = 0;

}

/// <summary>
/// Mise � jour de la liste des positions correspondant au tag cherch�
/// </summary>
/// <param name="markerIds"></param>
/// <param name="tvecs"></param>
/// <param name="rvecs"></param>
/// <param name="nb"></param>
void CARBarrels::Update(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb)
{
	m_listPositions.clear();
	for (size_t i = 0; i < nb; i++) 
	{
		if (markerIds[i] == m_idTag) 
		{
			m_listPositions.push_back(tvecs[i]);
		}
	}
}

/// <summary>
/// Affiche les �l�ments correspondant � la liste des tags identifi�s dans l'image
/// </summary>
void CARBarrels::DrawOpenGL()
{
	clock_t now = clock();

	float ellapsed = (float)(now - m_initTime) / (float)CLOCKS_PER_SEC;
	m_initTime = now;

	m_hit = false;
	for (int i = 0; i < m_listPositions.size(); i++)
	{
		Draw(ellapsed, m_listPositions[i][0], m_listPositions[i][1], m_listPositions[i][2]);
	}

}

/// <summary>
/// Affichage openGL pour un tag
/// </summary>
/// <param name="x"></param>
/// <param name="y"></param>
/// <param name="z"></param>
void CARBarrels::Draw(float ellapsed, float x, float y, float z)
{
	m_baseAngle += ellapsed * 40;
	if(m_baseAngle >= 360.0)
	{
		m_baseAngle -= 360.0;
	}

	//calcul du vecteur QRcode->joueur
	float Vx = -x;
	float Vz = -z;

	float distJoueur = sqrtf(x * x + z * z);
	Vx /= distJoueur;
	Vz /= distJoueur;

	glPushMatrix();
	glTranslatef(x, y, z);

	glPushMatrix();
	glScalef(0.02, 0.02, 0.02);

	glRotatef(180, 1, 0, 0);
	glRotatef(45, 0, 1, 0);
	glBindTexture(GL_TEXTURE_2D, m_textureVoiture);
	glCallList(m_3DVoiture);
	glPopMatrix();

	for (float i = 0; i <= 240; i+=120) 
	{
		float theta = -m_baseAngle + i;

		glPushMatrix();
		glRotatef(-90, 1, 0, 0);
		glRotatef(theta, 0, 0, 1);
		glTranslatef(m_distanceBarrel, 0, 0);
		glScalef(0.0007, 0.0007, 0.0007);
		glBindTexture(GL_TEXTURE_2D, m_textureBidons);
		glCallList(m_3DBidons);
		glPopMatrix();

		theta *= M_PI / 180.0;
		//vecteur directeur du barril
		float Ux = cos(theta);
		float Uz = sin(theta);

		//calcul de l'angle entre les vecteurs
		float alpha = (float) acos(Ux * Vx + Uz * Vz) * 180 / M_PI;
		
		if (fabsf(alpha) < 5 && distJoueur < 0.6)
		{
			m_hit = true;
		}
		
	}
	
	glPopMatrix();
}

/// <summary>
/// Renvoi si le kart a touch� un barrile
/// </summary>
/// <returns></returns>
bool CARBarrels::GetHistStatus()
{
	return m_hit;
}