#include "../../include/Circuit/CARCible.h"

CARCible::CARCible()
{
	m_textureCible = COpenGL::LoadTexturePNG("./OBJ/ArcheryTarget.png");
	m_3DCible = OBJLoader::LoadFile("./OBJ/ArcheryTarget.obj.txt");

	m_noPosition[0] = 0;
	m_noPosition[1] = 0;
	m_noPosition[2] = -1.0;
}


CARCible::~CARCible()
{
	glDeleteTextures(1, &m_textureCible);
	m_textureCible = 0;

	glDeleteLists(m_3DCible, 1);
	m_3DCible = 0;

}

/// <summary>
/// Mise � jour de la liste des positions correspondant au tag cherch�
/// </summary>
/// <param name="markerIds"></param>
/// <param name="tvecs"></param>
/// <param name="rvecs"></param>
/// <param name="nb"></param>
void CARCible::Update(vector<int> markerIds, vector<cv::Vec3d> tvecs, vector<cv::Vec3d> rvecs, size_t nb)
{
	m_listPositions.clear();
	for (size_t i = 0; i < nb; i++)
	{
		if (markerIds[i] == m_idTag)
		{
			m_listPositions.push_back(tvecs[i]);
		}
	}
}

void CARCible::DrawOpenGL()
{
	for (int i = 0; i < m_listPositions.size(); i++)
	{
		glPushMatrix();
		glTranslatef(m_listPositions[i][0], m_listPositions[i][1] + 0.05, m_listPositions[i][2]);
		glRotatef(180, 1, 0, 0);
		glPushMatrix();
		glScalef(0.05, 0.05, 0.05);
		glBindTexture(GL_TEXTURE_2D, m_textureCible);
		glCallList(m_3DCible);
		glPopMatrix();

		glPopMatrix();
	}
}

cv::Vec3d CARCible::GetCiblePosition()
{
	if (m_listPositions.size() == 0) return m_noPosition;
	else return m_listPositions[0];
}