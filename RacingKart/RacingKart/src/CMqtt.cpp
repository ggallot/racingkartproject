#include "../include/CMqtt.h"
#include <thread>

/* Null, because instance will be initialized on demand. */
CMqtt* CMqtt::m_instance = 0;

/// <summary>
/// Fonction d'instantiation de la classe en singleton
/// </summary>
/// <returns></returns>
CMqtt* CMqtt::getInstance()
{
    if (m_instance == 0)
    {
        m_instance = new CMqtt();
    }

    return m_instance;
}

/// <summary>
/// Initialisation de la communication MQTT et cr�ation des topics
/// </summary>
CMqtt::CMqtt()
{
    m_config = CConfiguration::getInstance();

    MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
    MQTTAsync_willOptions will_opts = MQTTAsync_willOptions_initializer;

    MQTTAsync_token token;
    int rc;
  
    string clientName = "RacingKart";
    
    m_GamewillTopic = "GameKartPc/startcam";

    //topics pour le kart N�1
    m_radioDataReceiveTopic.push_back("radio1/joystick");
    m_rpiDataReceiveTopic.push_back("GameKartRpi1/recvattack");
    m_radioDisconnectTopic.push_back("radio1/startup");
    m_radioUpdateScreenTopic.push_back("radio1/screen");
    m_PCsendAttackTopic.push_back("GameKartPc1/sendattack");
    m_PCsendJoystickTopic.push_back("GameKartPc1/joystick");
    
    //topics pour le kart N�2
    m_radioDataReceiveTopic.push_back("radio2/joystick");
    m_rpiDataReceiveTopic.push_back("GameKartRpi2/recvattack");
    m_radioDisconnectTopic.push_back("radio2/startup");
    m_radioUpdateScreenTopic.push_back("radio2/screen");
    m_PCsendAttackTopic.push_back("GameKartPc2/sendattack");
    m_PCsendJoystickTopic.push_back("GameKartPc2/joystick");
    
    string brokerAdress = "tcp://" + m_config->m_brokerAdresse + ":1883";

    MQTTAsync_create(&m_client, brokerAdress.c_str(), clientName.c_str(), MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTAsync_setCallbacks(m_client, NULL, &CMqtt::on_connection_lost, &CMqtt::on_message_arrived, NULL);

    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    conn_opts.connectTimeout = 1;
    //conn_opts.will = &will_opts;
    conn_opts.onSuccess = &CMqtt::on_connected;
    conn_opts.onFailure = &CMqtt::onConnectFailure;
    conn_opts.context = CMqtt::m_client;
    
    //initialisation des variables
    for (int i = 0; i < 2; i++)
    {
        m_joystick.push_back(0);
        m_joySpeed.push_back(0);
        m_BonusSendValue.push_back('_');
        m_AttackReceivedValue.push_back('_');
        m_radioConnected.push_back(false);
    }
    
    if ((rc = MQTTAsync_connect(CMqtt::m_client, &conn_opts)) != MQTTASYNC_SUCCESS)
    {
        cout << "Error creating MQTT client" << endl;
    }

}

CMqtt::~CMqtt()
{  
    Sleep(500);

    MQTTAsync_disconnectOptions disc_opts = MQTTAsync_disconnectOptions_initializer;
    disc_opts.onSuccess = &CMqtt::on_disconnected;

    MQTTAsync_disconnect(m_client, &disc_opts);
    MQTTAsync_destroy(&m_client);
}

/// <summary>
/// Indique � la raspberry que le jeu est lanc� et qu'elle peut d�marrer sa webcam
/// </summary>
void CMqtt::Connect()
{
    RegisterToRpi(true);
}

/// <summary>
/// Indique � la raspberry que le jeu est arr�t� et qu'elle peut eteindre sa webcam
/// </summary>
void CMqtt::Disconnect()
{
    RegisterToRpi(false);
}


void CMqtt::onConnectFailure(void* context, MQTTAsync_failureData* response)
{
    cout << "MQTT client connection failed" << endl;
}

/// <summary>
/// Lors de la connexion aubroker MQTT, cr�ation des souscriptions aux topics
/// </summary>
/// <param name="context"></param>
/// <param name="response"></param>
void CMqtt::on_connected(void* context, MQTTAsync_successData* response)
{
    CMqtt* mqtt = CMqtt::getInstance();

    MQTTAsync client = (MQTTAsync)context;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    MQTTAsync_message pubmsg = MQTTAsync_message_initializer;

    cout << "Client connected" << endl;

    opts.onSuccess = &CMqtt::on_Subscribe;
    opts.onFailure = &CMqtt::on_SubscribeFailure;
    opts.context = client;

    //Subscribe et initialisations
    for (int i = 0; i < 2; i++)
    {
        MQTTAsync_subscribe(client, mqtt->m_radioDataReceiveTopic[i].c_str(), QOS, &opts);
        MQTTAsync_subscribe(client, mqtt->m_rpiDataReceiveTopic[i].c_str(), QOS, &opts);
        MQTTAsync_subscribe(client, mqtt->m_radioDisconnectTopic[i].c_str(), QOS, &opts);

        mqtt->m_BonusSendValue[i] = '_';
        mqtt->m_AttackReceivedValue[i] = '_';
    }
}

void CMqtt::on_connection_lost(void* context, char* cause)
{
    MQTTAsync client = (MQTTAsync)context;
    MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
    int rc;
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
    printf("Reconnecting\n");
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;

    /*if ((rc = MQTTAsync_connect(client, &conn_opts)) != MQTTASYNC_SUCCESS)
    {
        printf("Failed to start connect, return code %d\n", rc);
        //finished = 1;
    }*/
}

void CMqtt::on_Subscribe(void* context, MQTTAsync_successData* response)
{

}

void CMqtt::on_SubscribeFailure(void* context, MQTTAsync_failureData* response)
{

}

void CMqtt::on_disconnected(void* context, MQTTAsync_successData* response)
{

}



void CMqtt::on_send(void* context, MQTTAsync_successData* response)
{

}

void CMqtt::on_delivered(void* context, MQTTAsync_deliveryComplete dt)
{

}

/// <summary>
/// Traitement des messages re�us
/// </summary>
/// <param name="context"></param>
/// <param name="topicName"></param>
/// <param name="topicLen"></param>
/// <param name="msg"></param>
/// <returns></returns>
int  CMqtt::on_message_arrived(void* context, char* topicName, int topicLen, MQTTAsync_message* msg)
{
    CMqtt* mqtt = CMqtt::getInstance();
    
    int len = msg->payloadlen;
    char buffer[1024];

    memcpy(&buffer, msg->payload, len * sizeof(char));
    buffer[len] = '\0';

    for (int i = 0; i < 2; i++)
    {
        //donn�es re�ues depuis le joystick
        if (mqtt->m_radioDataReceiveTopic[i].compare(topicName) == 0)
        {
            int dir = 0;
            int speed = 0;
            char bonus = '_';
            if (sscanf_s(buffer, "J:%d;S:%d;B:%c", &dir, &speed, &bonus,1) == 3)
            {
                mqtt->m_joystick[i] = dir;
                mqtt->m_joySpeed[i] = speed;
                mqtt->m_BonusSendValue[i] = bonus;
                if (bonus != '_')
                {
                    cout << "receive from radio" << bonus << endl;
                }
            }
            break;
        }
        else if (mqtt->m_rpiDataReceiveTopic[i].compare(topicName) == 0)    //Attaque re�ue par le kart
        {
            mqtt->m_AttackReceivedValue[i] = buffer[0];
            break;
        }
        else if (mqtt->m_radioDisconnectTopic[i].compare(topicName) == 0)   //Connexion, d�connexion du joystick
        {
            if (buffer[0] == 'C') mqtt->m_radioConnected[i] = true;
            else if (buffer[0] == 'D') mqtt->m_radioConnected[i] = false;
            break;
        }
    }


    MQTTAsync_freeMessage(&msg);
    MQTTAsync_free(topicName);

    return 1;
}

/// <summary>
/// Retourne la valeur du joystick de direction
/// </summary>
/// <param name="noKart">num�ro du kart</param>
/// <returns></returns>
int CMqtt::getJoystick(int noKart) 
{ 
    int fRet = 0;
    if (noKart == 1) 
    {
        fRet = m_joystick[0];
    }
    else if (noKart == 2) 
    {
        fRet = m_joystick[1];
    }
    return fRet; 
}

// <summary>
/// Retourne la valeur du joystick de vitesse
/// </summary>
/// <param name="noKart">num�ro du kart</param>
/// <returns></returns>
int CMqtt::getSpeed(int noKart) 
{ 
    int fRet = 0;
    if (noKart == 1)
    {
        fRet = m_joySpeed[0];
    }
    else if (noKart == 2)
    {
        fRet = m_joySpeed[1];
    }
    return fRet;

    return fRet;
}


/// <summary>
/// Demande si la radiocommande est connect�e
/// </summary>
/// <param name="noKart"></param>
/// <returns></returns>
bool CMqtt::isRadioConnected(int noKart) 
{ 
    bool fRet = false;
    if (noKart == 1)
    {
        fRet = m_radioConnected[0];
    }
    else if(noKart == 2)
    {
        fRet = m_radioConnected[1];
    }
    return fRet;
}

/// <summary>
/// Envoi une chaine de caract�re � la raspberry
/// </summary>
/// <param name="data"></param>
void CMqtt::SendToRaspberry(int noKart, string data)
{
    if (sizeof(data) > 0)
    {
        string topic;
        MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
        MQTTAsync_message pubmsg = MQTTAsync_message_initializer;

        topic = m_PCsendAttackTopic[noKart - 1];
        
        opts.onSuccess = &on_send;
        opts.context = m_client;
        pubmsg.payload = &data;
        pubmsg.payloadlen = data.size();
        pubmsg.qos = QOS;
        pubmsg.retained = 0;
        MQTTAsync_sendMessage(m_client, topic.c_str(), &pubmsg, &opts);
    }
}


/// <summary>
/// Signal � la rapsberry que le logiciel est en route
/// </summary>
/// <param name="connected"></param>
void CMqtt::RegisterToRpi(bool connected)
{
    CMqtt* mqtt = CMqtt::getInstance();
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    MQTTAsync_message pubmsg = MQTTAsync_message_initializer;

    char data = '_';
    if (connected) data = 'C';
    else data = 'D';

   /* opts.onSuccess = &CMqtt::on_send;
    opts.context = &CMqtt::m_client;*/
    pubmsg.payload = &data;
    pubmsg.payloadlen = 1;
    pubmsg.qos = QOS;
    pubmsg.retained = true;

    MQTTAsync_sendMessage(m_client, m_GamewillTopic.c_str(), &pubmsg, &opts);
}

/// <summary>
/// Envoi d'une chaine de caract�res � la radio
/// </summary>
/// <param name="data"></param>
void CMqtt::SendToRadio(int noKart, string data, bool retain)
{
    if (sizeof(data) > 0)
    {
        string topic = "";
        MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
        MQTTAsync_message pubmsg = MQTTAsync_message_initializer;

        topic = m_radioUpdateScreenTopic[noKart - 1];
       
        //        opts.onSuccess = &on_send;
        opts.context = &m_client;
        pubmsg.payload = &data;
        pubmsg.payloadlen = data.size();
        pubmsg.qos = QOS;

        if (!retain)  pubmsg.retained = 0;
        else pubmsg.retained = 1;

        MQTTAsync_sendMessage(m_client, topic.c_str(), &pubmsg, &opts);
    }
}

/// <summary>
/// Demande quel bonus a �t� lanc� par le joueur
/// </summary>
/// <param name="noKart"></param>
/// <returns></returns>
char CMqtt::getBonusSendValue(int noKart)
{
    char val = '_';
    if (noKart == 1)
    {
        val = m_BonusSendValue[0];
        m_BonusSendValue[0] = '_';
    }
    else if (noKart == 1)
    {
        val = m_BonusSendValue[1];
        m_BonusSendValue[1] = '_';
    }
    return val;
}

/// <summary>
/// Demande quelle attaque a �t� re�ue
/// </summary>
/// <param name="noKart"></param>
/// <returns></returns>
char CMqtt::getReceiveAttackValue(int noKart)
{
    char val = '_';
    if (noKart == 1)
    {
        val = m_AttackReceivedValue[0];
        m_AttackReceivedValue[0] = '_';
    }
    else if (noKart == 1)
    {
        val = m_AttackReceivedValue[1];
        m_AttackReceivedValue[1] = '_';
    }

    return val;
}

void CMqtt::SendDataJoystick(int noKart, string data)
{
    if (sizeof(data) > 0)
    {
        string topic = "";
        MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
        MQTTAsync_message pubmsg = MQTTAsync_message_initializer;

        try
        {
            topic = m_PCsendJoystickTopic[noKart - 1];

            //        opts.onSuccess = &on_send;
            opts.context = &m_client;
            pubmsg.payload = &data;
            pubmsg.payloadlen = data.size();
            pubmsg.qos = QOS;
            pubmsg.retained = 0;

            MQTTAsync_sendMessage(m_client, topic.c_str(), &pubmsg, &opts);
        }
        catch (...) {}
    }
}