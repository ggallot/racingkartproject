#include "../include/CVehicule.h"

/// <summary>
/// Chargement des fichiers STL du kart pour pouvoir l'afficher � l'�cran
/// <param name="noKart">No du kart</param>
/// </summary>
CVehicule::CVehicule(int noKart)
{
    double couleurR = 0.816;
    double couleurG = 0.816;
    double couleurB = 0.45;

    if (noKart == 2)
    {
        couleurR = 0.753;
        couleurG = 0.604;
        couleurB = 0.804;
    }

    m_Calandre = STLLoader::LoadFile("./STL/Calandre.stl", couleurR, couleurG, couleurB);
	m_Console = STLLoader::LoadFile("./STL/Console.stl", couleurR, couleurG, couleurB);
	m_Fusee_droite = STLLoader::LoadFile("./STL/Fusee_droite.stl", couleurR, couleurG, couleurB);
	m_Fusee_gauche = STLLoader::LoadFile("./STL/Fusee_gauche.stl", couleurR, couleurG, couleurB);
	m_MoyeuDroit = STLLoader::LoadFile("./STL/MoyeuDroit.stl", couleurR, couleurG, couleurB);
	m_MoyeuGauche = STLLoader::LoadFile("./STL/MoyeuGauche.stl", couleurR, couleurG, couleurB);
    m_Direction = STLLoader::LoadFile("./STL/Direction.stl", 0.7, 0.7, 0.7);
	m_RoueDroite = STLLoader::LoadFile("./STL/RoueDroite.stl", 0.17, 0.17, 0.2);
	m_RoueGauche = STLLoader::LoadFile("./STL/RoueGauche.stl", 0.17, 0.17, 0.2);
}

CVehicule::~CVehicule()
{
    glDeleteLists(m_Calandre, 1);
    glDeleteLists(m_Console, 1);
    glDeleteLists(m_Fusee_droite, 1);
    glDeleteLists(m_Fusee_gauche, 1);
    glDeleteLists(m_MoyeuDroit, 1);
    glDeleteLists(m_MoyeuGauche, 1);
    glDeleteLists(m_Direction, 1);
    glDeleteLists(m_RoueDroite, 1);
    glDeleteLists(m_RoueGauche, 1);

}

/// <summary>
/// Affichage du kart
/// </summary>
/// <param name="direction">Angle de braquage des roues</param>
/// <param name="speed">Vitesse de rotation des roues</param>
void CVehicule::DrawVehicule(int direction, int speed)
{
    GLfloat mdl[16];
    glPushMatrix();
        //glTranslatef(0, 0.09, 0.06);
        glTranslatef(0, 0.0704, 0.01635);
        glRotatef(90, 1, 0, 0);
        glRotatef(-90, 0, 0, 1);

        //Positionnement des corps fixes
        glScalef(0.001, 0.001, 0.001);
        glCallList(m_Calandre);
        glCallList(m_Console);
        glCallList(m_Direction);

        m_angleRoue = (float)direction * 30.0f / 100.0f;
        m_speedRotation -= (float)speed * 15.0f / 100.0f;

        glGetFloatv(GL_MODELVIEW_MATRIX, mdl);
        
        //Positionnement des �l�ments: rotation  verticale des fus�es de roues + rotation des roues
        glTranslatef(-136, 41, 14.1);
        glRotatef(m_angleRoue, 0, 0, 1);
        glRotatef(m_speedRotation, 0, 1, 0);
        glTranslatef(136, -41, -14.1);
        glCallList(m_MoyeuDroit);
        glCallList(m_RoueDroite);
        
        glLoadMatrixf(mdl);
        glPushMatrix();
        glTranslatef(-136, -41, 14.1);
        glRotatef(m_angleRoue, 0, 0, 1);
        glRotatef(m_speedRotation, 0, 1, 0);
        glTranslatef(136, 41, -14.1);
        glCallList(m_MoyeuGauche);
        glCallList(m_RoueGauche);
        
    glPopMatrix();
}