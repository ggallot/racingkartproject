#include "../include/CKart.h"

/// <summary>
/// Initialisation du param�trage d'un kart
/// </summary>
/// <param name="noKart"></param>
CKart::CKart(int noKart)
{
    m_noKart = noKart;
    m_BackGroundTextureGenerated = false;
    m_bonusTextureGenerated = false;
    m_textureNoJoystick = 0;

    m_kartMQTT = CMqtt::getInstance();                  //Instantation de la classe de communication MQTT
    m_LapSerial = CLapSerial::getInstance();

    m_sound = CSoundManager::getInstance();             //Instantiation de la classe de gestion des sons
    m_config = CConfiguration::getInstance();           //Instantation de la classe de configuration

    m_kartStatus = t_ConnexionStatus::NOTHING;
    m_CameraFrame = cv::imread("./images/AttenteAll.png"); // Mat::zeros(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);
    m_NoCameraFrame = cv::imread("./images/AttenteCamera.png");
    
    m_renderRollingBonus = false;
    m_currentBonus = -1;
    m_attackReceived = nullptr;
    m_bonusAttack = nullptr;

    m_textureNoJoystick = 0;

    m_listAttaque["Stop"] = new CStop(m_kartMQTT, m_noKart);
    m_listAttaque["Blooper"] = new CBlooper(m_kartMQTT, m_noKart);
    m_listAttaque["Volant"] = new CVolant(m_kartMQTT, m_noKart);
    m_listAttaque["Eclair"] = new CEclair(m_kartMQTT, m_noKart);
    m_listAttaque["Shield"] = new CShield(m_kartMQTT, m_noKart);

    string port = "9001";
    if (noKart == 1)
    {
        m_serialPort = new CJoystickSerial(m_noKart, m_config->m_comPortKart1);
    }
    else if (noKart == 2)
    {
        m_serialPort = new CJoystickSerial(m_noKart, m_config->m_comPortKart2);
        port = "9002";
    }

    //Initialisation de la classe r�cup�rant les images en provenance de la webcam
    m_camera = new CCamPipe(port, &m_CameraFrame);
    //m_camera = new CCamPipe(0, &m_CameraFrame);
    m_camera->start();

#ifdef _DrawVehicule
    m_vehicule = new CVehicule(noKart);
#endif
}

CKart::~CKart()
{
    m_kartMQTT = NULL;
    m_sound = NULL;
    m_config = NULL;

    m_camera->stop();
      
    delete m_camera;

#ifdef _DrawVehicule
    delete m_vehicule;
#endif

    delete m_serialPort;

    delete m_listAttaque["Stop"];
    delete m_listAttaque["Blooper"];
    delete m_listAttaque["Volant"];
    delete m_listAttaque["Eclair"];
    delete m_listAttaque["Shield"];

    m_listAttaque.clear();

    if (m_textureStart != 0)
    {
        glDeleteTextures(1, &m_textureStart);
        m_textureStart = 0;
    }
    if (m_textureFond != 0)
    {
        glDeleteTextures(1, &m_textureFond);
        m_textureFond = 0;
    }
    if (m_textureRondBonus != 0)
    {
        glDeleteTextures(1, &m_textureRondBonus);
        m_textureRondBonus = 0;
    }
    if (m_textureNoJoystick != 0)
    {
        glDeleteTextures(1, &m_textureNoJoystick);
        m_textureNoJoystick = 0;
    }
    
    for (int i = 0; i < m_textureLap.size(); i++)
    {
        glDeleteTextures(1, &m_textureLap[i]);
    }
    m_textureLap.clear();

    for (int i = 0; i < m_listBonus.size(); i++)
    {
        glDeleteTextures(1, &m_listBonus[i]);
    }
    m_listBonus.clear();
    m_bonusTextureGenerated = false;

    m_listBonusKey.clear();
    m_listOpponent.clear();

    if (m_bonusAttack != nullptr)
    {
        delete m_bonusAttack;
    }
    if (m_attackReceived != nullptr)
    {
        delete m_attackReceived;
    }
    if (m_finish != nullptr)
    {
        delete m_finish;
    }

    m_BackGroundTextureGenerated = false;
    m_bonusTextureGenerated = false;
}

/// <summary>
/// R�initialise la course
/// </summary>
void CKart::InitRace()
{
    m_currentBonus = -1;
    m_lastPorte = 0;
    m_lap = 1;
    m_PartieTermine = false;
    m_gameStarted = false;
    m_initChrono = false;

    m_camera->m_listPortes[0]->DesactivePorte();
    for (size_t i = 1; i < m_camera->m_listPortes.size(); i++)
    {
        m_camera->m_listPortes[i]->ActivePorte();
    }

    m_currentLap = "Tour " + to_string(m_lap) + "/" + to_string(m_config->m_NombreToursPartie);
    m_serialPort->EnableSendData(false);
}

/// <summary>
/// Indique le d�marrage de la course
/// </summary>
void CKart::StartRace()
{
    m_gameStarted = true;
    m_serialPort->EnableSendData(true);
}


/// <summary>
/// Fonction de test des attaques
/// </summary>
/// <param name="key"></param>
/// <returns></returns>
char CKart::UpdateKeyPress(char key)
{
    return key;
}

/// <summary>
/// Ajoute un kart � la liste des opposants
/// </summary>
/// <param name="kart"></param>
void CKart::RegisterOpponent(CKart* kart)
{
    m_listOpponent.push_back(kart);
}

/// <summary>
/// Une attaque a �t� re�ue
/// </summary>
/// <param name="val"></param>
void CKart::AttaqueRecue(char val)
{
    switch (val)
    {
    case 'm':
        if (m_bonusAttack == nullptr || (m_bonusAttack != nullptr && !m_bonusAttack->m_protectionAttaques))
        {
            if (m_bonusAttack != nullptr) {
                //arr�t du bonus en cours
                m_bonusAttack->CancelAttaque();
                delete(m_bonusAttack);
                m_bonusAttack = nullptr;
            }
        }

        if (m_attackReceived == nullptr && m_bonusAttack == nullptr) {
            m_attackReceived = m_listAttaque["Stop"];
            m_attackReceived->Init();
            m_serialPort->StopKart();
        }
        break;

    case 'o':
        if (m_attackReceived == nullptr) {
            m_attackReceived = m_listAttaque["Blooper"];
            m_attackReceived->Init();
        }
        break;

    case 'v':
        if (m_attackReceived == nullptr) {
            m_attackReceived = m_listAttaque["Volant"];
            m_attackReceived->Init();
            m_serialPort->InverseSteering();
        }
        break;
    }
    
}


/// <summary>
/// Fonction de traitement lorsqu'un bonus est lanc� par le joueur
/// </summary>
/// <param name="val"></param>
void CKart::BonusActiveParJoueur(char val)
{
    val = tolower(val);

    if (m_bonusAttack != nullptr)
    return;

    switch (val)
    {
    case 'm':   //lancement d'un missile
        if (m_listOpponent.size() > 0)
        {
            //envoi d'un message � la raspberry
            m_kartMQTT->SendToRaspberry(m_noKart, "m");
        }
        else
        {
            m_ARelements.push_back(new CARMissile());
        }
        break;

    case 'e':   //Eclair. augmentation de la vitesse
        cout << 'e' << endl;
        m_bonusAttack = m_listAttaque["Eclair"];
        m_bonusAttack->Init();
        m_serialPort->ActiveBoost();
        break;

    case 's':   //Shield: protection du joueur
        m_bonusAttack = m_listAttaque["Shield"];
        m_bonusAttack->Init();
        break;

    case 'o':   //Oil: envoi d'huile sur l'adversaire
        if (m_listOpponent.size() > 0)
        {
            for (size_t i = 0; i < m_listOpponent.size(); i++)
            {
                if (m_listOpponent[i] != nullptr) m_listOpponent[i]->AttaqueRecue('o');
            }
        }
        else
        {
            m_bonusAttack = m_listAttaque["Blooper"];
            m_bonusAttack->Init();
        }
        break;

    case 'v':
        if (m_listOpponent.size() > 0)
        {
            for (size_t i = 0; i < m_listOpponent.size(); i++)
            {
                if (m_listOpponent[i] != nullptr) m_listOpponent[i]->AttaqueRecue('v');
            }
        }
        else
        {
            m_bonusAttack = m_listAttaque["Volant"];
            m_bonusAttack->Init();
            m_serialPort->InverseSteering();
        }
        break;
    }
}

/// <summary>
/// Fonction d'affichage de tous les �l�ments � l'�cran
/// </summary>
void CKart::DisplayScreen()
{
    Mat Frame;

    //D�finition du statut du joeur
    switch (m_kartStatus)
    {
    case t_ConnexionStatus::NOTHING: //Pas de cam�ra connect�e ni de joystick
        if (m_camera->isCameraConnected())
        {
            m_kartStatus = t_ConnexionStatus::CAMERA_OK;
        }
        else if (m_kartMQTT->isRadioConnected(m_noKart) || m_serialPort->m_connected)
        {
            m_NoCameraFrame.copyTo(m_CameraFrame);
            m_kartStatus = t_ConnexionStatus::JOY_CONNECTED;
        }
        break;

    case t_ConnexionStatus::CAMERA_OK:  //seule la cam�ra est connect�e
        if (m_kartMQTT->isRadioConnected(m_noKart) || m_serialPort->m_connected)
        {
            m_kartStatus = t_ConnexionStatus::ALL_OK;
        }
        if (!m_camera->isCameraConnected())
        {
            m_kartStatus = t_ConnexionStatus::NOTHING;
        }
        break;

    case t_ConnexionStatus::JOY_CONNECTED:  //seul le joystick est connect�
        if (m_camera->isCameraConnected())
        {
            m_kartStatus = t_ConnexionStatus::ALL_OK;
        }
        if (!m_kartMQTT->isRadioConnected(m_noKart) && !m_serialPort->m_connected)
        {
            m_kartStatus = t_ConnexionStatus::NOTHING;
        }
        break;

    case t_ConnexionStatus::ALL_OK: //tout est connect�
        if (!m_camera->isCameraConnected())
        {
            m_kartStatus = t_ConnexionStatus::JOY_CONNECTED;
        }
        if (!m_kartMQTT->isRadioConnected(m_noKart) && !m_serialPort->m_connected)
        {
            m_kartStatus = t_ConnexionStatus::CAMERA_OK;
        }
        break;
    }

    try
    {    
        //r�cup�ration de l'image courante
        m_camera->lock();
        m_CameraFrame.copyTo(Frame);
        m_camera->unlock();

        //Affichage de l'image � l'�cran
        renderBackgroundGL(Frame);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixd(m_camera->m_perspectiveMatrix);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(0, 0, 0, 0.0, 0.0, 1.0, 0.0, -1.0, 0.0);

        bool UpdateStatusPorte = false;

        m_camera->m_ARBarrel->DrawOpenGL();
        if (m_camera->m_ARBarrel->GetHistStatus())
        {
            AttaqueRecue('m');
        }


        m_camera->m_ARCible->DrawOpenGL();

        int nb = m_ARelements.size() - 1;
        for (int i = nb ; i >= 0; i--)
        {
            m_ARelements[i]->UpdatetargetPosition(m_camera->m_ARCible->GetCiblePosition());
            m_ARelements[i]->DrawOpenGL();
            if (m_ARelements[i]->isEnded())
            {
                m_ARelements.pop_back();
            }
        }

        if(m_gameStarted && !m_PartieTermine)
        if (m_LapSerial->IsNextLap(m_noKart)) {
            m_lap++; //le kart vient de faire un tour suppl�mentaire
            m_currentLap = "Tour " + to_string(m_lap) + "/" + to_string(m_config->m_NombreToursPartie);
        }


        //Regarde si le kart vient de passer une porte
        for (size_t i = 0; i < m_camera->m_listPortes.size(); i++)
        {
            m_camera->m_listPortes[i]->DrawOpenGL();

            //d�termine si le kart vient de passer la porte
            bool passagePorte = m_camera->m_listPortes[i]->IsGetBonus();
            int idPorte = m_camera->m_listPortes[i]->GetPorteId();

            if (m_lap == m_config->m_NombreToursPartie)
            {
                //finish
                m_PartieTermine = true;
                m_gameStarted = false;
            }

            if (m_currentBonus == -1 && passagePorte)
            {
                if (!m_PartieTermine && !m_renderRollingBonus)
                {
                    //La porte est automatique d�valid�e pour �viter de prendre des bonus en boucle
                    m_sound->PlaySoundEffect(BK_SOUND_BONUS);
                    m_renderRollingBonus = true;
                }
                m_lastPorte = idPorte;  
                UpdateStatusPorte = true;
            }
        }

        //On r�active les autres portes pour capturer les bonus
        if (UpdateStatusPorte)
        {
            for (size_t i = 0; i < m_camera->m_listPortes.size(); i++)
            {
                if (m_camera->m_listPortes[i]->GetPorteId() != m_lastPorte)
                {
                    m_camera->m_listPortes[i]->ActivePorte();
                }
                else
                {
                    m_camera->m_listPortes[i]->DesactivePorte();
                }
            }
        }

        //Tirage al�atoire d'un bonus lorsqu'une porte est pass�e
        if (m_renderRollingBonus && !m_sound->SoundEffectPlaying(BK_SOUND_BONUS))
        {
            m_renderRollingBonus = false;
            UniformVar* var = new UniformVar(0, m_listBonus.size()-1);
            m_currentBonus = (int)std::round(var->Tirage());

            //envoi le choix du bonus � la radio
            m_kartMQTT->SendToRadio(m_noKart, m_listBonusKey[m_currentBonus]);

        }

        //V�rifie si un bonus a �t� lanc� par le joueur
        char bonusSendValue = m_serialPort->m_bonus; //m_kartMQTT->getBonusSendValue(m_noKart);
        if (m_currentBonus != -1 && bonusSendValue != '_' && !m_PartieTermine)
        {
            m_kartMQTT->SendToRadio(m_noKart, "_");

            char bonus = m_listBonusKey[m_currentBonus][0];
            BonusActiveParJoueur(bonus);
            m_currentBonus = -1;
        }

        //V�rifie si une attaque a �t� re�ue
        char receiveAttackValue = m_kartMQTT->getReceiveAttackValue(m_noKart);
        if (receiveAttackValue != '_' && !m_PartieTermine)
        {
            cout << "attaque recue " << receiveAttackValue << endl;
            AttaqueRecue(receiveAttackValue);
        }

        if (m_gameStarted)
        {
            //r�cup�ration des consignes du joystick et affichage du kart
            int joy = m_serialPort->m_direction; // m_kartMQTT->getJoystick(m_noKart);
            int speed = m_serialPort->m_speed; //m_kartMQTT->getSpeed(m_noKart);

#ifdef _DrawVehicule
            m_vehicule->DrawVehicule(joy,speed);
#endif
        }

        renderFront();

        //Affichage des attaques re�ues
        if (m_attackReceived != nullptr && !m_PartieTermine)
        {
            m_attackReceived->Run();
            if (m_attackReceived->isEnded())
            {
                m_attackReceived = nullptr;
                m_serialPort->ResetBonus();
            }
        }

        //Affichage des bonus
        if (m_bonusAttack != nullptr && !m_PartieTermine)
        {
            try
            {
                m_bonusAttack->Run();
                if (m_bonusAttack->isEnded())
                {
                    m_bonusAttack = nullptr;
                    m_serialPort->ResetBonus();
                }
            }
            catch (...) {}
        }

        //affichage de fin de partie
        if (m_PartieTermine)
        {
            if(m_finish == nullptr) m_finish = new CFinish();
            m_finish->Run();
        }
    }
    catch (Exception e)
    {

    }
}

/// <summary>
/// Affichage de l'image dans l'environnement OpenGL
/// </summary>
/// <param name="image"></param>
void CKart::renderBackgroundGL(const cv::Mat& image)
{
 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);

    if (!m_BackGroundTextureGenerated)
    {
        glGenTextures(1, &m_textureFond);

        glBindTexture(GL_TEXTURE_2D, m_textureFond);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.size().width, image.size().height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, image.data);


        m_BackGroundTextureGenerated = true;
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, m_textureFond);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.size().width, image.size().height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, image.data);
    }


    // Draw the image.
    
    glBegin(GL_QUADS);
    glColor3d(1, 1, 1);
    glTexCoord2f(0, 0); glVertex3f(0, IMAGE_HEIGHT, -9.998f);
    glTexCoord2f(0, 1); glVertex3f(0, 0, -9.998f);
    glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, 0, -9.998f);
    glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, -9.998f);
    glEnd();

    glDisable(GL_TEXTURE_2D);
   
}

/// <summary>
/// Affichage des �l�ments de jeu au premier plan
/// </summary>
void CKart::renderFront()
{

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, IMAGE_WIDTH, 0, IMAGE_HEIGHT, -10, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();


    if (m_camera->isCameraConnected())
    {
        DrawBonus();
    }
    
    if (m_kartStatus == t_ConnexionStatus::CAMERA_OK)
    {
       /* if (m_textureNoJoystick == 0)
        {
            m_textureNoJoystick = COpenGL::LoadTexturePNG("./images/AttenteJoystick.png");
        }
        glBindTexture(GL_TEXTURE_2D, m_textureNoJoystick);
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);

        glBegin(GL_QUADS);
        glColor3d(1, 1, 1);
        glTexCoord2f(0, 1); glVertex3f(0, IMAGE_HEIGHT, 9.9f);
        glTexCoord2f(0, 0); glVertex3f(0, 0, 9.9f);
        glTexCoord2f(1, 0); glVertex3f(IMAGE_WIDTH, 0, 9.9f);
        glTexCoord2f(1, 1); glVertex3f(IMAGE_WIDTH, IMAGE_HEIGHT, 9.9f);
        glEnd();

        glDisable(GL_TEXTURE_2D);
        glPopMatrix();*/
    }

    if (m_gameStarted)
    {
        if (!m_initChrono)
        {
            m_initChrono = true;
            m_initChronoTime = std::chrono::system_clock::now();
        }

        auto chronoCourse = std::chrono::system_clock::now() - m_initChronoTime;   
        std::chrono::milliseconds ms = duration_cast<milliseconds>(chronoCourse);
        auto secs = duration_cast<seconds>(ms);
        ms -= duration_cast<milliseconds>(secs);
        auto mins = duration_cast<minutes>(secs);
        secs -= duration_cast<seconds>(mins);
        auto hour = duration_cast<hours>(mins);
        mins -= duration_cast<minutes>(hour);
        
        glPushMatrix();
        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 0.5f);
        glVertex3f(25, 55, 9.92f);
        glVertex3f(20, 10, 9.92f);
        glVertex3f(120, 10, 9.92f);
        glVertex3f(125, 55, 9.92f);
        glEnd();

        glColor3f(1.0, 1.0, 0.7f);
        glRasterPos3f(30, 26, 9.93f);
        
        for (int i = 0; i < m_currentLap.length(); i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, m_currentLap[i]);
        }


        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 1.0f);
        glVertex3f(IMAGE_WIDTH - 135, IMAGE_HEIGHT - 10, 9.92f);
        glVertex3f(IMAGE_WIDTH - 140, IMAGE_HEIGHT - 55, 9.92f);
        glVertex3f(IMAGE_WIDTH - 20, IMAGE_HEIGHT - 55, 9.92f);
        glVertex3f(IMAGE_WIDTH - 15, IMAGE_HEIGHT - 10, 9.92f);
        glEnd();

        glColor3f(0.0, 0.0, 0.0f);
        glRasterPos3f(IMAGE_WIDTH - 120, IMAGE_HEIGHT - 40, 9.93f);

        char buffer[20];  // maximum expected length of the float
        int len = std::snprintf(buffer, 20, "%02d:%02d:%03d", mins.count(), secs.count(), ms.count());
        
        for (int i = 0; i < len; i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);
        }

        glPopMatrix();
    }

    if (m_PartieTermine)
    {
        if (m_initChrono)
        {
            m_initChrono = false;
            m_endChronoTime = std::chrono::system_clock::now();
        }

        auto chronoCourse = m_endChronoTime - m_initChronoTime;
        std::chrono::milliseconds ms = duration_cast<milliseconds>(chronoCourse);
        auto secs = duration_cast<seconds>(ms);
        ms -= duration_cast<milliseconds>(secs);
        auto mins = duration_cast<minutes>(secs);
        secs -= duration_cast<seconds>(mins);
        auto hour = duration_cast<hours>(mins);
        mins -= duration_cast<minutes>(hour);

        glPushMatrix();
        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 1.0f);
        glVertex3f(IMAGE_WIDTH - 135, IMAGE_HEIGHT - 10, 9.92f);
        glVertex3f(IMAGE_WIDTH - 140, IMAGE_HEIGHT - 55, 9.92f);
        glVertex3f(IMAGE_WIDTH - 20, IMAGE_HEIGHT - 55, 9.92f);
        glVertex3f(IMAGE_WIDTH - 15, IMAGE_HEIGHT - 10, 9.92f);
        glEnd();

        glColor3f(0.0, 0.0, 0.0f);
        glRasterPos3f(IMAGE_WIDTH - 120, IMAGE_HEIGHT - 40, 9.93f);

        char buffer[20];  // maximum expected length of the float
        int len = std::snprintf(buffer, 20, "%02d:%02d:%03d", mins.count(), secs.count(), ms.count());

        for (int i = 0; i < len; i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);
        }
        glPopMatrix();
    }


    glPopMatrix();
}

/// <summary>
/// Affichage du bonus obtenu en passant une porte
/// </summary>
void CKart::DrawBonus()
{
    static int prtTextureBonus = 0;

    if (!m_bonusTextureGenerated)
    {
        m_textureRondBonus = COpenGL::LoadTexturePNG("./images/zoneBonus.png");
        m_listBonus.push_back(COpenGL::LoadTexturePNG("./images/Missile.png"));
        m_listBonus.push_back(COpenGL::LoadTexturePNG("./images/Eclair.png"));
        //m_listBonus.push_back(COpenGL::LoadTexturePNG("./images/Shield.png"));
        m_listBonus.push_back(COpenGL::LoadTexturePNG("./images/Oil.png"));
        m_listBonus.push_back(COpenGL::LoadTexturePNG("./images/Volant.png"));

        m_listBonusKey.push_back("M");
        m_listBonusKey.push_back("E");
        //m_listBonusKey.push_back("S");
        m_listBonusKey.push_back("O");
        m_listBonusKey.push_back("V");

        m_bonusTextureGenerated = true;
    }

    glEnable(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, m_textureRondBonus);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glColor3d(1, 1, 1);
    glTexCoord2f(0, 0); glVertex3f(50, IMAGE_HEIGHT - 200, 9.9f);
    glTexCoord2f(1, 0); glVertex3f(200, IMAGE_HEIGHT - 200, 9.9f);
    glTexCoord2f(1, 1); glVertex3f(200, IMAGE_HEIGHT - 50, 9.9f);
    glTexCoord2f(0, 1); glVertex3f(50, IMAGE_HEIGHT - 50, 9.9f);
    glEnd();
   
    if (m_renderRollingBonus || m_currentBonus != -1)
    {
        if (m_renderRollingBonus)
        {
            prtTextureBonus++;
            if (prtTextureBonus >= m_listBonus.size())  prtTextureBonus = 0;
            glBindTexture(GL_TEXTURE_2D, m_listBonus[prtTextureBonus]);
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, m_listBonus[m_currentBonus]);
        }
        glBegin(GL_QUADS);
        glColor3d(1, 1, 1);
        glTexCoord2f(0, 0); glVertex3f(50, IMAGE_HEIGHT - 200, 9.91f);
        glTexCoord2f(1, 0); glVertex3f(200, IMAGE_HEIGHT - 200, 9.91f);
        glTexCoord2f(1, 1); glVertex3f(200, IMAGE_HEIGHT - 50, 9.91f);
        glTexCoord2f(0, 1); glVertex3f(50, IMAGE_HEIGHT - 50, 9.91f);
        glEnd();
        
    }

    
    glDisable(GL_TEXTURE_2D);
}



