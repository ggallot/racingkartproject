#include "../include/CJoystickSerial.h"


CJoystickSerial::CJoystickSerial(int noKart, string portCom)
{
	unsigned long ThreadId;

	m_noKart = noKart;
	m_serialPort = portCom;
	m_mqtt = CMqtt::getInstance();
	m_SerialThread = CreateThread(NULL, NULL, _SerialThreadEntry, (void*)this, NULL, &ThreadId);

	m_sendDataToKart = true;
}

CJoystickSerial::~CJoystickSerial()
{
	m_ThreadSerialRunning = false;
}

void CJoystickSerial::EnableSendData(bool enable)
{
	m_sendDataToKart = enable;
}

void CJoystickSerial::ActiveBoost()
{
	m_activeBoost = true;
}

void CJoystickSerial::InverseSteering()
{
	m_invertSteering = true;
}

void CJoystickSerial::StopKart()
{
	m_stopKart = true;
}

void CJoystickSerial::ResetBonus()
{
	m_activeBoost = false;
	m_stopKart = false;
	m_invertSteering = false;
}

bool CJoystickSerial::OpenPort()
{
	int fRet = false;

	wstring noPort(m_serialPort.begin(), m_serialPort.end());
	wstring com = L"\\\\.\\" + noPort;

	if (m_serialHandle == 0)
	{
		// Open serial port
		m_serialHandle = CreateFile(com.c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		if (m_serialHandle == INVALID_HANDLE_VALUE)
		{
			cout << "error opening handle\n";
		}
		else
		{
			cout << "port opened\n";
			fRet = true;

			// Do some basic settings
			DCB serialParams = { 0 };
			serialParams.DCBlength = sizeof(serialParams);

			GetCommState(m_serialHandle, &serialParams);
			serialParams.BaudRate = m_baudRate;
			serialParams.ByteSize = m_bitSize;
			serialParams.StopBits = m_stopBits;
			serialParams.Parity = m_parity;
			SetCommState(m_serialHandle, &serialParams);

			// Set timeouts
			COMMTIMEOUTS timeout = { 0 };
			timeout.ReadIntervalTimeout = 50;
			timeout.ReadTotalTimeoutConstant = 50;
			timeout.ReadTotalTimeoutMultiplier = 50;
			timeout.WriteTotalTimeoutConstant = 50;
			timeout.WriteTotalTimeoutMultiplier = 10;

			SetCommTimeouts(m_serialHandle, &timeout);
		}
	}

	return fRet;
}

unsigned long WINAPI CJoystickSerial::ThreadSerial(void* param)
{
	char bufferRead[255];
	char bufferSend[255];
	DWORD  nbDataRead = 0;
	DWORD  nbDataWrite = 0;
	
	m_ThreadSerialRunning = true;

	while (m_ThreadSerialRunning)
	{
		try
		{
			if (!m_connected)
			{
				m_connected = OpenPort();

				if (!m_connected) {
					m_serialHandle = 0;
					Sleep(2000);
				}

			}
			else
			{
				bool success = ReadFile(m_serialHandle, &bufferRead, 255, &nbDataRead, NULL);
				if (nbDataRead > 0 && bufferRead[0] == 'J')
				{
					m_connected = true;
					bufferRead[nbDataRead - 1] = '\0';
					string message = bufferRead;
					message = message.substr(0, message.find(m_delimiter));

					int dir = 0;
					int speed = 0;
					char bonus = '_';
					if (sscanf_s(message.c_str(), "J:%d;S:%d;B:%c", &dir, &speed, &bonus, 1) == 3)
					{
						m_direction = dir;
						m_speed = speed;
						m_bonus = bonus;
					}
					else
					{
						m_direction = 0;
						m_speed = 0;
						m_bonus = '_';
					}

					if (m_activeBoost)
					{
						if (m_speed > 0) m_speed += 10;
						if (m_speed < 0) m_speed -= 10;

						if (m_speed > 100) m_speed = 100;
						if (m_speed < -100) m_speed = -100;
					}

					if (m_invertSteering)
					{
						m_direction = -m_direction;
					}

					if (m_stopKart || !m_sendDataToKart)
					{
						m_speed = 0;
					}

					stringstream ss;
					ss << "J:" << m_direction << ";S:" << m_speed << ";B:" << m_bonus;
					m_mqtt->SendDataJoystick(m_noKart, ss.str());
					
				}

				if (success)
				{
					//ack message
					bufferSend[0] = 'C';
					bufferSend[1] = '\0';
					WriteFile(m_serialHandle, &bufferSend, 1, &nbDataWrite, NULL);
				}
				else {
					m_connected = false;
					CloseHandle(m_serialHandle);
					m_serialHandle = 0;
				}
			}
		}
		catch (...)
		{
			m_connected = false;
			m_serialHandle = 0;
			cout << "Disconnected" << endl;
		}
	}

	if (m_serialHandle != 0)
	{
		CloseHandle(m_serialHandle);
	}

	return -1;
}


DWORD CJoystickSerial::_SerialThreadEntry(LPVOID lpthis)
{
	CJoystickSerial* pClass = reinterpret_cast <CJoystickSerial*>(lpthis);
	if (pClass != NULL) {
		return (DWORD)pClass->ThreadSerial(lpthis);
	}
	else
		TerminateThread(0, 0);
	return 1;
}