#include "../include/CLapSerial.h"


/* Null, because instance will be initialized on demand. */
CLapSerial* CLapSerial::m_instance = 0;

CLapSerial::CLapSerial()
{
	CConfiguration *conf = CConfiguration::getInstance();
	m_serialPort = conf->m_comPortKartLap;

	unsigned long ThreadId;
	//m_SerialThread = CreateThread(NULL, NULL, _SerialThreadEntry, (void*)this, NULL, &ThreadId);

}

CLapSerial::~CLapSerial()
{
	m_ThreadSerialRunning = false;
}

/// <summary>
/// Fonction d'instantiation de la classe en singleton
/// </summary>
/// <returns></returns>
CLapSerial* CLapSerial::getInstance()
{
	if (m_instance == 0)
	{
		m_instance = new CLapSerial();
	}

	return m_instance;
}

bool CLapSerial::OpenPort()
{
	int fRet = false;

	wstring noPort(m_serialPort.begin(), m_serialPort.end());
	wstring com = L"\\\\.\\" + noPort;

	if (m_serialHandle == 0)
	{
		// Open serial port
		m_serialHandle = CreateFile(com.c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		if (m_serialHandle == INVALID_HANDLE_VALUE)
		{
			cout << "error opening handle\n";
		}
		else
		{
			cout << "port opened\n";
			fRet = true;

			// Do some basic settings
			DCB serialParams = { 0 };
			serialParams.DCBlength = sizeof(serialParams);

			GetCommState(m_serialHandle, &serialParams);
			serialParams.BaudRate = m_baudRate;
			serialParams.ByteSize = m_bitSize;
			serialParams.StopBits = m_stopBits;
			serialParams.Parity = m_parity;
			SetCommState(m_serialHandle, &serialParams);

		}
	}

	return fRet;
}

void CLapSerial::ResetLapCount()
{
	char bufferSend[20];
	DWORD  nbDataWrite = 0;

	m_flagNextLap1 = false;
	m_flagNextLap2 = false;

	/*if (m_connected)
	{
		bufferSend[0] = 'R';
		bufferSend[1] = '\0';
		WriteFile(m_serialHandle, &bufferSend, 1, &nbDataWrite, NULL);
	}*/
}


/// <summary>
/// Indique si le v�hicule � termin� le tour
/// </summary>
/// <returns></returns>
bool CLapSerial::IsNextLap(int noKart)
{
	bool fRet = false;

	if (noKart == 1 && m_flagNextLap1)
	{
		m_flagNextLap1 = false;
		fRet = true;
	}

	if (noKart == 2 && m_flagNextLap2)
	{
		m_flagNextLap2 = false;
		fRet = true;
	}
	return fRet;
}


unsigned long WINAPI CLapSerial::ThreadSerial(void* param)
{
	int ptrBuffer = 0;
	char bufferRead[255];
	char bufferSend[255];

	char serialMessage[255];
	bool traiteMessage = false;

	DWORD  nbDataRead = 0;
	DWORD  nbDataWrite = 0;
	
	m_ThreadSerialRunning = true;

	while (m_ThreadSerialRunning)
	{
		try
		{
			if (!m_connected)
			{
				m_connected = OpenPort();

				if (!m_connected) {
					m_serialHandle = 0;
					Sleep(2000);
				}

			}
			else
			{
				bool success = ReadFile(m_serialHandle, &bufferRead, 1, &nbDataRead, NULL);
				
				if (nbDataRead > 0 )
				{
					if (bufferRead[0] == 'L') ptrBuffer = 0;

					serialMessage[ptrBuffer] = bufferRead[0];

					if (bufferRead[0] == '\r')
					{
						serialMessage[ptrBuffer] = '\0';
						traiteMessage = true;
					}

					ptrBuffer++;
					if (ptrBuffer > 254) ptrBuffer = 254;
				}

				if(traiteMessage)
				{
					traiteMessage = false;
					m_connected = true;
					string message = serialMessage;
					message = message.substr(0, message.find(m_delimiter));

					
					int NoKart = 0;
					int NextLap = 0;
					if (sscanf_s(message.c_str(), "L:%d;%d", &NoKart , &NextLap, 1) == 2)
					{
						if (NoKart == 1 && NextLap == 100)
						{
							m_flagNextLap1 = true;
						}

						if (NoKart == 2 && NextLap == 100)
						{
							m_flagNextLap2 = true;
						}

					}
				}

				/*if (success)
				{
					//ack message
					bufferSend[0] = 'C';
					bufferSend[1] = '\0';
					WriteFile(m_serialHandle, &bufferSend, 1, &nbDataWrite, NULL);
				}
				else {
					m_connected = false;
					CloseHandle(m_serialHandle);
					m_serialHandle = 0;
				}*/
			}
		}
		catch (...)
		{
			m_connected = false;
			m_serialHandle = 0;
			cout << "Disconnected" << endl;
		}
	}

	if (m_serialHandle != 0)
	{
		CloseHandle(m_serialHandle);
	}

	return -1;
}


DWORD CLapSerial::_SerialThreadEntry(LPVOID lpthis)
{
	CLapSerial* pClass = reinterpret_cast <CLapSerial*>(lpthis);
	if (pClass != NULL) {
		return (DWORD)pClass->ThreadSerial(lpthis);
	}
	else
		TerminateThread(0, 0);
	return 1;
}