# Racing Kart Project

Bienvenue sur le dépôt du projet de kart Open-Source ayant pour modèle le jeu Mario-Kart live
Pour en savoir plus sur le projet, n'hésitez pas à consulter ma chaine Youtube https://www.youtube.com/user/djzzRobotic

Le projet est composé de plusieurs projets

## Arduino

Ce dossier contient deux projets arduino

- [ ] [Programme_kart: contient le programme à mettre sur la carte électronique du kart]
- [ ] [Radio_M5Stack: contient le programme à mettre dans la radiocommande]

## Racingkart

Contient les sources du jeu a faire tourner sur PC (compatible uniquement pour windows actuellement)

## CAO

Contient le fichier Step de la conception 3D du kart

## ControlPanel

Contient une IHM en python pour pouvoir modifier certains paramètres du jeu sur un ordinateur séparé.
Ce script peut tourner sur une raspberry équipé d'un écran.

## Kicad

Contient les schémas électroniques ainsi que les boards des différents systèmes

## Raspberry

Contient le script python a installer sur la raspberry présente dans le kart
 

# Installation des logiciels

Les procédures d'installation sont en cours d'écriture