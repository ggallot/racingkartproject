from tkinter import * 
import paho.mqtt.client as mqtt
import time
from threading import Thread

broker_address = "192.168.0.12"
global client
global running_thread

def onSlider1(value=None):
    global client
    print(value)
    client.publish('radio1/config',value,1,False)

def onSlider2(value=None):
    global client
    print(value)
    client.publish('radio2/config',value,1,False)

def onStart():
    global client
    client.publish('radio1/screen','rN',1,False)

def onPause():
    global client
    client.publish('radio1/screen','sE',1,False)

def onPowerOff():
    global client
    client.publish('GameKartPc/shutdown','S',1,False)


def onJoystickConnected(noJoystick, value):
    if(noJoystick == 1):
        if(value == True):
            Joystick1.config(background='lightgreen')
        else:
            Joystick1.config(background='red')
    elif(noJoystick == 2):
        if(value == True):
            Joystick2.config(background='lightgreen')
        else:
            Joystick2.config(background='red')


def onKartConnected(noKart, value):
    if(noKart == 1):
        if(value == True):
            StatusKart1.config(background='lightgreen')
        else:
            StatusKart1.config(background='red')
    elif(noKart == 2):
        if(value == True):
            StatusKart2.config(background='lightgreen')
        else:
            StatusKart2.config(background='red')


def Mqtt_Task(arg):
    global running_thread
    global client
    #creation de la connexion MQTT
    client = mqtt.Client('ControlPanel')
    client.on_connect = on_MqttConnect
    client.on_message = on_MqttMessage
    client.loop_start()

    while(running_thread):
        try:
            if(client.is_connected() == False):
                client.connect(broker_address,1883,10)
        except:
            print("connexion failed with broker")
        time.sleep(1)


def on_MqttConnect(client, userdata, flags, rc):
    print("connected")
    if rc==0:
        client.subscribe('GameKartRpi1/startup')
        client.subscribe('GameKartRpi2/startup')
        client.subscribe('radio1/startup')
        client.subscribe('radio2/startup')
        
    else:
        print("Bad connection Returned code=",rc)
  

def on_MqttMessage(client, userdata, message):
    topic = message.topic
    message = str(message.payload.decode("utf-8"))

    connect = False
    if(message == 'C'):
        connect = True

    if(topic == 'GameKartRpi1/startup'):
        onKartConnected(1, connect)
    elif(topic=='GameKartRpi2/startup'):
        onKartConnected(2, connect)
    elif(topic=='radio1/startup'):
        onJoystickConnected(1, connect)
    elif(topic=='radio2/startup'):
        onJoystickConnected(2, connect)

if __name__ == '__main__':
    app =  Tk()
    app.title("toto")
    app.geometry("1024x600")
        
    p = PanedWindow(app,orient=VERTICAL)
    p.pack(side=TOP, expand=Y, fill=BOTH)

    #imageApp =  Label(p,text="Volet 1", background='blue', anchor=CENTER)
    photo = PhotoImage(file="titre.png")
    canvas = Canvas(p,width=1024, height=100)
    canvas.create_image(0, 0, anchor=NW, image=photo)
    canvas.pack()


    Karts = PanedWindow(app,orient=HORIZONTAL, height=400)
    Karts.pack(side=TOP, expand=Y, fill=BOTH)

    #Kart 1
    Kart1 = PanedWindow(app,orient=VERTICAL,width=512)
    Kart1.pack(side=TOP, expand=Y, fill=BOTH)

    ####
    InfoKart1 = PanedWindow(app,orient=HORIZONTAL)
    InfoKart1.pack(side=TOP, expand=Y, fill=BOTH)

    Joystick1 = Button(InfoKart1,text="Joystick", background='red', anchor=CENTER,width=50)
    StatusKart1 = Button(InfoKart1,text="Kart", background='red', anchor=CENTER)

    InfoKart1.add(Joystick1)
    InfoKart1.add(StatusKart1)
    InfoKart1.pack()
    ####


    value1 = DoubleVar()
    value1.set(80)
    sliderk1 = Scale(Kart1,variable=value1, background='yellow', sliderlength=50, resolution=10, width=60, tickinterval=10, command=onSlider1)
    sliderk1.config(from_ = 100, to = 0)
    sliderk1.pack(anchor=CENTER)

    Kart1.add(InfoKart1)
    Kart1.add(sliderk1)
    Kart1.pack()
    #Kart 2
    Kart2 = PanedWindow(app,orient=VERTICAL)
    Kart2.pack(side=TOP, expand=Y, fill=BOTH)

    ####
    InfoKart2 = PanedWindow(app,orient=HORIZONTAL)
    InfoKart2.pack(side=TOP, expand=Y, fill=BOTH)

    Joystick2 = Button(InfoKart2,text="Joystick", background='red', anchor=CENTER,width=50)
    StatusKart2 = Button(InfoKart2,text="Kart", background='red', anchor=CENTER)

    InfoKart2.add(Joystick2)
    InfoKart2.add(StatusKart2)
    InfoKart2.pack()
    ####

    value = DoubleVar()
    value.set(80)
    sliderk2 = Scale(Kart2,variable=value, background='purple', fg='white', sliderlength=50, resolution=10, width=60, tickinterval=10, command=onSlider2)
    sliderk2.config(from_ = 100, to = 0)
    sliderk2.pack(anchor=CENTER)

    Kart2.add(InfoKart2)
    Kart2.add(sliderk2)
    Kart2.pack()


    ####
    ConfigGame = PanedWindow(app,orient=HORIZONTAL)
    ConfigGame.pack(side=TOP, expand=Y, fill=BOTH)

    btnStart = Button(ConfigGame,text="Play",  background='lightgreen', anchor=CENTER,width=50, command=onStart)
    btnPause = Button(ConfigGame,text="Pause",  background='yellow', anchor=CENTER,width=50, command=onPause)
    btnPowerOFf = Button(ConfigGame,text="Eteindre",  background='red', anchor=CENTER, command=onPowerOff)

    ConfigGame.add(btnStart)
    ConfigGame.add(btnPause)
    ConfigGame.add(btnPowerOFf)
    ConfigGame.pack()
    ####

    #Assemblage
    Karts.add(Kart1)
    Karts.add(Kart2)
    Karts.pack()

    p.add(canvas)
    p.add(Karts)
    p.add(ConfigGame)
    p.pack()

    running_thread = True
    thread2 = Thread(target = Mqtt_Task, args = (12,))
    thread2.start()

    app.mainloop()
    running_thread = False